Contribution
============

## Contributeurs historiques

* **BOURLARD Donovan** - Travail initial
* **ANODE Stratégie Digitale** - Maintenance et évolutions

Des contributions ont peut-être été faites sur le projet. Pour analyser la liste des contributeurs, référez-vous au dépôt de code sur GitLab.

Si vous souhaitez contribuer à Madis, veuillez vous référer à la documentation vous correspondant :
- [Signaler un problème ou une suggestion d'amélioration](#signaler-un-problème-ou-une-suggestion-damélioration)
- [Proposer un développement d'une fonctionnalité](#proposer-un-développement-dune-fonctionnalité)
    1. [Création d'un ticket](#1-création-dun-ticket)
    2. [Développements](#2-développements)
    3. [Traitement de la demande de fusion par les mainteneurs](#3-traitement-de-la-demande-de-fusion-par-les-mainteneurs)

## Signaler un problème ou une suggestion d'amélioration

Avant toute contribution, merci de vérifier qu'une demande similaire n'est pas déjà en cours ou résolue. Pour cela, veuillez vérifier :
1. Que le problème soit bien présent dans la [dernière version](https://gitlab.adullact.net/soluris/madis/-/tags) (Consulter le [journal des modifications (Changelog)](CHANGELOG.md#changelog)) ;
2. Qu'un [ticket ouvert](https://gitlab.adullact.net/soluris/madis/-/issues) ne décrit pas déjà votre demande (dans ce cas, participez à la discussion de ce dernier).

Dans le cas où il n'y aurait pas de ticket déjà ouvert, il faudra alors en créer un. Il est recommandé de donner au ticket un titre concis et le plus explicite possible.

**Recommandation Madis** : Il est proposé que chaque ticket soit préfixé par le nom du module. Par exemple, un ticket pour le registre des traitements aura comme titre `[Traitements] Mon titre explicite et concis`.

La description du ticket détaille et précise le propos. Il est possible d'ajouter des images, voire des fichiers. Veuillez apporter un maximum d'informations pour permettre un traitement efficace de celui-ci.

## Proposer un développement d'une fonctionnalité

Il est possible de proposer un développement que ce soit pour le correctif d'un problème, pour l'amélioration de Madis, ou encore le développement de nouvelles fonctionnalités.

### 1. Création d'un ticket

En général, avant de réaliser des développements, il est préférable de commencer par [créer un ticket](#signaler-un-problème-ou-une-suggestion-damélioration) regroupant vos intentions de développement pour le partager au reste de la communauté et de la faire réagir. Cela permet par exemple, d'anticiper le fait qu'une fonctionnalité ou un correctif ne corresponde pas à Madis et à ses utilisateurs, ou soit incomplète.

La discussion se fait sous forme de commentaires du ticket. Pour les contributeurs ayant un peu d'expérience, il est suggéré de mettre à jour la description de l'issue au gré de la discussion.

### 2. Développements

Dans les recommandations de travail collaboratif, il est proposé de créer une nouvelle branche à partir de la `develop` pour chaque nouveau travail. Par convention, il est conseillé de préfixer le nom la branche nouvellement créée par l'id du ticket associé.

Avant tout développement, vous devez suivre et respecter l'[architecture et la qualité du code existant](doc/developpement/2-architecture-et-qualite.md#architecture-et-qualité), de plus il est important de respecter plusieurs éléments :
- Le projet Madis étant sous licence AGPLv3, tout ajout de librairie au projet Madis doit donc être conforme avec celle-ci ;
- Les conventions de codage ;
- La sécurité ;
- L'accessibilité selon les [critères et tests du RGAA](https://accessibilite.numerique.gouv.fr/methode/criteres-et-tests/).

Une fois le code développé et testé, vous devez pousser votre code vers le GitLab et faire un merge request de votre branche vers la branche `develop`. Gérer les éventuels conflits en les corrigeant. Lors du merge, vous devrez faire référence au ticket associé si ce n'est pas déjà le cas, et éventuellement apporter des informations détaillées de votre développement.

### 3. Traitement de la demande de fusion par les mainteneurs

Le lancement de la merge request génère une notification aux mainteneurs. Ces derniers traitent la merge request. Une phase de recettage est réalisée, et un nouvel espace de discussion est possible entre mainteneur et développeur.

Si tout est conforme, le mainteneur valide la merge request et le code est passé dans la branche develop. Celui-ci sera alors intégré directement dans une nouvelle version Madis, ou dans une version future.
