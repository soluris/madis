CHANGELOG
=========
## [2.4.9] - 2025-XX-XX
### Ajout
- [Formulaire] Ajout d'un label aux champs type et liste des structures dans la gestion des droits des modèles d'AIPD et dans les référentiels, ainsi que dans la duplication.
- [Accessibilité] Ajout du plan d'actions 2025.
- [Preuves] Ajout de la possibilité de déposer des documents ayant comme extension .txt, .log, .csv et .md.
- [Bilan] Ajout du score moyen de l'indice de maturité dans le bilan et dans le document Word de synthèse.
- [Référentiels] Ajout du nom de l'élément modifié dans la gestion des droits des référentiels.
- [AIPD] Ajout du nom de l'élément modifié dans la gestion des droits des modèles d'AIPD.
- [AIPD] Ajout du modèle XML sur la vidéoprotection mis en oeuvre par les autorités publiques.
- [AIPD] Ajout d'un bouton pour retourner à la liste et un bouton pour générer un PDF depuis l'étape 5 d'évaluation d'une AIPD.
- [AIPD] Ajout d'un changement dynamique de l'impact potentiel selon le choix de la vraisemblance et de la gravité dans l'étape 3 d'instruction d'une AIPD.
### Changement
- [Global] Divers renommages et traductions.
- [Documentation] Mise à jour de la documentation technique.
- [Documentation] Mise à jour de la documentation utilisateur.
- [Accessibilité] Mise à jour du schéma pluriannuel et du plan d'actions 2024.
- [Plan du site] Mise à jour des pages liées à l'accessibilité.
- [Traitements] Déplacement des champs finalités et état.
- [Traitements] Modification du champ finalités en sélecteur.
- [Traitements] Déplacement du consentement demandé et du format du bloc détails au bloc informations générales.
- [Traitements] Déplacement des critères spécifiques par ordre alphabétique.
- [Traitements] Le critère « Croisement de données » est renommé en « Croisement d’ensemble de données ».
- [Traitements] Le critère « Décisions automatisées avec effet » est renommé en « Décision automatique avec effet juridique ».
- [Traitements] Le critère « Exclusion automatique d'un service » est renommé en « Exclusion du bénéfice d’un droit, d’un service ou contrat ».
- [Traitements] Déplacement des mesures de sécurité et confidentialité par ordre alphabétique.
- [Sous-traitants] Inversion de la case à cocher de l'envoi de données hors UE en dans l'UE.
- [Indice de maturité] Modification du bouton de modification pour s'afficher que s'il y a des préconisations dans la synthèse d'un indice de maturité.
- [Bilan] Modification de l'affichage des données du dernier indice de maturité dans le bilan.
- [AIPD] Amélioration globale dans le PDF d'une AIPD.
- [AIPD] Modification de l'image sur la gravité et la vraisemblance dans le PDF d'une AIPD.
- [AIPD] Modification des dates des avis des acteurs pour ne rien afficher par défaut dans le PDF d'une AIPD.
- [AIPD] Modification pour afficher par défaut les champs de justification des principes fondamentaux dans l'étape 3 d'instruction d'une AIPD.
### Suppression
- [AIPD] Suppression du bouton pour enregistrer un brouillon dans l'étape 5 d'évaluation d'une AIPD.
### Fix
- [Global] Fix de certains champs manquants dans les documents PDF et Word générés depuis les différents modules.
- [Indice de maturité] Fix de l'affichage du graphique dans le bilan et la synthèse d'un indice de maturité.
- [Bilan] Fix de l'affichage de la date dans le tableau des actions de protection mises en place.
- [Bilan] Fix de certaines données présentes dans le tableau annexe du registre des traitements.
- [Bilan] Fix de certaines données calculées dans le paragraphe du registre des sous-traitants.
- [AIPD] Fix de coquilles dans le modèle d'AIPD par défaut.
- [Registre public] Fix affichage des durées de conservation.
- [Migration] Fix d'une migration qui peut causer une erreur lors du déploiement.

## [2.4.8] - 2024-12-10
### Changement
- [Global] Divers renommages et traductions.
### Fix
- [Traitements] Fix du tri par ordre croissant ou décroissant de plusieurs colonnes dans la liste des traitements.
- [Logiciels et supports] Fix des boutons d'ajout et de génération visibles en administrateur alors que le module est inactif.
- [Violation] Fix de la date dans la confirmation d'archivage d'une violation.
- [Violation] Fix du tri par ordre croissant ou décroissant de plusieurs colonnes dans la liste des violations.
- [Preuves] Fix des actions en trop et manquantes en lecteur dans les preuves.
- [Indice de maturité] Fix de l'accès à une page de l'indice de maturité par un lecteur.
- [Espace documentaire] Fix de l'ordre d'affichage des éléments du champ des catégories dans la vue grille et liste, ainsi que dans l'ajout/modification d'un document.
- [Espace documentaire] Fix de l'accès aux documents partagés via un lien de partage.
- [Bilan] Fix du module logiciels et supports listé dans les registres recensés alors que le module est inactif.

## [2.4.7] - 2024-11-25
### Ajout
- [Global] Ajout de la possibilité de lier des traitements, des sous-traitants, des logiciels et supports, des demandes, des violations, des preuves, ainsi que des actions de protection depuis chacun d'entre eux.
- [Global] Ajout des différents éléments associés dans les documents PDF et Word générés.
- [Services] Augmentation du périmètre d'effet des services aux logiciels et supports, aux preuves, et aux actions de protection.
- [Services] Ajout d'une colonne "Services" dans les différents modules soumis aux services.
- [Services] Ajout du service dans la visualisation d'un élément et dans les documents générés.
- [Traitements] Ajout de la possibilité de filtrer et ordonner certaines colonnes dans la liste des traitements.
- [Preuves] Ajout de la possibilité de désarchiver une preuve.
- [Plan d'actions] Ajout de la possibilité de générer le plan d'actions au format Word.
- [Indice de maturité] Ajout de la possibilité de visualiser les actions de protection associées aux préconisations dans la synthèse d'un indice de maturité en lecteur.
- [Structures & Ma structure] Ajout de la possibilité de paramétrer le bilan dans la gestion de sa structure ou d'une structure.
- [Structures & Ma structure] Ajout de la possibilité de générer un bilan dans la visualisation de sa structure ou d'une structure.
- [Structures] Ajout de la possibilité de générer des bilans en masse dans la liste des structures.
- [Bilan] Ajout d'une section pour le registre des logiciels et supports.
- [Bilan] Ajout du paramètre suivant dans le .env concernant le logo du DPD mutualisé : `APP_DPO_IMAGE_LOGO_COULEUR="images/logo_madis_2020_couleur.png"`.
- [Conformité des traitements] Ajout de la possibilité de filtrer et ordonner les colonnes dans la liste de la conformité des traitements.
- [Conformité de la structure] Ajout de la possibilité de filtrer et ordonner les colonnes dans la liste de la conformité de la structure.
- [Duplication] Ajout de la possibilité de dupliquer des logiciels et supports.
### Changement
- [Global] Divers renommages et traductions.
- [Global] Augmentation du nombre de caractères maximums à 150 pour les données affichées dans les options des sélecteurs multiples.
- [Formulaires] Modification des sélecteurs multiples des différents éléments associés pour intégrer des données sur les éléments (actif/inactif, archivé/non archivé, type, date...).
- [Espace documentaire] Modification pour que la colonne "Poids" soit vide pour les liens en vue liste.
- [Structures & Ma structure] Renommage du champ FINESS Géo pour intégrer le numéro RNA et accès à l'information pour l'ensembles des utilisateurs dans la visualisation.
- [Structures] Modification de la colonne "Informations complémentaires" pour ne pas être affichée par défaut.
- [Structures] Modification du SIREN pour qu'il soit unique et qu'il permette de renseigner un SIRET.
- [Documentation] Mise à jour de la documentation technique.
- [Documentation] Mise à jour de la documentation utilisateur.
- [Historique] Accès au nom de la structure en référent multi-structures.
### Suppression
- [Utilisateurs] Suppression temporaire de l'action de suppression d'un utilisateur le temps que la fonctionnalité soit développée.
### Fix
- [Connexion] Fix du favicon pour être celui renseigné dans le .env.
- [Tableau de bord] Fix des calculs des demandes à traiter.
- [Plan d'actions] Fix du tri par ordre croissant ou décroissant de la colonne "Date de création".
- [Plan d'actions] Fix de l'accès à la liste du plan d'actions par un lecteur.
- [Indice de maturité] Fix de l'affichage du graphique dans la synthèse d'un indice de maturité.
- [Espace documentaire] Fix de l'accès à la vue grille de l'espace documentaire par un lecteur.
- [Structures] Fix du tri par ordre croissant ou décroissant de plusieurs colonnes dans la liste des structures.
- [Sécurité] Cloisonnement d'accès des données, des actions et des interfaces selon le périmètre de l'utilisateur et de sa structure (modules complémentaires).
- [Sécurité] Un traitement n'étant plus public ne pourra pas être consulté depuis le registre public.
### Migration
- [Migration] Ajout d'un fichier de migration pour corriger le nom d'un principe fondamental de la conformité des traitements.

## [2.4.6] - 2024-07-05
### Changement
- [Global] Divers renommages et traductions.
- [Traitements] Déplacement de la colonne "Mise à jour" avec les autres colonnes des mesures de sécurité et confidentialité.
- [Documentation] Mise à jour de la documentation technique.
- [Documentation] Mise à jour de la documentation utilisateur.
- [AIPD] Augmentation du nombre de caractères maximums à 2 000 pour le champ "Justification" dans la création ou modification d'un modèle d'AIPD, et dans l'instruction d'une AIPD.
### Suppression
- [Global] Suppression de divers messages générés par console.log.
- [Logiciels et supports] Suppression de la limite du nombre de caractères maximums de certains champs.
### Fix
- [Traitements] Fix du tri par ordre croissant ou décroissant de certaines colonnes.
- [Traitements] Fix de la limite de caractères du message d'erreur de certains champs.
- [Logiciels et supports] Fix de l'accès à la liste des logiciels et supports par un référent multi-structures.
- [Demandes] Fix du tri par ordre croissant ou décroissant de certaines colonnes.
- [Bilan] Fix de certaines données présentes dans le tableau du registre des violations.
- [AIPD] Fix du sommaire dans le PDF d'une AIPD.
- [AIPD] Fix de la couleur de certains tags dans le PDF d'une AIPD.
- [Notification] Fix de l'accès au centre de notification par un gestionnaire.

## [2.4.5] - 2024-05-31
### Ajout
- [Tableau de bord] Ajout du type "S'opposer au traitement" dans le graphique des types des demandes.
- [Référentiels] Ajout du référentiel XML sur la sécurité des données de la CNIL v2024.
- [Référentiels] Ajout du référentiel XML sur la sécurité et l'homologation des services publics numériques à la cybersécurité.
- [Référentiels] Ajout du référentiel XML sur les recommandations sur le nomadisme numérique de l'ANSSI.
- [Référentiels] Ajout du référentiel XML de l'indice de maturité Madis par défaut.
### Changement
- [Global] Divers renommages et traductions.
- [Global] Migration de la font Awesome v6.4.2 en v6.5.2.
- [Documentation] Mise à jour de la documentation technique.
- [Documentation] Mise à jour de la documentation utilisateur.
- [Accessibilité] Mise à jour de la déclaration d'accessibilité et du plan d'actions 2024.
- [Traitements] Modification du terme "Délai de conservation" en "Durée de conservation".
- [Traitements] Modification du bloc "Conformité du traitement" et "Analyse d'impact" dans la visualisation d'un traitement, ainsi que les différents PDF générés.
- [Actions de protection] Modification des colonnes "Coûts" et "Charge" pour ne pas être affichées par défaut.
- [Actions de protection] Modification sur le nom de la colonne ordonnée par défaut (avant priorité).
- [Plan d'actions] Modification sur la date d'échéance de la colonne ordonnée par défaut (avant priorité).
- [AIPD] Modification du modèle AIPD pour qu'une justification ne soit pas obligatoire par défaut.
- [AIPD] Modification des icônes et actions liées aux AIPD dans la conformité des traitements.
- [Structure] Modification du type de structure "Association" en "Association ou Groupement (GIP ou GIE)".
### Fix
- [Global] Fix global d'éléments non souhaités affichés en Lecteur.
- [Accessibilité] De nombreux fixes divers liés à l'accessibilité suite à l'audit RGAA.
- [Tableau de bord] Fix des calculs des demandes à traiter.
- [Tableau de bord] Fix des calculs du graphique des types des demandes.
- [Tableau de bord] Fix des calculs du graphique du statut des demandes.
- [Tableau de bord] Fix des calculs des violations.
- [Traitements] Fix du bloc pour de la conformité du traitement et de l'AIPD qui ne s'affichaient pas toujours.
- [Violations] Fix des nombres supérieurs à 9 chiffres.
- [Preuves] Fix d'obligation de dépôt de fichier lors de la modification d'une preuve.
- [AIPD] Fix d'un principe fondamental qui générait une erreur lors de la réalisation d'une AIPD, ou de son export PDF.
- [AIPD] Fix d'éléments affichés dans le graphique des mesures dans l'export PDF d'une AIPD.
- [Bilan] Fix de la largeur des tableaux dans le bilan.
- [Référentiels] Fix de l'URL de certains liens PDF.
- [Espace documentaire] Fix du type .avi qui n'était pas déposable dans l'espace documentaire.
### Suppression
- [Image] Suppression de l'icône `favicon.ico` du dossier `public`, qui étant en double dans les images. Penser à adapter le chemin vers le bon favicon `images/logo_madis_2020_favicon.png`.
### Migration
- [Migration] Ajout d'un fichier de migration pour créer automatiquement les catégories de données dans les traitements.
- [Migration] Ajout d'un fichier de migration pour mettre à jour les titres, les couleurs, les descriptions, et les questions, pour la conformité de la structure.
- [Migration] Ajout d'un fichier de migration pour corriger le nom d'un principe fondamental de la conformité des traitements.

## [2.4.0] - 2024-02-13
Détails des modifications #894
### Ajout
- [Formulaire] Largeur fixe sur les textarea.
- [Accessibilité] Ajout de pages obligatoires (déclaration, schéma, plans annuels) depuis le pied de page et le plan du site.
- [Accessibilité] Ajout d'attribut ARIA pour signaler la page actuelle.
- [Traductions] Ajout d'un fichier de traduction pour les nouvelles pages.
- [Configuration] Ajout d'une phrase d'introduction dans la configuration du registre public.
- [Structure] Ajout du bloc « Adresse » pour les gestionnaires en modification de sa structure.
- [Structure] Ajout du champ « Numéro de désignation CNIL » pour les gestionnaires en modification de sa structure.
### Changement
- [Global] Divers renommages et traductions.
- [Global] Migration de la font Awesome v5.0.13 en v6.4.2.
- [Global] Modification de nombreuses icônes.
- [Éléments associés] Déplacement et harmonisation des éléments liés dans un bloc commun « Éléments associés ».
- [Configuration] Le menu « Configuration des traitements » est déplacé et renommé en « Registre public ».
- [Preuves] Renommage de la partie menu « Preuves » en « Justificatifs », et du menu « Documents » en « Preuves ».
- [Mon compte & Ma structure] Déplacement des menus « Mon compte » et « Ma structure » dans un dropdown de l'en-tête.
- [Structures & Ma structure] Déplacement et réorgnisation des blocs et de certains champs dans la visualisation et la gestion.
- [Utilisateurs & Mon compte] Déplacement et réorgnisation des blocs et de certains champs dans la gestion.
- [Formulaire] Modification de tous les champs de formulaire en vertical et refonte de certains formulaires comme les référentiels.
- [Formulaire] Modification de l'astérisque « * » d'obligation d'un champ en indication explicite « (Obligatoire) ».
- [Ergonomie] Modification de l'apparence de pastilles de couleur.
- [CSS] Amélioration générale du fichier de CSS.
- [Accessibilité] Refonte de l'en-tête et du pied de page.
- [Traduction] L'ensemble des traductions de l'application ont été refaites et réorganisées.
- [Traitements] Augmentation du nombre de caractères maximums à 500 pour la durée de conservation.
- [AIPD] Augmentation du nombre de caractères maximums à 1000 pour divers champs dans un modèle et dans l'instruction d'une AIPD et transformation de ceux-ci en textarea.
- [Plan d'actions] Modification de l'action de visualisation en modification.
- [Duplication] Amélioration visuelle de l'interface d'avancement d'une duplication et ajout de l'information de la structure source.
- [Conformité de la structure] Les actions de protections associées sont cliquables vers leur visualisation.
- [AIPD] Les actions de protections associées sont cliquables vers leur visualisation.
- [URL] MAJ des URL de son compte et sa structure.
- [URL] MAJ des URL « editer » en « modifier ».
### Fix
- [Plan du site] Le fil d'Ariane est désormais présent pour l'ensemble des rôles.
- [AIPD] Le fil d'Ariane est désormais présent pour l'instruction d'une nouvelle AIPD.
- [Global] Corrections diverses d'accents et majuscules.
- [Accessibilité] Focus visible des liens et boutons.
- [Formulaire] Fix du bloc dans le formulaire de gestion des droits (modèles AIPD et référentiels) qui s'affichait mal dans certaines résolutions.
- [Conformité de la structure] Fix des blocs dans le formulaire de conformité qui s'affichaient mal dans certaines résolutions.
- [Preuve] Fix du message d'erreur du mauvais format d'un fichier.
- [Listes] Fix du z-index des datepicker.
- [Listes] Fix du responsive du tableau des listes.
- [Registre public] Fix affichage des coordonnées du responsable de traitement.
- [Registre public] Fix affichage des délais de conservation.
- [Registre public] Fix affichage des logiciels et supports.
### Suppression
- [Tableau de bord] Suppression de la colonne « Lien » dans le plan d’actions du tableau de bord gestionnaire et transfère du lien vers le nom de l'action.
- [Registre public] Suppression des liens (Sous-traitants, Structure, Preuves) dans la visualisation d'un traitement public.
### Migration
- [Migration] Ajout d'un fichier de migration pour gérer l'augmentation du nombre de caractères maximums des différents champs.

## [2.3.0] - 2023-09-14
### Changement
- Ajout du module de gestion des logiciels et supports.
- Ajout du module indice de maturité.
- Acessibilité : premier pas.
- Durées de conservation multiples pour les traitements.
- De nombreuses autres améliorations et correctifs.

## [2.0.2] - 2023-02-28
### Changement
- Ajout des parametres suivants dans le .env concernant les notifications
```
# number of unread notificaitons to display on the admin dashboard
APP_NOTIFICATION_DASHBOARD_NUMBER=15
# number of unread notificaitons to display on the page header
APP_NOTIFICATION_HEADER_NUMBER=5
# Whether to show notifications on the admin dashboard
APP_NOTIFICATION_DASHBOARD_SHOWN=true
# Number of days before a inactive user notification is sent
APP_INACTIVE_USER_NOTIFICATION_DELAY_DAYS=365
# Number of days before a late request notification is sent
APP_REQUEST_NOTIFICATION_DELAY_DAYS=365
# Number of days before a late Survey (indice de maturité) notificaiton is sent
APP_SURVEY_NOTIFICATION_DELAY_DAYS=365
# First line of notification emails
APP_NOTIFICATION_EMAIL_FIRST_LINE="Des modifications ont été apportées dans <a href='https://madis.fr'>Madis</a>"
```

## [2.0.1] - 2022-12-13
### Changement
- Ajout du parametre APP_URL dans le .env pour afficher les liens corrects dans les emails de notifications

## [2.0] - 2022-12-13
### Changement
- Mise à jour vers Symfony 5.4 et PHP 8.1
- Mise à jour vers nodejs 18
- Ajout du système de notifications
- Ajout de la connexion SSO

## [1.10] - 2022-04-15
### Changement
- Ajout du module Espace Documentaire

## [1.9] - 2022-03-25
### Changement
- Incorporation de tout le module AIPD : Modèle d'analyse, Mesure protection et Analyse d'impact

## [1.8.14] - 2022-03-25
### Changement
- Correction d'un bug sur le champs service dans la conformité des traitements
- Correction d'un bug permettant d'afficher les boutons d'actions pour un lecteur
- Correction d'un bug où des propriétés des actions de protections n'étaient pas dupliquées
- Correction d'un bug déconnectant l'utilisateur lors d'une tentative de modification de son propre profil
- Correction d'un bug empêchant de se connecter en tant que user avec des services supprimés

## [1.8.10] - 2022-03-14
### Changement
- Modification des services pour afficher tous les services aux utilisateurs n'appartenant à aucun

## [1.8.8] - 2022-03-10
### Changement
- Correction d'un bug empêchant l'administrateur de se connecter en un utilisateur d'une structures comportant des services supprimés

## [1.8.7] - 2022-03-04
### Changement
- Correction d'un bug affichant tous les services de la structure lors de la création d'éléments sans prise en compte des services de l'utilisateur
- Correction d'un bug empêchant les utilisateurs sans service de supprimer les services des éléments
- Correction d'une erreur 500 lors de la duplication d'éléments
- Correction d'un bug empêchant d'affecter des services lors de la création d'un utilisateur si la structure n'est pas celle par défaut du formulaire

## [1.8.6] - 2022-02-17
### Changement
- Correction d'un bug entraînant la multiplication du nombre d'éléments dupliqués
- Correction d'un bug affichant toujours les services comme inactifs lors de la modification d'une structure, empêchant de les désactiver

## [1.8.5] - 2022-02-15
### Changement
- Correction d'un bug affichant les services dans les éléments d'une structure n'ayant pas de services

## [1.8.4] - 2022-02-14
### Changement
- Correction d'une erreur entraînant la multiplication des éléments dupliqués
- Correction d'un bug affichant tous les services de toutes les structures sur certains éléments
- Ajout de la possibilité pour un utilisateur sans service de modifier tous les éléments de sa structure quelque soit le service
- Renommage "publique" en "public"

## [1.8.3] - 2022-02-02
### Changement
- Correction d'une erreur empêchant l'affichage de la liste des traitements

## [1.8.2] - 2022-01-27
### Changement
- Correction d'un bug rendant incorrect le nombre d'élément affiché dans le texte des tableaux #391
- Correction d'un bug permettant de rediriger vers un lien externe lorsqu'une balise était ajouté dans le nom d'un élément #392

## [1.8.1] - 2022-01-17
### Changement
- Corrections de diverses erreur mineures
- Registre public : suppression des liens

## [1.8] - 2022-01-05
### Changement
- Ajout des webservices
- Ajout du registre grand public
- Nombreuses améliorations ergonomiques diverses
- Nombreuses corrections et améliorations mineures

## [1.7.13] - 2021-12-22
### Changement
- Correction d'un bug concernant le calcul de l'indice de maturité #337
- Augmentation de la limite du nombre de caractère de la justification de refus des demandes #338
- Inversion des couleurs du graphique des sous-traitants #297
- Mise à jour de librairies suite à des vulnérabilités découvertes ou des changements de propriété (dont #380)

## [2.0] - 2022-12-13
### Changement
- Mise à jour vers Symfony 5.4 et PHP 8.1
- Mise à jour vers nodejs 18
- Ajout du système de notifications
- Ajout de la connexion SSO

## [1.10] - 2022-04-15
### Changement
- Ajout du module Espace Documentaire

## [1.9] - 2022-03-25
### Changement
- Incorporation de tout le module AIPD : Modèle d'analyse, Mesure protection et Analyse d'impact

## [1.8.14] - 2022-03-25
### Changement
- Correction d'un bug sur le champs service dans la conformité des traitements
- Correction d'un bug permettant d'afficher les boutons d'actions pour un lecteur
- Correction d'un bug où des propriétés des actions de protections n'étaient pas dupliquées
- Correction d'un bug déconnectant l'utilisateur lors d'une tentative de modification de son propre profil
- Correction d'un bug empêchant de se connecter en tant que user avec des services supprimés


## [1.8.10] - 2022-03-14
### Changement
- Modification des services pour afficher tous les services aux utilisateurs n'appartenant à aucun

## [1.8.8] - 2022-03-10
### Changement
- Correction d'un bug empêchant l'administrateur de se connecter en un utilisateur d'une collectivités comportant des services supprimés

## [1.8.7] - 2022-03-04
### Changement
- Correction d'un bug affichant tous les services de la collectivité lors de la création d'éléments sans prise en compte des services de l'utilisateur
- Correction d'un bug empêchant les utilisateurs sans service de supprimer les services des éléments
- Correction d'une erreur 500 lors de la duplication d'éléments
- Correction d'un bug empêchant d'affecter des services lors de la création d'un utilisateur si la collectivité n'est pas celle par défaut du formulaire

## [1.8.6] - 2022-02-17
### Changement
- Correction d'un bug entraînant la multiplication du nombre d'éléments dupliqués
- Correction d'un bug affichant toujours les services comme inactifs lors de la modification d'une collectivité, empêchant de les désactiver

## [1.8.5] - 2022-02-15
### Changement
- Correction d'un bug affichant les services dans les éléments d'une collectivité n'ayant pas de services

## [1.8.4] - 2022-02-14
### Changement
- Correction d'une erreur entraînant la multiplication des éléments dupliqués
- Correction d'un bug affichant tous les services de toutes les collectivités sur certains éléments
- Ajout de la possibilité pour un utilisateur sans service de modifier tous les éléments de sa collectivité quelque soit le service
- Renommage "publique" en "public"

## [1.8.3] - 2022-02-02
### Changement
- Correction d'une erreur empêchant l'affichage de la liste des traitements

## [1.8.2] - 2022-01-27
### Changement
- Correction d'un bug rendant incorrect le nombre d'élément affiché dans le texte des tableaux #391
- Correction d'un bug permettant de rediriger vers un lien externe lorsqu'une balise était ajouté dans le nom d'un élément #392

## [1.8.1] - 2022-01-17
### Changement
- Corrections de diverses erreur mineures
- Registre public : suppression des liens

## [1.8] - 2022-01-05
### Changement
- Ajout des webservices
- Ajout du registre grand public
- Nombreuses améliorations ergonomiques diverses
- Nombreuses corrections et améliorations mineures

## [1.7.13] - 2021-12-22
### Changement
- Correction d'un bug concernant le calcul de l'indice de maturité #337
- Augmentation de la limite du nombre de caractère de la justification de refus des demandes #338
- Inversion des couleurs du graphique des sous-traitants #297
- Mise à jour de librairies suite à des vulnérabilités découvertes ou des changements de propriété (dont #380)

## [1.7.12] - 2021-01-05
### Changement
- Correction d'un bug empêchant la suppression d'une action de protection ou d'un sous-traitant pour lesquels il existe un duplicat

## [1.7.11] - 2020-10-16
### Changement
- Correctif bug utilisateur archivé visible dans la liste "Ma structure" #295
- Dans le bilan chapitre 4.1 le nombre de domaine est désormais calculé
- Correctif sur la liste des plans d'actions qui affiche bien les actions de protections non planifiées et non appliquées

## [1.7.10] - 2020-10-12
### Changement
- Correctif des cases à cocher (Mesures de sécurité et confidentialité) lors de la modification d'un traitement.

## [1.7.9] - 2020-10-06
### Changement
- Ajout des liens pour accéder à la vue de consultation depuis la vue la liste d'un sous-traitant et d'une violation


## [1.7.8] - 2020-09-30
### Changement
- Correction sur le dashboard utilisateur et correction des placeholders sur la liste des structures.

## [1.7.7] - 2020-09-21
### Ajout
- Ajout du Lot 4
- Ajout d'un export de CSV des actions de protections, sous-traitants sur le dashboard
- Ajout du traitement côté serveur pour les sous-traitants, utilisateurs, demandes, structures, preuves et violations
- Ajout du référent multi-structures

## [1.7.6] - 2020-09-03
### Ajout
- Personnaliser les listes et les trier (Traitements, Actions de protection et Utilisateurs) #286
- Journalisation des actions, log, traçabilité #286
- Imprimer (PDF) une fiche (traitements, sous-traitants, ...) #286
### Changement
- Divers changements sur le dashboard des structures #285

## [1.7.5] - 2020-07-29
### Changement
- Divers changements suite à la mise en production du lot 2

## [1.7.4] - 2020-07-28
### Ajout
- [MODULE] Conformité des traitements, #279
- [MODULE] Conformité de la structure, #280

## [1.7.3] - 2020-10-06
### Ajout
- [MA COLLECTIVITE] Comité IL afficher dans le bilan, #258
- [BILAN] Ajout de paragraphes, #257
- [DASHBOARD] Cartographie, stats, export csv traitement, export csv structure, #255
- [DASHBOARD] Nombre de structures (par type)(par DPD), #225
### Changement
- [SOUS-TRAITANT] Fiche sous-traitants - traitements effectués par sous-traitant, #260
- [TRAITEMENTS] Fiche Traitements - Précision des personnes concernées, #259

## [1.6.3] - 2019-12-03
### Changement
- [TECHNIQUE] Mise à jour de PHP CS Fixer en v2.16
- [TECHNIQUE] Mise à jour de Symfony (de la version 4.2.8 à la version 4.3.8)
### Fix
- [TRAITEMENT] Il est désormais possible de supprimer un traitement ayant servi de template à la duplication, #221

## [1.6.2] - 2019-10-07
### Ajout
- [TECHNIQUE] Mise en place d'outils de qualité de code (PHPMD, PHPStan, csFixer, ...), #188
### Changement
- [TRAITEMENT] Word / Modification du "Personne référentes" en "Personnes concernées", #208

## [1.6.1] - 2019-09-23
### Fix
- [ADMINISTRATION] Subrogation / Créer une donnée avec l'admin en tant que créateur ne génère plus de 500, #207

## [1.6.0] - 2019-09-20
### Ajout
- [DUPLICATION] Un administrateur peut maintenant dupliquer des traitements / sous-traitants / actions de protections d'une structure vers des autres, #187
- [PREUVE] Possibilité de lier une preuve à une ou plusieurs données, #186
- [ADMINISTRATION] Subrogation d'un utilisateur de l'application, #107
- [GLOBAL] Ajout d'un DatePicker dans les formulaires pour les champs date, #37
### Changement
- [USER] La suppression (non fonctionnelle) a été remplacée par un archivage, #199
### Fix
- [COLLECTIVITE] Il est maintenant possible de supprimer le site web d'une structure, #202
- [PREUVE] Un administrateur peut maintenant télécharger les documents qui ne sont pas de sa structure, #197
- [GLOBAL] Passage des dates au format FR (DD/MM/YYYY) dans les listes, #37 #205

## [1.5.2] - 2019-07-27
### Fix
- [TABLEAU DE BORD] Le type de demande de personne concerné "Autre" ne fait plus planter le Dashboard, #195

## [1.5.1] - 2019-07-18
### Fix
- [DOCUMENTATION] Ajout de la table des matières dans le fichier README.md

## [1.5.0] - 2019-07-18
### Ajout
- [DOCUMENTATION] Création d'un dossier `doc` pour la documentation du projet (le fichier `README.md` en est le point d'entré), #184
- [PAGE CREDIT] Mise à jour du contenu et suppression de la partie "Hébergement", #179
- [PREUVE] Possibilité de supprimer une preuve, #178
- [TRAITEMENT] Ajout du champ "Personnes habilitées" dans le bloc "Mesures de sécurité", #177
- [TRAITEMENT] Ajout de la base légale "Intérêt légitime", #176
- [DEMANDE] Ajout de l'objet de demande "Autre", #130
### Changement
- [GLOBAL] Changement des entêtes de fichiers PHP pour mentionner la license AGPLv3, #181
- [TRAITEMENT] Renommage de la section "Mesures de sécurité" en "Mesures de sécurité et confidentialité", #177
- [DEMANDE] Le champ "Motif" est devenu facultatif, #167
### Fix
- [DEMANDE] Le champ "Réponse" ne retourne plus d'erreur s'il dépasse 255 caractères, #193
- [AUTHENTIFICATION] Je suis déconnecté si je suis resté 1h30 inactif, #185
- [DEMANDE] Afficher la personne concernée qui n'a pas de civilité est de nouveau fonctionnel, #173
- [MATURITE] Les questions sont dorénavant odonnées dans l'ordre alphabétique, #170
- [MATURITE] Le score de l'indice de maturité n'était pas calculé en cas d'édition, #169

## [1.4.3] - 2019-05-17
### Fix
- [BILAN] Correction d'une faute lexicale dans la partie 3.2, #162
- [WORD] Changement du Content-Type pour permettre le téléchargement sur iOS, #161
- [BILAN] Correction d'une faute d'orthographe dans la partie 3.3, #157

## [1.4.2] - 2019-05-06
### Changement
- [TECH] Mise à jour Symfony 4.2.2 à 4.2.8 + MAJ des vulnérabilités, #153
### Fix
- [CONNEXION] Modification de la durée de session et du temps d'invalidation de la session selon l'inactivité, #152

## [1.4.1] - 2019-04-03
### Fix
- [USER] Suppression du bouton "Retour" dans l'onglet "Mon compte" car il était inutile et pointait vers la liste des sous-traitants

## [1.4.0] - 2019-04-03
### Ajout
- [USER] Pouvoir modifier son mot de passe dans son profil, #135
- [CHARTE] Possibilité de cliquer sur un icone "oeil" dans les champs mot de passe pour le voir en clair, #126
- [TRAITEMENT] Ajout d'un champ "Observations", #121
- [TRAITEMENT] Ajout d'un champ "Origine des données", #117
- [LOGO] Pouvoir configurer les logos/le fournisseur de service et l'URL associé, #99
### Changement
- [TECH] Mise à jour Symfony 4.2.2 en 4.2.4 + autres packages (dont vulnérabilité Twig), #148
- [USER] Ré-agencement des blocs du formulaire "Utilisateurs" pour les admins, #135
- [CONNEXION] Passage du temps de connexion de 4h à 1h30, #125
- [TRAITEMENT] Passage en BDD de la liste des catégories de données (table `registry_treatment_data_category`), #105
- [TRAITEMENT] Remplacement de la catégorie de données "Etat civil" par "Nom, prénom", "Date et lieu de naissance", "Situation pro", #105
### Fix
- [TRAITEMENT] Le champ de formulaire "Délai de conservation" n'était pas bien aligné, #149
- [USER] Lors de la création d'un utilisateur, la saisie de son mot de passe n'était pas prise en compte, #147
- [TRAITEMENT] Le champ "Autre délai" ne s'affichait pas dans la visualisation d'un traitement, #144
- [TRAITEMENT] Le champ "Délai de conservation" n'étais pas traduit sur le word (on pouvait lire "month" par exemple), #144
- [USER] Modifier uniquement un mot de passe ne fonctionnait pas, #139
- [GLOBAL] La sidebar se décalait lorsque nous allions sur l'onglet "Ma structure", #139
- [USER] Le lecteur ne pouvait pas accéder aux infos de sa structure et son profil, #139
- [VIOLATION] Erreur d'affichage lors de la visualisation d'une violation qui n'a pas de champ notification renseigné, #139

## [1.3.1] - 2019-01-31
### Changement
- [TECH] Mise à jour Symfony 4.2.1 à 4.2.2, #133
### Fix
- [BILAN] Le DPD moral par défaut est bien chargé au lieu des coordonnés SOLURIS, #132

## [1.3.0] - 2018-12-12
### Changement
- [TECH] Mise à jour Symfony 3.4 à 4.2.1, #129
### Fix
- [BILAN] Le graphique de l'indice de maturité est maintenant bien ordonné, #128
- [GLOBAL] Les générations WORD acceptent maintenant les caractères spéciaux, #127

## [1.2.2] - 2018-11-16
### Fix
- [GLOBAL] Le code postal commencant par 0 ne fonctionnait pas pour les adresses du registre, #119

## [1.2.0] - 2018-11-07
### Changement
- [MATURITE] Les catégories sont ordonnées lors de la visualisation et la génération Word, #109
### Fix
- [GLOBAL] Le numéro de téléphone respecte maintenant les normes 0[1-9]XXXXXXXX, #119
- [GLOBAL] Le code postal peut maintenant commencer par 0 (07100 ne fonctionnait pas par exemple), #119
- [UTILISATEUR] Le mot de passe s'encode maintenant correctement lors de la création/édition, #113

## [1.1.0] - 2018-09-20
### Ajout
- [DOC] Ajout du README, #93
- [GLOBAL] Style - Ajout du favicon & logo Soluris, #41
### Changement
- [VIOLATION] Création - La date de la violation est par défaut la date du jour, #79
- [BILAN] Bilan global - Ajout du DPD dans le bilan s'il est différent de celui par défaut, #72
### Fix
- [BILAN] La génération LibreOffice n'affichait pas le document correctement, #90 #89 #88 #87
- [GLOBAL] Fix de typo, #92 #84 #80 #46
- [TABLEAU DE BORD] Ajout de la couleur pour le donuts des statuts des demandes (le statut "Incomplet" n'avait pas de couleur)
- [TABLEAU DE BORD] Les traitements inactifs ne sont plus comptabilisés dans les stats "Mesures de sécurité", #82
- [GLOBAL] Fil d'ariane - Les URLs qui poitaient vers la liste des structures n'en sont plus, #62

## [1.0.0] - 2018-08-29
### Ajout
- Release initiale
