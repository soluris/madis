<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240909093408 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE contractor_request (request_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', contractor_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', INDEX IDX_57C594D8427EB8A5 (request_id), INDEX IDX_57C594D8B0265DC7 (contractor_id), PRIMARY KEY(request_id, contractor_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tool_request (request_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', tool_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', INDEX IDX_2FF382DC427EB8A5 (request_id), INDEX IDX_2FF382DC8F7B22CC (tool_id), PRIMARY KEY(request_id, tool_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE request_violation (violation_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', request_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', INDEX IDX_AC4BB5457386118A (violation_id), INDEX IDX_AC4BB545427EB8A5 (request_id), PRIMARY KEY(violation_id, request_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tool_violation (violation_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', tool_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', INDEX IDX_425F30FD7386118A (violation_id), INDEX IDX_425F30FD8F7B22CC (tool_id), PRIMARY KEY(violation_id, tool_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE contractor_request ADD CONSTRAINT FK_57C594D8427EB8A5 FOREIGN KEY (request_id) REFERENCES registry_request (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE contractor_request ADD CONSTRAINT FK_57C594D8B0265DC7 FOREIGN KEY (contractor_id) REFERENCES registry_contractor (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tool_request ADD CONSTRAINT FK_2FF382DC427EB8A5 FOREIGN KEY (request_id) REFERENCES registry_request (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tool_request ADD CONSTRAINT FK_2FF382DC8F7B22CC FOREIGN KEY (tool_id) REFERENCES registry_tool (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE request_violation ADD CONSTRAINT FK_AC4BB5457386118A FOREIGN KEY (violation_id) REFERENCES registry_violation (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE request_violation ADD CONSTRAINT FK_AC4BB545427EB8A5 FOREIGN KEY (request_id) REFERENCES registry_request (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tool_violation ADD CONSTRAINT FK_425F30FD7386118A FOREIGN KEY (violation_id) REFERENCES registry_violation (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tool_violation ADD CONSTRAINT FK_425F30FD8F7B22CC FOREIGN KEY (tool_id) REFERENCES registry_tool (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE contractor_request');
        $this->addSql('DROP TABLE tool_request');
        $this->addSql('DROP TABLE request_violation');
        $this->addSql('DROP TABLE tool_violation');
    }
}
