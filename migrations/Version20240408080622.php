<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240408080622 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Update questions in several tables';
    }

    public function up(Schema $schema): void
    {
        // Update question in conformite_traitement_question table
        $this->addSql('UPDATE conformite_traitement_question SET question = "Information des personnes concernées : traitement loyal et transparent" WHERE question = "Information des personnes concernées (traitement loyal et transparent"');
        // Update question in aipd_analyse_question_conformite table
        $this->addSql('UPDATE aipd_analyse_question_conformite SET question = "Information des personnes concernées : traitement loyal et transparent" WHERE question = "Information des personnes concernées (traitement loyal et transparent"');
        // Update question in aipd_modele_question_conformite table
        $this->addSql('UPDATE aipd_modele_question_conformite SET question = "Information des personnes concernées : traitement loyal et transparent" WHERE question = "Information des personnes concernées (traitement loyal et transparent"');
    }

    public function down(Schema $schema): void
    {
        // Revert the updates by setting the questions back to their original values
        $this->addSql('UPDATE conformite_traitement_question SET question = "Information des personnes concernées (traitement loyal et transparent" WHERE question = "Information des personnes concernées : traitement loyal et transparent"');
        $this->addSql('UPDATE aipd_analyse_question_conformite SET question = "Information des personnes concernées (traitement loyal et transparent" WHERE question = "Information des personnes concernées : traitement loyal et transparent"');
        $this->addSql('UPDATE aipd_modele_question_conformite SET question = "Information des personnes concernées (traitement loyal et transparent" WHERE question = "Information des personnes concernées : traitement loyal et transparent"');
    }
}
