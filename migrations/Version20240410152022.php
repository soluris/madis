<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240410152022 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Update sections and questions in several tables for organization conformite';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        // Titre
        $this->addSql('UPDATE registry_conformite_organisation_processus SET nom = "Gestion de la documentation et des preuves"
            where position = 11');
        $this->addSql('UPDATE registry_conformite_organisation_processus SET nom = "Piloter le système de management de la protection des données"
            where position = 12');

        // Couleur
        $this->addSql('UPDATE registry_conformite_organisation_processus SET couleur = "info" where position = 1');
        $this->addSql('UPDATE registry_conformite_organisation_processus SET couleur = "success" where position = 2');
        $this->addSql('UPDATE registry_conformite_organisation_processus SET couleur = "primary" where position = 3');
        $this->addSql('UPDATE registry_conformite_organisation_processus SET couleur = "warning" where position = 4');
        $this->addSql('UPDATE registry_conformite_organisation_processus SET couleur = "info" where position = 5');
        $this->addSql('UPDATE registry_conformite_organisation_processus SET couleur = "success" where position = 6');
        $this->addSql('UPDATE registry_conformite_organisation_processus SET couleur = "primary" where position = 7');
        $this->addSql('UPDATE registry_conformite_organisation_processus SET couleur = "warning" where position = 8');
        $this->addSql('UPDATE registry_conformite_organisation_processus SET couleur = "info" where position = 9');
        $this->addSql('UPDATE registry_conformite_organisation_processus SET couleur = "success" where position = 10');
        $this->addSql('UPDATE registry_conformite_organisation_processus SET couleur = "primary" where position = 11');
        $this->addSql('UPDATE registry_conformite_organisation_processus SET couleur = "warning" where position = 12');

        // Description
        $this->addSql('UPDATE registry_conformite_organisation_processus SET description = "Disposer d’un registre des activités de traitement à jour. S\'assurer que les mesures de conformité des traitements des données à caractère personnel sont effectivement prises en compte en cas de traitements nouveaux ou modifiés. Gérer les transferts de données."
            where position = 2');
        $this->addSql('UPDATE registry_conformite_organisation_processus SET description = "En tant que responsable du traitement, s\'assurer de la conformité des contrats de sous-traitance. Si l\'entreprise agit en tant que sous-traitant, s\'assurer que le responsable du traitement a pris en compte ses obligations."
            where position = 4');
        $this->addSql('UPDATE registry_conformite_organisation_processus SET description = "En relation avec le service sécurité de l\'information de la structure, mettre en œuvre les mesures de protection sélectionnées afin de répondre aux objectifs de disponibilité, d’intégrité et de confidentialité des données à caractère personnel. Détecter les incidents de sécurité pouvant avoir pour conséquence une violation de données."
            where position = 6');
        $this->addSql('UPDATE registry_conformite_organisation_processus SET description = "S\'assurer que le personnel à qui ont été affectées les responsabilités définies dans le système de management de la protection des données, a les compétences nécessaires pour exécuter les tâches requises. S\'assurer que tout le personnel approprié a conscience de la pertinence et de l\'importance de ses activités liées aux traitements des données à caractère personnel."
            where position = 8');
        $this->addSql('UPDATE registry_conformite_organisation_processus SET description = "Connaître la réglementation applicable et ses évolutions. Être en capacité de répondre à des sollicitations ou à des poursuites. Réagir en cas de violations de données."
            where position = 9');
        $this->addSql('UPDATE registry_conformite_organisation_processus SET description = "À intervalles réguliers, vérifier que le système de management de la protection des données est conforme à la politique définie. Apporter la preuve que les traitements effectués par le responsable du traitement et les sous-traitants sont conformes au règlement."
            where position = 10');
        $this->addSql('UPDATE registry_conformite_organisation_processus SET description = "Gérer le patrimoine documentaire attaché à la protection des données. Établir et conserver des enregistrements pour apporter la preuve des mesures techniques et organisationnelles."
            where position = 11');
        $this->addSql('UPDATE registry_conformite_organisation_processus SET description = "Disposer d\'indicateurs pour piloter l\'efficacité des processus du système de management de la protection des données. Produire le bilan (rapport) annuel."
            where position = 12');

        // Question
        $this->allProcessus = $this->getData('SELECT * FROM registry_conformite_organisation_processus ORDER BY position ASC');

        foreach ($this->allProcessus as $processus) {
            switch ($processus['position']) {
                case 1:
                    $this->addSql('UPDATE registry_conformite_organisation_question SET nom = "La politique de gestion des données détaille les mesures techniques et organisationnelles définies par le responsable du traitement pour répondre dans le temps aux exigences du RGPD et pouvoir le démontrer : rôles et responsabilités, règles relatives aux processus du RGPD, procédures de gestion des droits de la personne, procédure en cas de violation de données, sommaire du bilan annuel..."
                        where processus_id = "' . $processus['id'] . '" and position = 4');
                    break;
                case 2:
                    $this->addSql('UPDATE registry_conformite_organisation_question SET nom = "La structure dispose d\'un registre des traitements. Le registre renseigne sur les critères minimum précisés à l\'article 30 du règlement : nom du responsable du traitement et du DPD, les finalités du traitement, les catégories de personnes concernées et les catégories de données à caractère personnel, les catégories de destinataires, les transferts de données à caractère personnel vers un pays tiers, les délais de conservation, une description générale des mesures de sécurité."
                        where processus_id = "' . $processus['id'] . '" and position = 1');
                    $this->addSql('UPDATE registry_conformite_organisation_question SET nom = "À chaque traitement est associé un gestionnaire. Tout nouveau traitement est signalé par le gestionnaire au DPD ou au référent DPD pour mise à jour du registre. Tout traitement ultérieur est porté à la connaissance du DPD ou du référent DPD pour avis puis partagé par la gouvernance."
                        where processus_id = "' . $processus['id'] . '" and position = 2');
                    break;
                case 3:
                    $this->addSql('UPDATE registry_conformite_organisation_question SET nom = "Les modalités de l\'exercice des droits sont communiquées à la personne concernée : Droit d’accès aux données (Art 15) ; Droit de rectification des données (Art 16) ; Droit d’effacement des données (Art 17) ; Droit de limitation du traitement (Art 18) ; Droit de portabilité des données (Art 20) ; Droit d’opposition au traitement (Art 21) ; Droit d’opposition au transfert de données (Art 46)."
                        where processus_id = "' . $processus['id'] . '" and position = 1');
                    break;
                case 4:
                    $this->addSql('UPDATE registry_conformite_organisation_question SET nom = "La structure dispose de la liste exhaustive des sous-traitants à qui elle a délégué des traitements de données à caractère personnel. Le gestionnaire de ces contrats est clairement identifié."
                        where processus_id = "' . $processus['id'] . '" and position = 3');
                    $this->addSql('UPDATE registry_conformite_organisation_question SET nom = "La structure surveille à intervalles réguliers la conformité des traitements de données à caractère personnel confiés à des sous-traitants. Pour tout changement majeur, les risques sont réappréciés."
                        where processus_id = "' . $processus['id'] . '" and position = 4');
                    break;
                case 5:
                    $this->addSql('UPDATE registry_conformite_organisation_question SET nom = "La structure partage une méthode de gestion de projet. Le lancement d\'un projet traitant de données à caractère personnel implique une information préalable auprès du DPD ou du référent DPD. La note de cadrage définit la licéité du traitement, précise les droits que la personne concernée pourra exercer et renseigne sur la finalité, la minimisation des données, la limitation des durées de conservation."
                        where processus_id = "' . $processus['id'] . '" and position = 1');
                    $this->addSql('UPDATE registry_conformite_organisation_question SET nom = "La procédure de livraison du projet permet au DPD ou référent DPD de vérifier qu\'il est conforme à la réglementation et que les procédures visant à permettre à la personne concernée d\'exercer ses droits sont opérationnelles."
                        where processus_id = "' . $processus['id'] . '" and position = 4');
                    break;
                case 6:
                    $this->addSql('UPDATE registry_conformite_organisation_question SET nom = "La structure dispose d\'un document (Politique de sécurité du système d\'information - PSSI) qui rassemble l\'ensemble des mesures de sécurité appliquées."
                        where processus_id = "' . $processus['id'] . '" and position = 1');
                    $this->addSql('UPDATE registry_conformite_organisation_question SET nom = "L\'accès physique aux locaux contenant des équipements informatiques (serveurs, équipements du réseau) est contrôlé. Seules les personnes autorisées peuvent y avoir accès. Les armoires et coffres-forts contenant des données sensibles sont systématiquement fermés à clé."
                        where processus_id = "' . $processus['id'] . '" and position = 4');
                    $this->addSql('UPDATE registry_conformite_organisation_question SET nom = "Tous les équipements (Postes de travail, smartphones, objets connectés) sont protégés par un antivirus géré par une console centralisée. Les patchs logiciels sont gérés pour éviter les \"trous\" de sécurité. Les postes sont verrouillés en fin de session."
                        where processus_id = "' . $processus['id'] . '" and position = 5');
                    $this->addSql('UPDATE registry_conformite_organisation_question SET nom = "Lorsque des mobiles multifonctions (smartphone), ordinateurs portables ou clé USB sont utilisés, leur usage est encadré. Les utilisateurs en situation de mobilité sont sensibilisés au vol, à l\'écoute et à l\'observation passive. Lorsque cela est justifié, les données sont chiffrées sur les équipements ou supports mobiles."
                        where processus_id = "' . $processus['id'] . '" and position = 6');
                    $this->addSql('UPDATE registry_conformite_organisation_question SET nom = "L\'ensemble des données est sauvegardé. Des tests de restauration sont planifiés et réalisés pour vérifier l\'intégrité des données sauvegardées. Les sauvegardes sont externalisées dans un lieu sécurisé."
                        where processus_id = "' . $processus['id'] . '" and position = 7');
                    $this->addSql('UPDATE registry_conformite_organisation_question SET nom = "L\'ensemble des utilisateurs est sensibilisé à la sécurité informatique, à la protection de la vie privée. Les utilisateurs sont formés aux outils et vigilant dans leurs usages pour éviter les erreurs de manipulation. La charte informatique est signée par chaque utilisateur."
                        where processus_id = "' . $processus['id'] . '" and position = 8');
                    $this->addSql('UPDATE registry_conformite_organisation_question SET nom = "La politique de mot de passe s\'applique à l\'accès au réseau et aux applications. Elle précise la force du mot de passe : 12 caractères ou plus, un nombre, une majuscule, un signe de ponctuation ou un caractère spécial (dollar, dièse, ...). Les mots de passe sont gardés secret par chacun des utilisateurs."
                        where processus_id = "' . $processus['id'] . '" and position = 10');
                    $this->addSql('UPDATE registry_conformite_organisation_question SET nom = "La mise au rebut des supports numériques est contrôlée. Les données sont effacées avant le recyclage."
                        where processus_id = "' . $processus['id'] . '" and position = 12');
                    $this->addSql('UPDATE registry_conformite_organisation_question SET nom = "Les services en ligne, les échanges ou transferts sur les réseaux sont protégés contre l\'interception (Chiffrement). Les configurations sont systématiquement contrôlées après la mise à jour d\'un service en ligne."
                        where processus_id = "' . $processus['id'] . '" and position = 13');
                    break;
                case 7:
                    $this->addSql('UPDATE registry_conformite_organisation_question SET nom = "La structure dispose d\'une méthode formalisée d\'analyse de risque notamment celle proposée par la CNIL. Si d\'autres méthodes de sécurité globale existent, elles peuvent être mises en cohérence avec la protection des données (ISO 31000, ISO 27005, ISO 27000, EBIOS, HAZOP, HACCP, MÉHARI). Dans ce cas, une vigilance devra être apportée, car ces dernières ne couvrent pas l\'ensemble des mesures permettant la protection des données (par exemple au niveau juridique)."
                        where processus_id = "' . $processus['id'] . '" and position = 1');
                    $this->addSql('UPDATE registry_conformite_organisation_question SET nom = "La structure a déjà procédé à une ou plusieurs analyses de risques de son système d\'information. Elle dispose d\'une compétence interne ou externe pouvant réaliser des analyses d\'impacts sur la protection des données."
                        where processus_id = "' . $processus['id'] . '" and position = 2');
                    $this->addSql('UPDATE registry_conformite_organisation_question SET nom = "Conformément à l\'article 35 du RGPD et aux lignes directrices portant sur l\'AIPD, les nouveaux traitements de données à caractère personnel \"à risque élevé\" pour les droits et libertés des personnes comme par exemple de contrôle des personnes, d\'évaluations systématiques font l\'objet d\'une analyse d\'impact. La structure a pris connaissance des listes publiées par la CNIL pour lesquelles les analyses sont obligatoires ou non. Dans le cas où le traitement ne figurerait pas sur cette liste, elle applique la règle des critères déterminants l\'analyse (Cf. Infographie \"Dois-je faire une AIPD\")."
                        where processus_id = "' . $processus['id'] . '" and position = 3');
                    $this->addSql('UPDATE registry_conformite_organisation_question SET nom = "Les résultats des analyses sont documentés, partagés avec le DPD ou le référent DPD. Ils font l\'objet d\'un plan d\'action formalisé et approuvé par le responsable de traitement. Le DPD ou référent RGPD contrôle la réalisation du plan de traitement."
                        where processus_id = "' . $processus['id'] . '" and position = 4');
                    break;
                case 8:
                    $this->addSql('UPDATE registry_conformite_organisation_question SET nom = "Les catégories de collaborateurs directement impliqués dans les traitements de données à caractère personnel bénéficient de formations ou des sensibilisations spécifiques centrées sur les principes qui les concernent : gestion du consentement, privacy by design, privacy by default, protection contre les traitements non autorisés, exactitude, gestion des droits, violation de données."
                        where processus_id = "' . $processus['id'] . '" and position = 2');
                    $this->addSql('UPDATE registry_conformite_organisation_question SET nom = "Les messages essentiels de la protection des données à caractère personnel sont repris régulièrement sous la forme de campagne d\'affichages, de mails, d\'intervention dans les réunions ou sur les réseaux internes. Les opérations de sensibilisation, de formation et de communication sont mesurées et archivées. Les obligations de l\'utilisateur en matière de données à caractère personnel sont précisées dans la charte informatique."
                        where processus_id = "' . $processus['id'] . '" and position = 4');
                    break;
                case 9:
                    $this->addSql('UPDATE registry_conformite_organisation_question SET nom = "La structure a clairement identifié une expertise juridique interne ou externe, spécialisée dans le RGPD et dans ses périmètres connexes comme par exemple les spécifications juridiques locales."
                        where processus_id = "' . $processus['id'] . '" and position = 2');
                    break;
                case 10:
                    $this->addSql('UPDATE registry_conformite_organisation_question SET nom = "Conformément à l\'article 32 du RGPD, la structure teste, analyse, et évalue l\'efficacité des mesures techniques et organisationnelles, pour assurer la sécurité du traitement. Les mesures de sécurité en prévention de la violation de données à caractère personnel sont contrôlées régulièrement et sont appliquées en cas de violation de données."
                        where processus_id = "' . $processus['id'] . '" and position = 1');
                    break;
                case 11:
                    $this->addSql('UPDATE registry_conformite_organisation_question SET nom = "Le DPD ou le référent DPD a établi une liste des documents à produire comme par exemple : politique générale de protection des données, politique de gestion des données, compte rendu de gouvernance, registre des traitements, procédures relative à l\'exercice des droits de la personne concernée, modèle de cadrage d\'un projet privacy by design, politique de sécurité."
                        where processus_id = "' . $processus['id'] . '" and position = 2');
                    $this->addSql('UPDATE registry_conformite_organisation_question SET nom = "Conformément à l\'article 47 du RGPD, les groupes de collectivités engagées dans une activité publique conjointe ayant définis des codes de conduite, ou des règles contraignantes doivent préciser la procédure de validation et de révision de leur contenu par le responsable du traitement."
                        where processus_id = "' . $processus['id'] . '" and position = 3');
                    $this->addSql('UPDATE registry_conformite_organisation_question SET nom = "Les documents à valeur de preuve sont enregistrés (signés par le responsable du traitement). Ils sont protégés contre les pertes, les divulgations, les falsifications et les accès non autorisés. La liste des documents à valeur de preuve est disponible et tenue à jour (Liste des preuves)."
                        where processus_id = "' . $processus['id'] . '" and position = 4');
                    break;
                case 12:
                    $this->addSql('UPDATE registry_conformite_organisation_question SET nom = "S\'il existe déjà une organisation interne basée sur les processus, elle communique au DPD pour qu\'il adapte l\'accompagnement. La cartographie des processus de gestion des données détaille les éléments d\'entrées et de sorties et mentionne les objectifs, les rôles et responsabilités, les activités."
                        where processus_id = "' . $processus['id'] . '" and position = 1');
                    $this->addSql('UPDATE registry_conformite_organisation_question SET nom = "L\'ensemble des actions sont consolidées dans un seul et même tableau afin de s\'assurer de leur réalisation."
                        where processus_id = "' . $processus['id'] . '" and position = 2');
                    $this->addSql('UPDATE registry_conformite_organisation_question SET nom = "Le DPD ou le référent DPD a mis en place un tableau de bord qui comprend des indicateurs permettant de mesurer la conformité au règlement et les progrès réalisés. Les indicateurs sont simples à comprendre et partageable avec les membres de la gouvernance. Exemple d\'indicateurs : maturité des processus du système de gestion, ratio d\'actions réalisées et à réaliser, ratio nombre de traitement interne/sous-traités."
                        where processus_id = "' . $processus['id'] . '" and position = 3');
                    $this->addSql('UPDATE registry_conformite_organisation_question SET nom = "Conformément aux lignes directrices relatives au DPD, le DPD ou le référent DPD produit un bilan annuel (Rapport annuel) partagé avec la structure de gouvernance et validé par le responsable du traitement. Ce rapport comprend entre autres : bilan de conformité des traitements, bilan des opérations de sensibilisation, bilan des AIPD, bilan du plan d\'actions."
                        where processus_id = "' . $processus['id'] . '" and position = 4');
                    break;
            }
        }
    }

    public function down(Schema $schema): void
    {
    }

    private function getData(string $sql): array
    {
        $stmt = $this->connection->query($sql);

        return $stmt->fetchAll();
    }
}
