<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20241028155109 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE contractor_contractor (a_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', b_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', INDEX IDX_1AFDB2843BDE5358 (a_id), INDEX IDX_1AFDB284296BFCB6 (b_id), PRIMARY KEY(a_id, b_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE mesurement_mesurement (a_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', b_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', INDEX IDX_97A5EFF03BDE5358 (a_id), INDEX IDX_97A5EFF0296BFCB6 (b_id), PRIMARY KEY(a_id, b_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE proof_proof (a_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', b_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', INDEX IDX_C56D3EE53BDE5358 (a_id), INDEX IDX_C56D3EE5296BFCB6 (b_id), PRIMARY KEY(a_id, b_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE request_request (a_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', b_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', INDEX IDX_11B3FDC83BDE5358 (a_id), INDEX IDX_11B3FDC8296BFCB6 (b_id), PRIMARY KEY(a_id, b_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tool_tool (a_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', b_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', INDEX IDX_FBB17E9A3BDE5358 (a_id), INDEX IDX_FBB17E9A296BFCB6 (b_id), PRIMARY KEY(a_id, b_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE treatment_treatment (a_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', b_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', INDEX IDX_8A247F293BDE5358 (a_id), INDEX IDX_8A247F29296BFCB6 (b_id), PRIMARY KEY(a_id, b_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE violation_violation (a_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', b_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', INDEX IDX_9B7E384E3BDE5358 (a_id), INDEX IDX_9B7E384E296BFCB6 (b_id), PRIMARY KEY(a_id, b_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE contractor_contractor ADD CONSTRAINT FK_1AFDB2843BDE5358 FOREIGN KEY (a_id) REFERENCES registry_contractor (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE contractor_contractor ADD CONSTRAINT FK_1AFDB284296BFCB6 FOREIGN KEY (b_id) REFERENCES registry_contractor (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE mesurement_mesurement ADD CONSTRAINT FK_97A5EFF03BDE5358 FOREIGN KEY (a_id) REFERENCES registry_mesurement (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE mesurement_mesurement ADD CONSTRAINT FK_97A5EFF0296BFCB6 FOREIGN KEY (b_id) REFERENCES registry_mesurement (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE proof_proof ADD CONSTRAINT FK_C56D3EE53BDE5358 FOREIGN KEY (a_id) REFERENCES registry_proof (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE proof_proof ADD CONSTRAINT FK_C56D3EE5296BFCB6 FOREIGN KEY (b_id) REFERENCES registry_proof (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE request_request ADD CONSTRAINT FK_11B3FDC83BDE5358 FOREIGN KEY (a_id) REFERENCES registry_request (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE request_request ADD CONSTRAINT FK_11B3FDC8296BFCB6 FOREIGN KEY (b_id) REFERENCES registry_request (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tool_tool ADD CONSTRAINT FK_FBB17E9A3BDE5358 FOREIGN KEY (a_id) REFERENCES registry_tool (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tool_tool ADD CONSTRAINT FK_FBB17E9A296BFCB6 FOREIGN KEY (b_id) REFERENCES registry_tool (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE treatment_treatment ADD CONSTRAINT FK_8A247F293BDE5358 FOREIGN KEY (a_id) REFERENCES registry_treatment (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE treatment_treatment ADD CONSTRAINT FK_8A247F29296BFCB6 FOREIGN KEY (b_id) REFERENCES registry_treatment (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE violation_violation ADD CONSTRAINT FK_9B7E384E3BDE5358 FOREIGN KEY (a_id) REFERENCES registry_violation (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE violation_violation ADD CONSTRAINT FK_9B7E384E296BFCB6 FOREIGN KEY (b_id) REFERENCES registry_violation (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE contractor_contractor');
        $this->addSql('DROP TABLE mesurement_mesurement');
        $this->addSql('DROP TABLE proof_proof');
        $this->addSql('DROP TABLE request_request');
        $this->addSql('DROP TABLE tool_tool');
        $this->addSql('DROP TABLE treatment_treatment');
        $this->addSql('DROP TABLE violation_violation');
    }
}
