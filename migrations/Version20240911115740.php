<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240911115740 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Supprimer les doublons sur le champ siren d une collectivité et rendre le champ nullable et unique';
    }

    public function up(Schema $schema): void
    {
        // rendre le champ Siren nullable
        $this->addSql('ALTER TABLE user_collectivity MODIFY siren VARCHAR(14) NULL;');

        // Supprimer les doublons Siren
        $this->addSql('
        CREATE TEMPORARY TABLE tmp_user_collectivity_ids AS
        SELECT MIN(uc2.id) as id
        FROM user_collectivity uc2
        WHERE uc2.siren IS NOT NULL
        GROUP BY uc2.siren
    ');

        $this->addSql('
        UPDATE user_collectivity uc1
        SET uc1.siren = NULL
        WHERE uc1.id NOT IN (SELECT id FROM tmp_user_collectivity_ids)
    ');

        // rendre unique le champ siren
        $this->addSql('
        ALTER TABLE user_collectivity
        ADD CONSTRAINT unique_siren UNIQUE (siren)
    ');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('
        ALTER TABLE user_collectivity
        DROP CONSTRAINT unique_siren
    ');

        $this->addSql('
        ALTER TABLE user_collectivity 
        CHANGE siren siren INT NULL
    ');
    }
}
