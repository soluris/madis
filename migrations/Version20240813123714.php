<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use App\Domain\User\Model\ReviewData;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Ramsey\Uuid\Uuid;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240813123714 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE user_review_data (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', collectivity_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', document_name VARCHAR(255) NOT NULL, logo VARCHAR(512) DEFAULT NULL, sections LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', created_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', UNIQUE INDEX UNIQ_E65C77EEBD56F776 (collectivity_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user_review_data ADD CONSTRAINT FK_E65C77EEBD56F776 FOREIGN KEY (collectivity_id) REFERENCES user_collectivity (id)');
        $this->addSql('ALTER TABLE user_review_data ADD show_collectivity_logo_footer TINYINT(1) NOT NULL, ADD show_dpd_logo_footer TINYINT(1) NOT NULL');
        // create empty review data for each collectivity
        $sql    = 'SELECT id, name FROM user_collectivity';
        $stmt   = $this->connection->executeQuery($sql);
        $result = $stmt->fetchAllAssociative();

        foreach ($result as $col) {
            $this->addSql("
            INSERT INTO user_review_data
            (id, collectivity_id, document_name, sections, show_collectivity_logo_footer, show_dpd_logo_footer)
            VALUES ('" . Uuid::uuid4() . "', '" . $col['id'] . "', 'Bilan de gestion des données à caractère personnel', '" . serialize([
                ReviewData::TREATMENT_REGISTRY,
                ReviewData::CONTRACTOR_REGISTRY,
                ReviewData::TOOL_REGISTRY,
                ReviewData::REQUEST_REGISTRY,
                ReviewData::VIOLATION_REGISTRY,
                ReviewData::CONFORMITY_EVALUATION,
                ReviewData::TREATMENT_CONFORMITY,
                ReviewData::AIPD,
                ReviewData::COLLECTIVITY_CONFORMITY,
                ReviewData::PROTECT_ACTIONS,
                ReviewData::CONTINUOUS_AMELIORATION,
                ReviewData::PROOF_LIST,
                // ReviewData::SUIVI,
                ReviewData::USER_LIST,
            ]) . "', false, false)

            ");
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE user_review_data');
    }
}
