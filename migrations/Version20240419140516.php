<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240419140516 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Insert data into registry_treatment_data_category if empty';
    }

    public function up(Schema $schema): void
    {
        // Check if registry_treatment_data_category table is empty
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');
        $sql   = 'SELECT COUNT(*) as count FROM registry_treatment_data_category';
        $stmt  = $this->connection->query($sql);
        $count = (int) $stmt->fetchColumn();

        if (0 === $count) {
            // Insert data into registry_treatment_data_category
            $this->addSql("
                INSERT INTO registry_treatment_data_category
                VALUES
                ('bank','Information bancaire',7,0),
                ('bank-situation','Situation bancaire',8,0),
                ('birth','Date, lieu de naissance',2,0),
                ('caf','Numéro de CAF',17,0),
                ('connexion','Connexion',15,0),
                ('earning','Revenus',12,0),
                ('email','Emails',13,0),
                ('family-situation','Situation familiale',3,0),
                ('filiation','Filiation',4,0),
                ('geo','Géolocalisation',16,0),
                ('health','Santé',18,1),
                ('heritage','Patrimoine',9,0),
                ('identity','Pièces d’identité',20,0),
                ('ip','Adresse IP',14,0),
                ('name','Nom, Prénom',1,0),
                ('phone','Coordonnées téléphoniques',6,0),
                ('picture','Photos-vidéos',21,0),
                ('postal','Coordonnées postales',5,0),
                ('professional-situation','Situation professionnelle',11,0),
                ('public-religious-opinion','Opinion politique ou religieuse',23,1),
                ('racial-ethnic-opinion','Origine raciale ou ethnique',24,1),
                ('sex-life','Vie sexuelle',25,1),
                ('social-security-number','Numéro de Sécurité Sociale',19,1),
                ('syndicate','Appartenance Syndicale',22,1),
                ('tax-situation','Situation fiscale',10,0)
            ");
        }
    }

    public function down(Schema $schema): void
    {
        // Down migration not needed for this scenario
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('DELETE FROM registry_treatment_data_category');
    }
}
