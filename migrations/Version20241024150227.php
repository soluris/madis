<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20241024150227 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('ALTER TABLE registry_mesurement ADD service_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE registry_mesurement ADD CONSTRAINT FK_9CFD1BFAED5CA9E6 FOREIGN KEY (service_id) REFERENCES registry_service (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_9CFD1BFAED5CA9E6 ON registry_mesurement (service_id)');
        $this->addSql('ALTER TABLE tool_mesurement DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE tool_mesurement ADD PRIMARY KEY (mesurement_id, tool_id)');
        $this->addSql('ALTER TABLE registry_proof ADD service_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE registry_proof ADD CONSTRAINT FK_5982E8EDED5CA9E6 FOREIGN KEY (service_id) REFERENCES registry_service (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_5982E8EDED5CA9E6 ON registry_proof (service_id)');
        $this->addSql('ALTER TABLE tool_proof DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE tool_proof ADD PRIMARY KEY (proof_id, tool_id)');
        $this->addSql('ALTER TABLE registry_tool ADD service_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE registry_tool ADD CONSTRAINT FK_3C2A7CF2ED5CA9E6 FOREIGN KEY (service_id) REFERENCES registry_service (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_3C2A7CF2ED5CA9E6 ON registry_tool (service_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('ALTER TABLE registry_mesurement DROP FOREIGN KEY FK_9CFD1BFAED5CA9E6');
        $this->addSql('DROP INDEX IDX_9CFD1BFAED5CA9E6 ON registry_mesurement');
        $this->addSql('ALTER TABLE registry_mesurement DROP service_id');
        $this->addSql('ALTER TABLE registry_proof DROP FOREIGN KEY FK_5982E8EDED5CA9E6');
        $this->addSql('DROP INDEX IDX_5982E8EDED5CA9E6 ON registry_proof');
        $this->addSql('ALTER TABLE registry_proof DROP service_id');
        $this->addSql('ALTER TABLE registry_tool DROP FOREIGN KEY FK_3C2A7CF2ED5CA9E6');
        $this->addSql('DROP INDEX IDX_3C2A7CF2ED5CA9E6 ON registry_tool');
        $this->addSql('ALTER TABLE registry_tool DROP service_id');
    }
}
