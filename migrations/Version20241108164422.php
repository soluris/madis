<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20241108164422 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Update questions in several tables';
    }

    public function up(Schema $schema): void
    {
        // Update question in conformite_traitement_question table
        $this->addSql('UPDATE conformite_traitement_question SET question = "Finalités : déterminées, explicites et légitimes" WHERE question = "Finalités : déterminées, explicites et légitime"');
        // Update question in aipd_analyse_question_conformite table
        $this->addSql('UPDATE aipd_analyse_question_conformite SET question = "Finalités : déterminées, explicites et légitimes" WHERE question = "Finalités : déterminées, explicites et légitime"');
        // Update question in aipd_modele_question_conformite table
        $this->addSql('UPDATE aipd_modele_question_conformite SET question = "Finalités : déterminées, explicites et légitimes" WHERE question = "Finalités : déterminées, explicites et légitime"');
    }

    public function down(Schema $schema): void
    {
        // Revert the updates by setting the questions back to their original values
        $this->addSql('UPDATE conformite_traitement_question SET question = "Finalités : déterminées, explicites et légitime" WHERE question = "Finalités : déterminées, explicites et légitimes"');
        $this->addSql('UPDATE aipd_analyse_question_conformite SET question = "Finalités : déterminées, explicites et légitime" WHERE question = "Finalités : déterminées, explicites et légitimes"');
        $this->addSql('UPDATE aipd_modele_question_conformite SET question = "Finalités : déterminées, explicites et légitime" WHERE question = "Finalités : déterminées, explicites et légitimes"');
    }
}
