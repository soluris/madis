# Madis

L'application Madis est une application pensée par [Soluris](https://www.soluris.fr) pour vous accompagner dans la gestion du Règlement Général à la Protection des Données (RGPD).

## Table des matières

- [Licence d'utilisation de Madis](LICENSE.md)
- [Installation de Madis](doc/installation#madis-installation)
    - [Installer Madis par script](doc/installation/1-installation-script.md#installer-madis-par-script)
    - [Installer Madis manuellement](doc/installation/2-installation-manuelle.md#installation-manuelle)
- [Journal des modifications (Changelog)](CHANGELOG.md#changelog)
- [Documentation utilisateur](https://documentation-madis.readthedocs.io/fr/latest/docutilisateur.html)
- [Maintenance et gestion applicative](doc/maintenance-et-gestion-application#maintenance-et-gestion-applicative)
    - [Paramétrage complémentaire et .env](doc/maintenance-et-gestion-application/1-parametrages-complementaires-et-env.md#paramétrage-complémentaire-et-env)
    - [Mettre à jour Madis](doc/maintenance-et-gestion-application/2-mettre-a-jour-madis.md#mettre-à-jour-madis)
    - [Requêtes SQL](doc/maintenance-et-gestion-application/3-requetes-sql.md#requêtes-sql)
- [Documentation technique développeurs](doc/developpement#madis-documentation-technique-développeurs)
    - [Lancer la stack de développement](doc/developpement/1-lancer-stack-developpement.md#lancer-la-stack-de-développement)
    - [Architecture et qualité](doc/developpement/2-architecture-et-qualite.md#architecture-et-qualité)
    - [Déployer une nouvelle version](doc/developpement/3-deployer-une-nouvelle-version.md#déployer-une-nouvelle-version)
- [Contribuer au projet](CONTRIBUTING.md#contribution)
