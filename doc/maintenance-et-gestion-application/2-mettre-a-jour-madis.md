Mettre à jour Madis
==============================

1. [Monter de version](#1-monter-de-version)
2. [Rétrograder de version](#2-rétrograder-de-version)
    - [Cas 1 : Il n'y a pas eu de migration de données, ou des données ont migré sans perte](#cas-1--il-ny-a-pas-eu-de-migration-de-données-ou-des-données-ont-migré-sans-perte)
    - [Cas 2 : Je souhaite rétrograder une migration précise](#cas-2--je-souhaite-rétrograder-une-migration-précise)
3. [Problèmes connus de mise à jour](#3-problèmes-connus-de-mise-à-jour)
    - [Des fichiers sont en cours de modification](#des-fichiers-sont-en-cours-de-modification)

Si vous êtes amenés à mettre à jour Madis. Vous pouvez consulter le fichier [CHANGELOG.md](../../CHANGELOG.md#changelog) pour prendre connaissance des changements qui ont eu lieu dans le code entre les versions.

## 1. Monter de version

Pour mettre à jour votre dépôt de code, commencer par faire une sauvegarde de la base de données actuelle afin de prévenir d'éventuels problèmes lors de la mise à jour.
```shell
mysqldump -u madis -pMonMotDePasse madis > madis_bdd_DATE.sql
```

Se placer dans le dossier de Madis.
```shell
cd /var/www/madis
```

Pour prendre connaissance des modifications Git.
```shell
sudo -u www-data git fetch -p
```

Pour consulter la liste des versions disponibles.
```shell
sudo -u www-data git tag
```

Aller sur la version de votre choix (ici v2.4.4).
```shell
sudo -u www-data git checkout v2.4.4
```

Enfin, lancez la commande suivante pour mettre à jour Madis.
```shell
sudo -u www-data ./bin/deploy
```

## 2. Rétrograder de version

**Attention : Avant toute utilisation de ce qui suit, assurez-vous d'avoir effectué une sauvegarde de votre base de données, voire de contacter une personne du Support Madis pour vous aiguiller. Toute intervention non maîtrisée peut amener à de la perte de données.**

Vous pourrez être confronté au besoin de rétrograder Madis à une version antérieure. Cependant, il peut y avoir eu des modifications de votre base de données durant la montée de version.

### Cas 1 : Il n'y a pas eu de migration de données, ou des données ont migré sans perte

Il suffit uniquement de remettre le code applicatif souhaité.

Faire une sauvegarde de la base de données actuelle afin de prévenir d'éventuels problèmes lors de la mise à jour.
```shell
mysqldump -u madis -pMonMotDePasse madis > madis_bdd_DATE.sql
```

Se placer dans le dossier de Madis.
```shell
cd /var/www/madis
```

Pour consulter la liste des versions disponibles.
```shell
sudo -u www-data git tag
```

Je suis en v2.4.4 et je souhaite revenir en v2.4.3, de ce fait, je retourne sur la version souhaitée.
```shell
sudo -u www-data git checkout v2.4.3
```

Je relance la mise à jour, qui va me réinstaller l'ancien code.
```shell
sudo -u www-data ./bin/deploy
```

### Cas 2 : Je souhaite rétrograder une migration précise

Les scripts Madis permettent de rejouer les scénarios inverses.

Faire une sauvegarde de la base de données actuelle afin de prévenir d'éventuels problèmes lors de la mise à jour.
```shell
mysqldump -u madis -pMonMotDePasse madis > madis_bdd_DATE.sql
```

Se placer dans le dossier de Madis.
```shell
cd /var/www/madis
```

Regarder les migrations possibles.
```shell
ls src/Application/Migrations
```

Une liste apparaît de toutes les migrations de base de données qui ont été créées depuis le lancement de Madis. Il me faut donc isoler la ou les migrations à rétrograder. Les fichiers sont formatés de la manière suivante : Années-Mois-Jours-Heures-Minutes-Secondes.

Exemple :
- `Version20240419140516.php -- v2.2.5`
- `Version20240410152022.php -- v2.2.5`
- `Version20240408080622.php -- v2.2.5`
- `Version20240219091346.php -- v2.2.4`

Je vais donc lancer les scripts inverses dans l'ordre décroissant de création pour migrer les données à leur état initial.
```shell
sudo -u www-data bin/console doctrine:migration:execute --down 20240419140516
```

Enfin, lancez la commande suivante pour réinstaller l'ancien code applicatif.
```shell
sudo -u www-data ./bin/deploy
```

## 3. Problèmes connus de mise à jour

### Des fichiers sont en cours de modification

Des fichiers ont été modifiés, le checkout est avorté.
```shell
sudo -u www-data git checkout v2.4.4
error: Your local changes to the following files would be overwritten by checkout:
        .env.dist
        config/domain/user/translations/messages.fr.yaml
        src/Domain/Reporting/Generator/Word/AbstractGenerator.php
Please, commit your changes or stash them before you can switch branches.
Aborting
```

Consulter les différences entre les fichiers.
```shell
sudo -u www-data git diff
```

Abandonner les modifications.
```shell
sudo -u www-data git reset --hard
```

Relancer le checkout.
```shell
sudo -u www-data git checkout v2.4.4
```
