Paramétrage complémentaire et .env
============================

1. [Configuration générale](#1-configuration-générale)
2. [Sécurisation de Madis](#2-sécurisation-de-madis)
3. [Gérer la taille des pièces jointes déposées](#3-gérer-la-taille-des-pièces-jointes-déposées)
4. [Configuration des images Madis](#4-configuration-des-images-madis)
5. [Module notifications](#5-module-notifications)
6. [Débugguer Madis](#6-débugguer-madis)
7. [Configuration SSO](#7-configuration-sso)
8. [Autres variables configurables](#8-autres-variables-configurables)

## 1. Configuration générale

Nom de l'application.
```bash
APP_APPLICATION_NAME="Madis"
```

Nom du référent RGPD par défaut.
```bash
APP_DEFAULT_REFERENT="Référent RGPD"
```

Adresse du DPD par défaut. Si vous êtes une structure de mutualisation assurant le rôle de DPD, saisissez votre adresse. Pour la civilité `APP_DPO_CIVILITY`, il est possible de mettre :
- Pour Monsieur : `m`
- Pour Madame : `mme`
```bash
APP_DPO_CIVILITY=m
APP_DPO_FIRST_NAME="Prénom"
APP_DPO_LAST_NAME="Nom"
APP_DPO_JOB="Titre de poste"
APP_DPO_MAIL=MonAdresseEmail@Domaine.fr
APP_DPO_PHONE_NUMBER=0123456789
APP_DPO_COMPANY="Nom de la structure"
APP_DPO_ADDRESS_STREET="Rue"
APP_DPO_ADDRESS_CITY="Ville"
APP_DPO_ADDRESS_ZIP_CODE="11111"
```

Configuration du pied de page du site (Nom et URL de la structure de mutualisation).
```bash
APP_FOOTER_PROVIDER_NAME="Nom de la structure"
APP_FOOTER_PROVIDER_URL="https://www.soluris.fr"
```

Paramétrage du nombre de données affichées dans le tableau du plan d'actions sur le tableau de bord utilisateur (gestionnaire ou lecteur).
```bash
APP_USER_DASHBOARD_ACTION_PLAN_LIMIT=10
```

Paramétrage du nombre de données affichées dans le tableau de journalisation des actions de la structure sur le tableau de bord utilisateur (gestionnaire ou lecteur).
```bash
APP_USER_DASHBOARD_JOURNALISATION_LIMIT=15
```

Paramétrage du nombre de données affichées dans les tableaux des vues listes.
```bash
APP_DATATABLE_DEFAULT_PAGE_LENGTH=15
```

## 2. Sécurisation de Madis

**Attention : Modifiez impérativement la chaîne de caractère suivante utilisée dans la sécurité de votre application lors de votre première installation.**
```bash
APP_SECRET=a98f56b9ea67f189df8ed6a39c548503
```

Configuration de la politique de mot de passe pour accéder à Madis. Il est possible de configurer les éléments suivants :
- Longueur minimum ;
- Nécessite des caractères majuscules et minuscules ;
- Nécessite au moins 1 lettre ;
- Nécessite au moins 1 chiffre ;
- Nécessite au moins 1 caractère spécial.
```bash
# Longueur minimale du mot de passe
APP_PASSWORD_REQUIRE_MIN_LENGTH=14
# Si true, le mot de passe nécessite des caractères majuscules ET minuscules
APP_PASSWORD_REQUIRE_CASE_DIFF=true
# Si true, le mot de passe nécessite au moins 1 lettre
APP_PASSWORD_REQUIRE_LETTERS=true
# Si true, le mot de passe nécessite au moins 1 chiffre
APP_PASSWORD_REQUIRE_NUMBERS=true
# Si true, le mot de passe nécessite au moins 1 caractère spécial (&!?, etc.)
APP_PASSWORD_REQUIRE_SPECIAL_CHARACTERS=true
```

Défini si l'administrateur doit être identifié comme le créateur d'un élément lors de subrogation.
- Si à 0, ce sera le nom de l'utilisateur subrogé qui apparaîtra ;
- Si à 1, ce sera le nom de l'administrateur qui subroge qui apparaîtra.
```bash
APP_IMPERSONATE_CREATOR_IS_ADMIN=0
```

Temps maximal de conservation de l'historique de journalisation des actions dans Madis. Par défaut, le temps est de 6 mois (En savoir plus sur le [format à utiliser](https://www.php.net/manual/fr/datetime.formats.relative.php)).
```bash
APP_LOG_JOURNAL_DURATION=6months
```

Nombre de tentatives de connexion avant la désactivation du compte. Un compte désactivé ne pourra plus se connecter, et seul un administrateur pourra le débloquer en modifiant l'option « **Actif** » dans le formulaire de modification de l'utilisateur.
```bash
APP_MAX_LOGIN_ATTEMPTS=10
```

Affiche une case à cocher « **Se souvenir de moi** » sur la page de connexion permettant de se souvenir de l'utilisateur. Cette option permet à un utilisateur de rester connecté jusqu'à 1 semaine.
```bash
APP_CONNEXION_STATUS_KNOW=false
```

Temps maximal d'inactivité pour un utilisateur (en secondes). Une fois le temps écoulé, l'utilisateur est automatiquement déconnecté. Par défaut, le temps est de 1h30.
```bash
APP_COOKIE_IDLE_TIMEOUT=5400
```

Temps maximal de connexion (en secondes). Une fois le temps écoulé, même si l'utilisateur utilise encore activement l'application, il sera automatiquement déconnecté. Par défaut, le temps est de 4h.
```bash
APP_COOKIE_LIFETIME=14400
```

**Note : S'assurer que la configuration PHP `session.gc_maxlifetime` autorise le temps de session au moins supérieur ou égale à ce qui est défini dans le .env.**

## 3. Gérer la taille des pièces jointes déposées

Dans la configuration NGINX, il est possible de définir la taille limite de dépôt de fichier :
```nginx
server {
    client_max_body_size 10M;
}
```

Puis recharger la configuration de NGINX :
```shell
service nginx reload
```

Il faut aussi modifier la limite présente dans le `php.ini` (`/etc/php/8.1/fpm/php.ini`) :
```php
upload_max_filesize = 10M
```

Puis recharger la configuration de php :
```shell
service php8.1-fpm reload
```

Il faut ensuite modifier la limite présente dans le .env :
```php
APP_MAX_UPLOAD_SIZE=10M
```

**Note : S'assurer que les configurations NGINX et PHP autorisent des envois au moins supérieurs ou égaux à ce qui est défini dans le .env.**

## 4. Configuration des images Madis

Les images doivent être déposées dans le dossier `public` présent à la racine du projet. Si vous souhaitez ajouter vos propres images, il est possible d'ajouter un dossier `custom` dans `public`. Celui-ci n'est pas versionné dans Git.

Favicon.
```bash
APP_IMAGE_FAVICON_PATH="images/logo_madis_2020_favicon.png"
```

Image utilisée dans la page de connexion.
```bash
APP_IMAGE_LOGO_COULEUR="images/logo_madis_2020_couleur.png"
```

Image utilisée dans l'en-tête.
```bash
APP_IMAGE_LOGO_BLANC="images/logo_madis_2020_blanc.png"
```

Image utilisée dans l'en-tête lorsque la barre latérale du menu est réduite.
```bash
APP_IMAGE_SIDEBAR_REDUCED_PATH="images/logo_madis_2020_favicon.png"
```

Lien utilisé dans la barre latérale du menu.
```bash
APP_IMAGE_SIDEBAR_BOTTOM_TARGET_URL="https://www.soluris.fr"
```

Image utilisée pour le lien présent dans la barre latérale du menu.
```bash
APP_IMAGE_SIDEBAR_BOTTOM_PATH="images/soluris-logo-white.png"
```

Image SVG utilisée pour la carte du territoire sur le tableau de bord administrateur (administrateur ou référent multi-structures).
```bash
APP_COMMUNE_SVG_REDUCED_PATH="images/commune.svg"
```

Image utilisée dans le pied de page des bilans dans le cas où l'option serait activée dans la fiche d'une structure.
```bash
APP_DPO_IMAGE_LOGO_COULEUR="images/logo_madis_2020_couleur.png"
```

Si les images sont modifiées, penser à vider le cache pour que ces données soient appliquées.
```bash
sudo -u www-data ./bin/console cache:clear
```

## 5. Module notifications

Pour activer le module notifications, mettre la variable suivante à `true` dans le fichier .env.
```bash
APP_ACTIVATE_NOTIFICATIONS=true
```

À noter : Les notifications sont supprimées automatiquement après un certain temps. Ce temps est basé sur le temps paramétré pour la journalisation des actions via la variable `APP_LOG_JOURNAL_DURATION`. De plus, l'ensemble des notifications sont supprimées lors de la désactivation du module.

(Option) Si ce n'est pas déjà fait, ajouter au cron les lignes suivantes. Penser à adapter le chemin `/var/www/madis/` au besoin :
```bash
01 * * * * www-data cd /var/www/madis && $(which php8.1) ./bin/console notifications:generate
31 * * * * www-data cd /var/www/madis && $(which php8.1) ./bin/console notifications:send
```

Nombre de notifications non lues à afficher dans l'en-tête.
```bash
APP_NOTIFICATION_HEADER_NUMBER=5
```

Affichage d'un tableau des dernières notifications non lues sur le tableau de bord administrateur (administrateur ou référent multi-structures).
```bash
APP_NOTIFICATION_DASHBOARD_SHOWN=true
```

Nombre de notifications non lues à afficher sur le tableau de bord administrateur (administrateur ou référent multi-structures).
```bash
APP_NOTIFICATION_DASHBOARD_NUMBER=15
```

Nombre de jours avant l'envoi d'une notification : Aucune connexion depuis la création du compte de l'utilisateur.
```bash
APP_INACTIVE_USER_NOTIFICATION_DELAY_DAYS=365
```

Nombre de jours avant l'envoi d'une notification : Demande non traitée.
```bash
APP_REQUEST_NOTIFICATION_DELAY_DAYS=61
```

Nombre de jours avant l'envoi d'une notification : Aucun indice de maturité réalisé.
```bash
APP_SURVEY_NOTIFICATION_DELAY_DAYS=365
```

Phrase d'introduction des notifications envoyées par email.
```bash
APP_NOTIFICATION_EMAIL_FIRST_LINE="Des modifications ont été apportées dans <a href='https://madis.fr'>Madis</a>"
```

## 6. Débugguer Madis

Logs intéressants :
- Logs d'accès NGINX : `/var/log/nginx/madis_access.log`
- Logs d'erreur NGINX : `/var/log/nginx/madis_error.log`
- Logs Madis : `/var/www/madis/var/log/prod.log`

Configuration du fichier `/var/www/madis/.env` pour passer l'application en mode DEV :
* Passer `APP_ENV=prod` en `APP_ENV=dev` ;
* Mettre `APP_DEBUG=0` à 1 pour augmenter les logs et profiter d'une barre de logs/debug sur l'application.

**Attention : Il est déconseillé d'utiliser ceci en production, cela engendrerait des fuites de sécurité de votre application (comme la divulgation de votre configuration du fichier `.env` et ses mots de passe). Vous pouvez cependant l'utiliser sur des environnements n'ayant pas d'enjeux ou qui possèdent des accès restreints.**

Rediriger tous les mails envoyés sur un seul destinataire (cela écrase la liste des destinataires pour celle-ci).
```bash
APP_MAIL_RECEIVER_DEV=~
```

## 7. Configuration SSO

Nom du SSO utilisé dans la page de connexion et dans la gestion des comptes.
```bash
SSO_TITLE="SSO"
```

Icône du SSO utilisé dans la page de connexion et dans la gestion des comptes.
```bash
SSO_ICON="images/sso-icon.png"
```

Variables de configuration du SSO.
```bash
# sso user info field used to identify user on the app
SSO_KEY_FIELD="sub"

# leave empty to disable SSO logout on app logout
SSO_LOGOUT_URL=

###> Value used for oauth2 client config see config/packages/knpu_oauth2_client.yaml ###
# leave empty to disable SSO (available types: lemonldap and keycloak)
OAUTH_TYPE=keycloak
OAUTH_URL=https://xxxxxxxxxx/auth
OAUTH_REALM=REALM
OAUTH_CLIENT_ID=XXXXX
OAUTH_CLIENT_SECRET=XXXXX
```

## 8. Autres variables configurables

URL vers la base de données (En savoir plus sur le [format à utiliser](http://docs.doctrine-project.org/projects/doctrine-dbal/en/latest/reference/configuration.html#connecting-using-a-url)).
```bash
DATABASE_URL=mysql://user:user_pass@db:3306/madis
```

Adresse email qui apparaîtra dans les emails envoyés par Madis.
```bash
APP_MAIL_SENDER_EMAIL=ne-pas-repondre@example.fr
```

Nom de l'expéditeur qui apparaitra dans les emails envoyés par Madis.
```bash
APP_MAIL_SENDER_NAME="Madis"
```

Valeurs utilisées pour les tooltips sur les réponses dans le formulaire d'évaluation de conformité de la structure.
```bash
TOOLTIP_CONFORMITE_ORGANISATION_INEXISTANTE="Rien n'est fait"
TOOLTIP_CONFORMITE_ORGANISATION_TRES_ELOIGNEE="La ou les pratique(s) sont très éloignées de la définition (Pratique < 20%)."
TOOLTIP_CONFORMITE_ORGANISATION_PARTIELLE="La ou les pratique(s) sont partielles (20% < Pratique > 80%) au regard de la définition.<br/>Elles ne sont pas documentées."
TOOLTIP_CONFORMITE_ORGANISATION_QUASI_CONFORME="La ou les pratiques sont conformes ou quasiment conforme à la définition (80% < Pratique > 100%)."
TOOLTIP_CONFORMITE_ORGANISATION_MESURABLE="La ou les pratiques sont conforme à la définition.<br/>Elles sont documentées et contrôlables dans le cas d'un audit."
TOOLTIP_CONFORMITE_ORGANISATION_REVISEE="La ou les pratiques sont coordonnées et conforme à la définition.<br/>Des évaluations sont réalisées.<br/>Des améliorations sont systématiquement apportées à partir de l'analyse des évaluations effectuées."
```

URL vers le mailer que doit utiliser Madis.
- Pour GMAIL, utiliser : `"gmail://username:password@localhost"`
- Pour un SMTP générique, utiliser : `"smtp://localhost:25?encryption=&auth_mode="`
- Pour désactiver les mails, utiliser : `"null://localhost"`
```bash
MAILER_DSN=null://localhost
```

Configuration de l'emplacement de Wkhtmltopdf.
```bash
WKHTMLTOPDF_PATH=/usr/local/bin/wkhtmltopdf
WKHTMLTOIMAGE_PATH=/usr/local/bin/wkhtmltoimage
```
