Requêtes SQL
============

1. [Conformité de la structure : Ajouter une question à un processus existant](#1-conformité-de-la-structure--ajouter-une-question-à-un-processus-existant)
2. [Conformité de la structure : Ajouter un nouveau processus](#2-conformité-de-la-structure--ajouter-un-nouveau-processus)
3. [Exporter la base de données](#3-exporter-la-base-de-données)

## 1. Conformité de la structure : Ajouter une question à un processus existant

Il faut vérifier en base de données la dernière valeur pour le champ `position` qui ordonne les questions liées à ce processus. Dans l'exemple suivant la position de la question est à 5, car la dernière question en base de données a la valeur 4. Bien faire attention à la génération de la clef primaire qui est ici un `uuid`.

```sql
INSERT INTO `registry_conformite_organisation_question` (`id`, `processus_id`, `nom`, `position`) VALUES ('4d66c04e-62e7-4216-85a2-6d9feb71722a', 'b2a186df-cf81-4199-a292-53dbdb43b609', 'Ceci est le texte de la question', '5')
```

## 2. Conformité de la structure : Ajouter un nouveau processus

Il faut vérifier en base de données la dernière valeur pour le champ `position` qui ordonne les processus. Dans l'exemple suivant la position du processus est à 13 car le dernier processus en base a la valeur 12. Bien faire attention à la génération de la clef primaire qui est ici un `uuid`.

```sql
INSERT INTO `registry_conformite_organisation_processus` (`id`, `nom`, `couleur`, `description`, `position`) VALUES ('b2a186df-cf81-4199-a292-53dbdb43b609', 'Nom du processus', 'info', 'Description du processus', '13')
```

Puis ajouter la question en utilisant l'`uuid` du processus précédemment créé pour la colonne `processus_id`. Suivre alors la procédure décrite pour [ajouter une question à un processus existant](#1-conformité-de-la-structure-ajouter-une-question-à-un-processus-existant).

## 3. Exporter la base de données

Pour exporter la base de données.

```sql
mysqldump -u madis -pMonMotDePasse madis > /root/dump_madis.sql
```

Outre les données SQL, il est nécessaire en cas de sauvegarde ou migration de serveur de récupérer les fichiers suivants :
- `/var/www/madis/public/uploads` ;
- `/var/www/madis/public/images` ;
- `/var/www/madis/var/documents/files`.
