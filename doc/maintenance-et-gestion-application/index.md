Maintenance et gestion applicative
========================

## Table des matières

- [Paramétrage complémentaire et .env](1-parametrages-complementaires-et-env.md#paramétrage-complémentaire-et-env)
    - [Configuration générale](1-parametrages-complementaires-et-env.md#1-configuration-générale)
    - [Sécurisation de Madis](1-parametrages-complementaires-et-env.md#2-sécurisation-de-madis)
    - [Gérer la taille des pièces jointes déposées](1-parametrages-complementaires-et-env.md#3-gérer-la-taille-des-pièces-jointes-déposées)
    - [Configuration des images Madis](1-parametrages-complementaires-et-env.md#4-configuration-des-images-madis)
    - [Module notifications](1-parametrages-complementaires-et-env.md#5-module-notifications)
    - [Débugguer Madis](1-parametrages-complementaires-et-env.md#6-débugguer-madis)
    - [Configuration SSO](1-parametrages-complementaires-et-env.md#7-configuration-sso)
    - [Autres variables configurables](1-parametrages-complementaires-et-env.md#8-autres-variables-configurables)
- [Mettre à jour Madis](2-mettre-a-jour-madis.md#mettre-à-jour-madis)
    - [Monter de version](2-mettre-a-jour-madis.md#1-monter-de-version)
    - [Rétrograder de version](2-mettre-a-jour-madis.md#2-rétrograder-de-version)
    - [Problèmes connus de mise à jour](2-mettre-a-jour-madis.md#3-problèmes-connus-de-mise-à-jour)
- [Requêtes SQL](3-requetes-sql.md#requêtes-sql)
    - [Conformité de la structure : Ajouter une question à un processus existant](3-requetes-sql.md#1-conformité-de-la-structure--ajouter-une-question-à-un-processus-existant)
    - [Conformité de la structure : Ajouter un nouveau processus](3-requetes-sql.md#2-conformité-de-la-structure--ajouter-un-nouveau-processus)
    - [Exporter la base de données](3-requetes-sql.md#3-exporter-la-base-de-données)
