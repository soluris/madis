Madis - Documentation technique développeurs
=====================

## Table des matières

- [Lancer la stack de développement](1-lancer-stack-developpement.md#lancer-la-stack-de-développement)
    - [Pré-requis](1-lancer-stack-developpement.md#1-pré-requis)
    - [Installation](1-lancer-stack-developpement.md#2-installation)
    - [Utilisation basique](1-lancer-stack-developpement.md#3-utilisation-basique)
    - [Lancer les tests](1-lancer-stack-developpement.md#4-lancer-les-tests)
    - [Usages de développement](1-lancer-stack-developpement.md#5-usages-de-développement)
- [Architecture et qualité](2-architecture-et-qualite.md#architecture-et-qualité)
    - [Architecture applicative](2-architecture-et-qualite.md#1-architecture-applicative)
    - [Bonnes pratiques de développement](2-architecture-et-qualite.md#2-bonnes-pratiques-de-développement)
    - [Générer un rapport GitLab Page](2-architecture-et-qualite.md#3-générer-un-rapport-gitlab-page)
- [Déployer une nouvelle version](3-deployer-une-nouvelle-version.md#déployer-une-nouvelle-version)
    - [Montée de version des librairies](3-deployer-une-nouvelle-version.md#1-montée-de-version-des-librairies)
    - [Déploiement d’une nouvelle version de l’application](3-deployer-une-nouvelle-version.md#2-déploiement-dune-nouvelle-version-de-lapplication)
