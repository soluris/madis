Déployer une nouvelle version
=======================

1. [Montée de version des librairies](#1-montée-de-version-des-librairies)
2. [Déploiement d’une nouvelle version de l’application](#2-déploiement-dune-nouvelle-version-de-lapplication)
    - [Préparer une nouvelle version](#préparer-une-nouvelle-version)
    - [Déployer la nouvelle version](#déployer-la-nouvelle-version)

## 1. Montée de version des librairies

Les dépendances sont gérées avec le gestionnaire de paquet `composer`. Ce dernier permet de tenir à jour les librairies.

Une simple commande permet de mettre à jour les librairies (les fix de bugs ou de failles de sécurité).

Utilisez la commande `composer update` qui va modifier le fichier `composer.lock`. Pensez ensuite à l’ajouter au Git, puis créer un nouveau tag pour pouvoir effectuer cette montée de version en production.

## 2. Déploiement d’une nouvelle version de l’application

### Préparer une nouvelle version

Nous suivons le Git Flow. Le code qui est prêt à mettre en production se trouve sur la branche develop.

Il faut donc monter la version de l’application dans le fichier `config/packages/framework.yaml` et modifier le [CHANGELOG.md](../../CHANGELOG.md#changelog) avec les dernières modifications.

**Recommandation Madis** : Il est proposé que chaque nouveau changelog soit constitué d'un titre formé par le numéro de version, suivi de la date de tag en format Année-Mois-Jour. Par exemple : `[2.4.5] - 2024-04-29`.

De plus, les sous-titres suivent l'ordre suivant et sont à adapter selon la présence :
1. Ajout ;
2. Changements ;
3. Fix ;
4. Suppression ;
5. Migration.

Enfin, chacun des éléments est préfixé par le nom du module. Par exemple : `[Traitements] Mon titre explicite et concis`.

Il suffit ensuite de merger cette version dans la master, puis de [créer un tag](https://gitlab.adullact.net/soluris/madis/-/tags) avec le numéro de version correspondant.

### Déployer la nouvelle version

Pour déployer, rien de plus simple. Rendez-vous sur le site correspondant, dans le dossier du projet et lancez la commande `git checkout` dans la dernière version, puis `./bin/deploy`.

Pour plus d'informations, consulter la documentation pour [mettre à jour Madis](../maintenance-et-gestion-application/2-mettre-a-jour-madis.md#1-monter-de-version).
