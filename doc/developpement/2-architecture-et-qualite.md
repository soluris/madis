Architecture et qualité
========================

1. [Architecture applicative](#1-architecture-applicative)
    - [Organisation des dossiers](#organisation-des-dossiers)
    - [Utilisation de l’architecture hexagonale](#utilisation-de-larchitecture-hexagonale)
2. [Bonnes pratiques de développement](#2-bonnes-pratiques-de-développement)
3. [Générer un rapport GitLab Page](#3-générer-un-rapport-gitlab-page)

## 1. Architecture applicative

### Organisation des dossiers

L’architecture est standard, mais voici un petit rappel :
- `.env` et `.env.dist` sont les paramètres de l’application ;
- `composer.json` et `composer.lock` sont les gestionnaires de versions de paquets ;
- `config/bundles.php` contient la configuration des librairies ;
- `config/packages` contient la définition et la configuration des librairies utilisées (des paramétrages spécifiques peuvent être ajoutés selon l’environnement, il s’agit des dossiers `config/dev/*` et `config/test/*`) ;
- `config/routes.yaml` contient la définition des routes de l’application ;
- `config/services.yaml` contient la définition des services ;
- `public/index.php` est le fichier qui permet de lancer l’application (le serveur web pointe sur ce fichier) ;
- `src` contient le code métier de l’application (Controller par exemple) ;
- `vendor` contient les librairies (généré par composer).

### Utilisation de l’architecture hexagonale

L’architecture hexagonale est utilisée ici pour s’affranchir de la contrainte technique MySQL. Il sera ainsi très simple de connecter une autre source de donnée. On pourra facilement brancher une API, une base de données Postgres par exemple, si besoin.

C’est pour cela que vous possédez deux dossiers : Domain et Infrastructure.
-	**Domain** va contenir les classes PHP utilisées dans l’application. Elles sont décorrélées des implémentations de base de données et va mettre à disposition des Interfaces PHP qu’il faudra implémenter pour récupérer les données.
-	**Infrastructure** va contenir les implémentations aux interfaces du Domain. On y trouvera ici toute l’implémentation technique pour chaque source de données que l’on souhaitera brancher sur l’application. Actuellement, l’implémentation utilisée est ORM (soit le dossier `Infrastructure/ORM`). On va y trouver notamment le mapping Doctrine pour la création de la base de données et l’implémentation des `Repository` qui vont être utilisés dans l’application.

La configuration Symfony de cette architecture se déroule dans le dossier `config/domain` et `config/infrastructure` qui va respecter la même architecture précédemment expliquée avec ces mêmes dossiers.

## 2. Bonnes pratiques de développement

Des bonnes pratiques sont à adopter pour le développement :
-	**Git Flow** : Le dépôt Git est maintenu en suivant le Git flow. Appliquez-le durant vos développements ;
-	**GitHook** : Utilisez le git-hook mis à disposition (nécessite Docker) qui va lancer un php-cs-fixer afin que le code respecte des conventions de formatage ;
-	**Tests** : À chaque développement de fonctionnalité, pensez à créer les tests associés (PHPUnit, Behat) ;
-	**Migration de base de données** : Si des modifications de base de données sont à effectuer, générez les doctrines migrations associées ;
-	**Merge requests** : Si vous travaillez à plusieurs développeurs, utilisez des Merge Requests afin de faire valider votre code par une tierce personne ;
-	**Ubiquitous language** : Le projet a été développé initialement sans utiliser l'ubiquitous language, menant à une compréhension ardue des modèles/propriétés et de mauvaises traductions. Il est désormais utilisé afin de faciliter la compréhension et d'éviter ces problèmes.

## 3. Générer un rapport GitLab Page

Lors du commit, des jobs de qualité sont générés et consultables sur le [projet GitLab](https://gitlab.adullact.net/soluris/madis/pipelines).

Plusieurs étapes sont lancées, et généreront des indicateurs de qualité du projet visualisables sur une [page GitLab](https://soluris.gitlab.io/madis/) :
- PHPUnit code coverage ;
- PHPMetrics ;
- PHPMD (Mess Detector).

Certaines règles de qualités ont parallèlement été établies :
- PHPStan ;
- PHP-CS-FIXER ;
- Lint de fichiers YAML et PHP ;
- Check de sécurité Symfony.
