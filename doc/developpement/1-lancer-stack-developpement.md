Lancer la stack de développement
================================

1. [Pré-requis](#1-pré-requis)
2. [Installation](#2-installation)
    - [SSL](#ssl)
3. [Utilisation basique](#3-utilisation-basique)
4. [Lancer les tests](#4-lancer-les-tests)
5. [Usages de développement](#5-usages-de-développement)

## 1. Pré-requis

* Docker

## 2. Installation

Copier le fichier `.env.dist` vers `.env` et éditez le avec les valeurs vous correspondant. Pour plus d'informations, consultez le [paramétrage complémentaire](../maintenance-et-gestion-application/1-parametrages-complementaires-et-env.md#paramétrage-complémentaire-et-env) du .env.

Si vous utilisez un processeur ARM (raspberry pi, mac M1 / M2) vous pouvez notamment changer les images docker pour des images ARM en remplaçant :
```bash
DOCKER_IMAGE_PHP=gitlab.adullact.net:4567/soluris/madis/php:1.2
DOCKER_IMAGE_NGINX=gitlab.adullact.net:4567/soluris/madis/nginx:1.2
```
Par :
```bash
DOCKER_IMAGE_PHP=gitlab.adullact.net:4567/soluris/madis/php:1.2-arm64
DOCKER_IMAGE_NGINX=gitlab.adullact.net:4567/soluris/madis/nginx:1.2-arm64
```

Ensuite, connectez votre docker à GitLab avec vos identifiants. Cela vous permettra d'accéder aux images Docker du projet.
```bash
docker login gitlab.adullact.net:4567
```

### SSL

Afin d'obtenir des certificats auto-signés, vous avez besoin d'installer [mkcert](https://github.com/FiloSottile/mkcert) sur votre machine.

Créez le certificat et la clef pour votre environnement de développement.
```bash
mkcert -key-file key.pem -cert-file cert.pem 127.0.0.1 madis.local
```

Puis copiez les fichiers créés dans le dossier `docker/nginx/certificats/default`.
```bash
cp *.pem ./docker/nginx/certificats/default/
```

Finissez par initialiser le projet. Pour cela, le fichier `docker-service` fourni un raccourci pour lancer toute la stack.
```bash
./docker-service initialize
```

Pour aller plus loin, vous pouvez étudier le fichier `docker-service` pour voir les commandes lancées.

## 3. Utilisation basique

* URL du projet : `http://127.0.0.1:8888` ou `https://127.0.0.1`

Quelques identifiants clés :
- **Administrateur :** `admin@nomail.soluris.fr` / `111111` ;
- **Gestionnaire 1 :** `collectivite@nomail.soluris.fr` / `111111` ;
- **Gestionnaire 2 :** `collectivite2@nomail.soluris.fr` / `111111` ;
- **Lecteur :** `lecteur@nomail.soluris.fr` / `111111` ;
- **Utilisateur inactif :** `inactif@nomail.soluris.fr` / `111111`.

## 4. Lancer les tests

```bash
./docker-service tests         # Run quality tests, unit tests and functionnal tests
./docker-service unitTests     # Run unit tests
./docker-service qualityTests  # Run quality tests
```

En savoir plus sur la [génération de rapports](2-architecture-et-qualite.md#3-générer-un-rapport-gitlab-page).

## 5. Usages de développement

* Le process Git utilisé est le Git Flow ;
* Les tests sont écrits en PHPUnit ;
* Le style de code doit être vérifié avec CSFixer et PHPLint avant les commits.

En savoir plus sur l'[architecture applicative](2-architecture-et-qualite.md#architecture-et-qualité) et sur les [déploiements](3-deployer-une-nouvelle-version.md#déployer-une-nouvelle-version).
