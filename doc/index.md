Madis - Documentation
=====================

## Table des matières

- [Licence d'utilisation de Madis](../LICENSE.md)
- [Installation de Madis](installation#madis-installation)
    - [Installer Madis par script](installation/1-installation-script.md#installer-madis-par-script)
    - [Installer Madis manuellement](installation/2-installation-manuelle.md#installer-madis-manuellement)
- [Journal des modifications (Changelog)](../CHANGELOG.md#changelog)
- [Documentation utilisateur](https://documentation-madis.readthedocs.io/fr/latest/docutilisateur.html)
- [Maintenance et gestion applicative](maintenance-et-gestion-application#maintenance-et-gestion-applicative)
    - [Paramétrage complémentaire et .env](maintenance-et-gestion-application/1-parametrages-complementaires-et-env.md#paramétrage-complémentaire-et-env)
    - [Mettre à jour Madis](maintenance-et-gestion-application/2-mettre-a-jour-madis.md#mettre-à-jour-madis)
    - [Requêtes SQL](maintenance-et-gestion-application/3-requetes-sql.md#requêtes-sql)
- [Documentation technique développeurs](developpement#madis-documentation-technique-développeurs)
    - [Lancer la stack de développement](developpement/1-lancer-stack-developpement.md#lancer-la-stack-de-développement)
    - [Architecture et qualité](developpement/2-architecture-et-qualite.md#architecture-et-qualité)
    - [Déployer une nouvelle version](developpement/3-deployer-une-nouvelle-version.md#déployer-une-nouvelle-version)
- [Contribuer au projet](../CONTRIBUTING.md#contribution)
