#!/bin/bash
default_domain="madis.mondomaine.fr"
default_password="MotDePasseParDefaut"
default_mail="m.edlich@soluris.fr"
default_user_password="madis"
smtp_server="mail.smtp2go.com"
smtp_port="2525"
smtp_username="smtp-test-mike-sol-mad"
smtp_password="nz77KhEGVhuKVFob"
from_address="m.edlich@soluris.fr"

export NEWT_COLORS='
'
# Fonction pour afficher un message d'erreur et quitter le script
print_error_and_exit() {
    local message="$1"
    echo "Erreur : $message"
    exit 1
}
check_command_success() {
    local exit_code="$1"
    local command_name="$2"
    if [ "$exit_code" -ne 0 ]; then
        print_error_and_exit "La commande \"$command_name\" a échoué avec le code de sortie $exit_code."
    fi
}

# Vérification du système d'exploitation
if grep -qE "Ubuntu 22" /etc/os-release; then
    systemOS="ubuntu22"
elif grep -qE "Ubuntu 24" /etc/os-release; then
    systemOS="ubuntu24"
elif grep -qE "Debian GNU/Linux 12" /etc/os-release; then
    systemOS="debian12"
elif grep -qE "Rocky Linux" /etc/os-release; then
    systemOS="rocky"
else
    print_error_and_exit "Système d'exploitation non pris en charge."
	exit 1
fi


if [ "$(id -u)" -eq 0 ]; then
	apt install -y --no-install-recommends sudo
else
if ! sudo -v &> /dev/null; then
    echo "Vous devez être root ou avoir les privilèges avec Sudo pour executer ce script."
    exit 1
fi
fi

# Définition de la variable NEEDRESTART_MODE
sudo sed -i "s/#\$nrconf{restart} = 'i';/\$nrconf{restart} = 'a';/" /etc/needrestart/needrestart.conf
export NEEDRESTART_MODE=a

if [ "$systemOS" = "debian12" ]; then
	apt install -y --no-install-recommends ntpdate
    sudo ntpdate time.nist.gov
    sudo timedatectl set-timezone Europe/Paris
fi


if [ "$systemOS" = "debian12" ] || [ "$systemOS" = "ubuntu22" ] || [ "$systemOS" = "ubuntu24" ]; then
	# Installation des paquets
	export DEBIAN_FRONTEND=noninteractive
    sudo rm -rf /etc/apt/sources.list.d/yarn.list
	sudo apt update
	sudo apt -o Dpkg::Options::="--force-confold" upgrade -y
	nginxusr="www-data"
fi
if [ "$systemOS" = "rocky" ]; then
    sudo systemctl enable chronyd
    sudo systemctl start chronyd
    sudo timedatectl set-ntp true
    sudo timedatectl set-timezone Europe/Paris
    sudo systemctl restart chronyd
    sleep 2
	sudo dnf update -y
	sudo dnf upgrade -y
	nginxusr="nginx"
fi

if [ "$systemOS" = "debian12" ]; then
  sudo apt install -y --no-install-recommends xfonts-base net-tools xfonts-75dpi xfonts-utils xfonts-encodings x11-common libfontenc1 libxrender1 fontconfig git nginx default-mysql-server gnupg curl apt-transport-https lsb-release ca-certificates wget libjpeg62-turbo telnet whiptail openssl
  apt-key adv --fetch-keys "https://packages.sury.org/php/apt.gpg" > /dev/null 2>&1
  sudo wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg
  sudo echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | sudo tee /etc/apt/sources.list.d/php.list
  sudo echo "deb https://dl.yarnpkg.com/debian stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
  sudo curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
  sudo curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | sudo gpg --dearmor | sudo tee /usr/share/keyrings/yarnkey.gpg >/dev/null
  sudo apt update && sudo apt -y upgrade && apt -y install php8.1-bz2 php8.1-cli php8.1-common php8.1-curl php8.1-fpm php8.1-gd php8.1-intl php8.1-mbstring php8.1-mysql php8.1-opcache php8.1-readline php8.1-xml php8.1-zip
elif [ "$systemOS" = "ubuntu22" ] || [ "$systemOS" = "ubuntu24" ]; then
  sudo apt install -y --no-install-recommends tzdata xfonts-base net-tools xfonts-75dpi xfonts-utils xfonts-encodings x11-common libfontenc1 libxrender1 fontconfig git nginx mysql-server gnupg curl apt-transport-https lsb-release ca-certificates wget sudo telnet whiptail
  echo "tzdata tzdata/Areas select Europe" | sudo debconf-set-selections
  echo "tzdata tzdata/Zones/Europe select Paris" | sudo debconf-set-selections
  sudo dpkg-reconfigure -f noninteractive tzdata
  yes | sudo add-apt-repository ppa:ondrej/php
  sudo apt update
  sudo apt install -y --no-install-recommends sudo php8.1-bz2 php8.1-cli php8.1-common php8.1-curl php8.1-fpm php8.1-gd php8.1-intl php8.1-mbstring php8.1-mysql php8.1-opcache php8.1-readline php8.1-xml php8.1-zip
elif [ "$systemOS" = "rocky" ]; then
  sudo dnf install -y epel-release
	sudo dnf -y config-manager --set-enabled powertools
	sudo dnf -y module reset php
	sudo dnf install -y https://rpms.remirepo.net/enterprise/remi-release-9.rpm
	sudo dnf module enable php:remi-8.1 -y
  sudo dnf install -y --nogpgcheck sudo net-tools mariadb-server nginx php php-bz2 php-cli php-common php-curl php-fpm php-gd php-intl php-json php-mbstring php-mysqlnd php-opcache php-readline php-xml php-zip fontconfig libfontenc libXrender xorg-x11-fonts-Type1 xorg-x11-fonts-misc xorg-x11-fonts-75dpi git openssl telnet compat-openssl11
else
  print_error_and_exit "Système d'exploitation non pris en charge."
	exit 1
fi

# Affichage d'un message agréable
echo "Bienvenue dans le script d'installation de l'application Madis."

# Fonction pour afficher une boîte de dialogue Whiptail avec une question Oui/Non
ask_yes_no() {
    local question="$1"
    whiptail --title "Question" --yesno "$question" --yes-button "Oui" --no-button "Non" 10 60
    return $?
}

# Affichage de la boîte de dialogue de confirmation
whiptail --title "Question" --yesno "Ce script va installer Madis sur votre serveur.\n\nATTENTION : Si vous avez déjà une installation de Madis celle-ci va être écrasée.\n\nÊtes vous sûr de vouloir continuer ?"  --yes-button "Oui" --no-button "Non" 15 60
response=$?
if [ $response -eq 0 ]; then
    echo "Installation en cours..."

input_smtp_info() {
    smtp_server=$(whiptail --inputbox "Entrez le serveur SMTP :" 10 60 3>&1 1>&2 2>&3)
    smtp_port=$(whiptail --inputbox "Entrez le port SMTP :" 10 60 3>&1 1>&2 2>&3)
    smtp_username=$(whiptail --inputbox "Entrez le nom d'utilisateur SMTP :" 10 60 3>&1 1>&2 2>&3)
    smtp_password=$(whiptail --passwordbox "Entrez le mot de passe SMTP :" 10 60 3>&1 1>&2 2>&3)
    from_address=$(whiptail --inputbox "Entrez l'adresse e-mail expéditeur :" 10 60 3>&1 1>&2 2>&3)
}

# Demande du nom de domaine à configurer
domain_name=$(whiptail --inputbox "Veuillez saisir le nom de domaine à configurer (ex: madis.mondomaine.fr) :" 10 65 "$default_domain" --title "Nom de domaine" 3>&1 1>&2 2>&3)

# Si aucun nom de domaine n'est saisi, définir une valeur par défaut
if [ -z "$domain_name" ]; then
    domain_name=$default_domain
fi

# Choix de la version de Madis
liste_versions=$(GIT_TERMINAL_PROMPT=0 git ls-remote --tags "https://gitlab.adullact.net/soluris/madis/" 2>/dev/null| grep -v '\^{}$' | awk '{print $2}' | sed 's#refs/tags/##' | tail -n 20|tac)
liste_versions_choix=()
while IFS= read -r tag; do
    liste_versions_choix+=("$tag" "$tag")
done <<< "$liste_versions"
liste_versions_choix=()
first=1
while IFS= read -r tag; do
    if [ $first -eq 1 ]; then
        # Ajouter "Version stable" à la première entrée
        liste_versions_choix+=("$tag" " Version stable ")
        default_version=$tag
        first=0
    else
        liste_versions_choix+=("$tag" " ")
    fi
done <<< "$liste_versions"
version_madis=$(whiptail --title "Version de Madis" --menu "Veuillez choisir la version de Madis à installer :" 20 60 10 "${liste_versions_choix[@]}" 3>&1 1>&2 2>&3)
if [ -z "$version_madis" ]; then
    version_madis=$default_version
fi


# Demande du mot de passe pour la base de données
db_password=$(whiptail --passwordbox "Veuillez saisir le mot de passe pour la base de données :" 10 65 --title "Mot de passe MySQL" "$default_password" 3>&1 1>&2 2>&3)
# Si aucun mot de passe n'est saisi, définir une valeur par défaut
if [ -z "$db_password" ]; then
    db_password=$default_password
fi

user_name=$(whiptail --inputbox "Veuillez saisir votre email (ex: m.edlich@soluris.fr) :" 10 65 "$default_mail" --title "Mail de l'utilisateur" 3>&1 1>&2 2>&3)
# Si aucun email n'est saisi, définir une valeur par défaut
if [ -z "$user_name" ]; then
    user_name=$default_mail
fi

# Demande du mot de passe pour l'utilisateur
user_password=$(whiptail --passwordbox "Veuillez saisir le mot de passe pour l'utilisateur :" 10 65 --title "Mot de passe Utilisateur" "$default_user_password" 3>&1 1>&2 2>&3)
# Si aucun mot de passe n'est saisi, définir une valeur par défaut
if [ -z "$user_password" ]; then
    user_password=$default_user_password
fi

ask_yes_no "Voulez-vous envoyer un email avec les informations de connexion ?"
send_email=$?

ask_yes_no "Souhaitez-vous générer un certificat SSL autosigné ?"
auto_sign_ssl=$?

if [ $send_email -eq 0 ]; then
    # Demander à l'utilisateur s'il souhaite configurer le serveur SMTP maintenant
    ask_yes_no "Voulez-vous configurer le serveur SMTP maintenant ?"
    configure_smtp=$?

    if [ $configure_smtp -eq 0 ]; then
        # Saisir les informations SMTP
        input_smtp_info
        # Afficher les informations saisies
        whiptail --msgbox "Configuration SMTP :\nServeur : $smtp_server\nPort : $smtp_port\nNom d'utilisateur : $smtp_username\nMot de passe : *****\nAdresse e-mail expéditeur : $from_address" 12 60
    fi
fi

# Génération du certificat auto signé
if [ $auto_sign_ssl -eq 0 ]; then
    # Créez le fichier madis_ssl.cnf
    sudo tee /etc/ssl/madis_ssl.cnf > /dev/null <<EOF
[ req ]
default_bits       = 2048
default_keyfile    = /etc/nginx/ssl/nginx.key
default_md         = sha256
prompt             = no
distinguished_name = dn

[ dn ]
countryName                = FR
stateOrProvinceName        = France
localityName               = Saintes
organizationName           = Soluris
organizationalUnitName     = Madis
commonName                 = $domain_name
emailAddress               = $user_name
EOF

# générer le certificat
sudo mkdir /etc/nginx/ssl/
sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 \
-keyout /etc/nginx/ssl/nginx.key \
-out /etc/nginx/ssl/nginx.crt \
-config /etc/ssl/madis_ssl.cnf
fi

if sudo ss -tuln | grep ':80'; then
    echo "Quelque chose tourne sur le port 80. Arrêt du service..."
    # Rechercher les processus écoutant sur le port 80 avec lsof
    process_info=$(sudo lsof -i :80 | awk 'NR==2 {print $1}')
    # Arrêter le service
    sudo systemctl stop $process_info
fi

if sudo ss -tuln | grep ':443'; then
    echo "Quelque chose tourne sur le port 80. Arrêt du service..."
    # Rechercher les processus écoutant sur le port 80 avec lsof
    process_info=$(sudo lsof -i :443 | awk 'NR==2 {print $1}')
    # Arrêter le service
    sudo systemctl stop $process_info
fi

if [ "$systemOS" = "debian12" ] || [ "$systemOS" = "ubuntu22" ] || [ "$systemOS" = "ubuntu24" ]; then
sudo curl -fsSL https://deb.nodesource.com/setup_18.x | sudo -E bash -
sudo apt-get install -y --no-install-recommends nodejs
fi
if [ "$systemOS" = "rocky" ]; then
	sudo curl -sL https://rpm.nodesource.com/setup_18.x -o nodesource_setup.sh
	sudo bash nodesource_setup.sh
	sudo dnf install nodejs -y
fi

if [ "$systemOS" = "debian12" ]; then
sudo wget --continue --tries=10 --timeout=30 --read-timeout=30 --waitretry=5 "http://ftp.us.debian.org/debian/pool/main/o/openssl/libssl1.1_1.1.1w-0+deb11u1_amd64.deb"
sudo wget --continue --tries=10 --timeout=30 --read-timeout=30 --waitretry=5 "http://ftp.us.debian.org/debian/pool/main/o/openssl/libssl1.1_1.1.1w-0+deb11u1_amd64.deb"
sudo wget "https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.5/wkhtmltox_0.12.5-1.buster_amd64.deb"
sudo dpkg -i libssl1.1_1.1.1w-0+deb11u1_amd64.deb && sudo rm -f libssl1.1_1.1.1w-0+deb11u1_amd64.deb
sudo dpkg -i wkhtmltox_0.12.5-1.buster_amd64.deb && sudo rm -f wkhtmltox_0.12.5-1.buster_amd64.deb
fi
if [ "$systemOS" = "ubuntu22" ] || [ "$systemOS" = "ubuntu24" ]; then
sudo wget "https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.5/wkhtmltox_0.12.5-1.focal_amd64.deb"
sudo wget --continue --tries=10 --timeout=30 --read-timeout=30 --waitretry=5 "http://archive.ubuntu.com/ubuntu/pool/main/o/openssl/libssl1.1_1.1.0g-2ubuntu4_amd64.deb"
sudo wget --continue --tries=10 --timeout=30 --read-timeout=30 --waitretry=5 "http://archive.ubuntu.com/ubuntu/pool/main/o/openssl/libssl1.1_1.1.0g-2ubuntu4_amd64.deb"
sudo dpkg -i libssl1.1_1.1.0g-2ubuntu4_amd64.deb && sudo rm -f libssl1.1_1.1.0g-2ubuntu4_amd64.deb
sudo dpkg -i wkhtmltox_0.12.5-1.focal_amd64.deb && sudo rm -f wkhtmltox_0.12.5-1.focal_amd64.deb
fi
if [ "$systemOS" = "rocky" ]; then
sudo wget "https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.5/wkhtmltox-0.12.5-1.centos8.x86_64.rpm"
sudo rpm -i wkhtmltox-0.12.5-1.centos8.x86_64.rpm && rm -f wkhtmltox-0.12.5-1.centos8.x86_64.rpm
fi

password_hash=$(php -r "echo (password_hash('$user_password',1));")

# Installation de Composer
sudo curl -sS https://getcomposer.org/installer -o /usr/local/bin/composer-setup.php
if [ "$systemOS" = "rocky" ]; then
	sudo ln -s /bin/php /usr/bin/php8.1
	sudo php /usr/local/bin/composer-setup.php --install-dir=/usr/bin --filename=composer
fi
if [ "$systemOS" = "debian12" ] || [ "$systemOS" = "ubuntu22" ] || [ "$systemOS" = "ubuntu24" ]; then
	sudo php /usr/local/bin/composer-setup.php --install-dir=/usr/local/bin --filename=composer
fi

# Configuration de Nginx
if [ "$systemOS" = "rocky" ]; then
	sudo mkdir /etc/nginx/sites-available
	sudo mkdir /etc/nginx/sites-enabled
fi
sudo unlink /etc/nginx/sites-enabled/*
sudo rm -f /etc/nginx/sites-enabled/*
proto="http"
cat << EOF | sudo tee /etc/nginx/sites-available/madis.conf > /dev/null
server {
    listen 80;
    server_name $domain_name;
    root /var/www/madis/public;
    location / {
        try_files \$uri /index.php\$is_args\$args;
    }
    location ~ ^/index\.php(/|$) {
        fastcgi_pass unix:/var/run/php/php8.1-fpm.sock;
        fastcgi_split_path_info ^(.+\.php)(/.*)$;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME \$realpath_root\$fastcgi_script_name;
        fastcgi_param DOCUMENT_ROOT \$realpath_root;
        internal;
    }
    location ~ \.php$ {
        return 404;
    }
    error_log /var/log/nginx/madis_error.log;
    access_log /var/log/nginx/madis_access.log;
}
EOF

if [ $auto_sign_ssl -eq 0 ]; then
    sudo rm /etc/nginx/sites-available/madis.conf
    proto="https"
    cat << EOF | sudo tee /etc/nginx/sites-available/madis.conf > /dev/null
server {
    listen 443 ssl;
    server_name $domain_name;
    root /var/www/madis/public;
    ssl_certificate /etc/nginx/ssl/nginx.crt;
    ssl_certificate_key /etc/nginx/ssl/nginx.key;
    location / {
    try_files \$uri /index.php\$is_args\$args;
    }
    location ~ ^/index\.php(/|$) {
        fastcgi_pass unix:/var/run/php/php8.1-fpm.sock;
        fastcgi_split_path_info ^(.+\.php)(/.*)$;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME \$realpath_root\$fastcgi_script_name;
        fastcgi_param DOCUMENT_ROOT \$realpath_root;
        internal;
    }
    location ~ \.php$ {
        return 404;
    }
    error_log /var/log/nginx/madis_error.log;
    access_log /var/log/nginx/madis_access.log;
}
EOF


fi

sudo ln -s /etc/nginx/sites-available/madis.conf /etc/nginx/sites-enabled/
if [ "$systemOS" = "rocky" ]; then
	sudo sed -i 's|fastcgi_pass unix:/var/run/php/php8\.1-fpm\.sock;|fastcgi_pass unix:/run/php-fpm/www.sock;|g' /etc/nginx/sites-available/madis.conf
	sed -i '/include \/etc\/nginx\/conf.d\/\*\.conf;/a     include \/etc\/nginx\/sites-enabled\/\*\.conf;' /etc/nginx/nginx.conf
	sudo systemctl enable mariadb
	sudo systemctl restart mariadb
	sudo systemctl enable nginx
	sudo firewall-cmd --permanent --add-service=http
	sudo firewall-cmd --reload
	sudo sed -i 's|^SELINUX=.*|SELINUX=disabled|' /etc/selinux/config
fi
sudo systemctl restart nginx

# Configuration de la base de données
db_exists=$(sudo mysql -e "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME='madis';" | grep -q 'madis' && echo "true" || echo "false")
if [ "$db_exists" == "true" ]; then
    echo "La base de données 'madis' existe déjà. Elle sera supprimée."
    sudo mysql -e "DROP DATABASE madis;"
fi
user_exists=$(sudo mysql -e "SELECT User FROM mysql.user WHERE User='madis';" | grep -q 'madis' && echo "true" || echo "false")
if [ "$user_exists" == "true" ]; then
    echo "L'utilisateur 'madis' existe déjà. Il sera supprimé."
    sudo mysql -e "DROP USER madis@localhost;"
fi

sudo mysql <<MYSQL_SCRIPT
CREATE DATABASE madis;
CREATE USER madis@'localhost' IDENTIFIED BY '$db_password';
GRANT ALL PRIVILEGES ON madis.* TO 'madis'@'localhost';
FLUSH PRIVILEGES;
QUIT
MYSQL_SCRIPT

# Modification des permissions
sudo chown -R $nginxusr: /var/www

# Clonage du dépôt
if [ -d "/var/www/madis" ]; then
    echo "Le répertoire /var/www/madis existe déjà. Il sera supprimé."
    sudo rm -rf /var/www/madis
fi
sudo -u $nginxusr git clone https://gitlab.adullact.net/soluris/madis.git /var/www/madis

# Copie du fichier .env.dist
sudo -u $nginxusr cp /var/www/madis/.env.dist /var/www/madis/.env

# Configuration de .env
sudo sed -i "s|DATABASE_URL=.*|DATABASE_URL=mysql://madis:$db_password@localhost:3306/madis|" /var/www/madis/.env
sudo sed -i "s|APP_ENV=.*|APP_ENV=prod|" /var/www/madis/.env
sudo sed -i "s|APP_URL=.*|APP_URL=$proto://$domain_name|" /var/www/madis/.env
sudo sed -i "s|APP_CONNEXION_STATUS_KNOW=.*|APP_CONNEXION_STATUS_KNOW=true|" /var/www/madis/.env
sudo sed -i "s|APP_COMMUNE_SVG_REDUCED_PATH=.*|APP_COMMUNE_SVG_REDUCED_PATH=''|" /var/www/madis/.env
sudo sed -i "s|MAILER_DSN=.*|MAILER_DSN=smtp://$smtp_username:smtp_password@$smtp_server:$smtp_port|" /var/www/madis/.env

# Checkout du dépôt
sudo -u $nginxusr git --git-dir=/var/www/madis/.git --work-tree=/var/www/madis checkout $version_madis

# Déploiement
sudo -u $nginxusr bash -c "cd /var/www/madis && bin/deploy"

# Insertion des données initiales dans la base de données
sudo mysql madis <<MYSQL_SCRIPT
use madis;
INSERT INTO user_collectivity (id, name, short_name, type, siren, active, created_at, updated_at, address_line_one, address_city, address_zip_code, address_insee, different_dpo, different_it_manager) VALUES ('aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee', 'Administration', 'Admin', 'other', 111111111, 1, NOW(), NOW(), 'A DEFINIR', 'A DEFINIR', 11111, '0000', 0, 0);
INSERT INTO user_user (id, collectivity_id, first_name, last_name, email, password, roles, enabled, forget_password_token) VALUES (UUID(), 'aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee', 'Admin', 'Admin', '$user_name', '$password_hash', '[\"ROLE_ADMIN\"]', 1, NULL);
QUIT
MYSQL_SCRIPT

# Commenter cette ligne pour une installation sans trace
curl -sS --connect-timeout 2 -m 5 "https://statistiques-sites.soluris.fr/matomo.php?idsite=114&rec=1" > /dev/null || true

# Installation des modèles AIPD et des référentiels
sudo -u $nginxusr bash -c "cd /var/www/madis &&  bin/console aipd:model:import fixtures/default/aipd/"
sudo -u $nginxusr bash -c "cd /var/www/madis &&  ./bin/console maturity:referentiel:import fixtures/default/maturity/"

# Cron des notifications
cron_content="1 * * * * $nginxusr cd /var/www/madis && $(which php8.1) bin/console notifications:generate
31 * * * * $nginxusr cd /var/www/madis && $(which php8.1) bin/console notifications:send"
cron_file="/etc/cron.d/madis"
sudo touch $cron_file
sudo rm -f $cron_file
echo "$cron_content" | sudo tee "$cron_file" > /dev/null
sudo chmod 644 "$cron_file"

recipient=$user_name
subject="Madis a correctement été installé"
body="L'installation de Madis est terminée.

Vous pouvez vous connecter avec les informations suivantes :

URL : $proto://$domain_name
Login : $user_name

Soluris"
base64_encode() {
    echo -n "$1" | base64 | tr -d '\n'
}
encoded_username=$(base64_encode "$smtp_username")
encoded_password=$(base64_encode "$smtp_password")
{
sleep 0.5
echo "EHLO example.com"
sleep 0.3
echo "AUTH LOGIN"
sleep 0.3
echo "$encoded_username"
sleep 0.3
echo "$encoded_password"
sleep 0.3
echo "MAIL FROM: <$from_address>"
sleep 0.3
echo "RCPT TO: <$recipient>"
sleep 0.3
echo "DATA"
sleep 0.3
echo "From: $from_address"
echo "To: $recipient"
echo "Subject: $subject"
echo ""
echo "$body"
echo "."
sleep 0.3
echo "QUIT"
} | telnet "$smtp_server" "$smtp_port" > /dev/null 2>&1

# Redémarrage de Nginx
sudo systemctl restart nginx

echo "L'installation de l'application Madis est terminée.

URL : $proto://$domain_name
Login : $user_name
Pass : $user_password

"
whiptail --msgbox "L'installation de Madis est terminée.\n\nVous pouvez vous connecter avec les informations suivantes :\n\nURL : $proto://$domain_name\nLogin : $user_name\nPass : $user_password
" 15 80

if [ "$systemOS" = "rocky" ]; then
	if whiptail --title "Confirmation" --yesno "Le système doit redémarrer, voulez vous vraiment redémarrer ?" --yes-button "Ok" --no-button "Noonnnn !!!!!" 8 60; then
		# Si l'utilisateur choisit "Oui", afficher un message d'information et redémarrer
		whiptail --title "Information" --msgbox "Le serveur va redémarrer dans quelques secondes..." 8 60
		reboot
	else
		# Si l'utilisateur ne répond pas dans les 5 secondes, afficher un message et redémarrer
		whiptail --title "Information" --msgbox "Redémarrage annulé." 8 60
	fi
fi

else
    echo "L'utilisateur a annulé."
    exit 0
fi
