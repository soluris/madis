Madis - Installation
====================

## Table des matières

- [Installer Madis par script](1-installation-script.md#installer-madis-par-script)
    - [Pré-requis technique](1-installation-automatisee.md#1-pré-requis-technique)
    - [Étapes d'installation](1-installation-automatisee.md#2-étapes-dinstallation)
    - [Installation](1-installation-automatisee.md#3-installation)
- [Installer Madis manuellement](2-installation-manuelle.md#installer-madis-manuellement)
    - [Pré-requis technique](2-installation-manuelle.md#1-pré-requis-technique)
    - [Cloner le dépôt Git de Madis](2-installation-manuelle.md#2-cloner-le-dépôt-git-de-madis)
    - [Configurer les services](2-installation-manuelle.md#3-configurer-les-services)
    - [Paramétrer et déployer l'application Madis](2-installation-manuelle.md#4-paramétrer-et-déployer-lapplication-madis)
    - [Installer Wkhtmltopdf](2-installation-manuelle.md#5-installer-wkhtmltopdf)
    - [(Option) Configuration API](2-installation-manuelle.md#6-option-configuration-api)
