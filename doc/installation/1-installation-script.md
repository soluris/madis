Installer Madis par script
============

1. [Pré-requis technique](#1-pré-requis-technique)
2. [Étapes d'installation](#2-étapes-dinstallation)
3. [Installation](#3-installation)

**Cette méthode d'installation doit être utilisée uniquement sur un système d'exploitation fraîchement installé.**

## 1. Pré-requis technique

Pour installer Madis via un script, il vous faudra être sur l'un des systèmes d'exploitation suivant :
-	Ubuntu 22 ; Ubuntu 24 ;
-	Debian GNU/Linux 12 ;
-	Rocky Linux.

Configuration matérielle conseillée :
- 1 CPU ;
- Minimum 2 Go de mémoire ;
- 80 Go de stockage système.

## 2. Étapes d'installation

L'installateur permet de réaliser les étapes suivantes :
1. Nom de domaine de l'application ;
2. Mot de passe pour la base de données ;
3. Email (Permet de recevoir un email de confirmation d'installation, et sert comme identifiant de connexion à Madis) ;
4. Mot de passe pour l'utilisateur (sert comme mot de passe de connexion à Madis) ;
5. (Optionnel) Recevoir un email avec les informations de connexion ;
6. (Optionnel) Paramétrage d'un serveur SMTP ;
7. Installation automatique et paramétrage des éléments suivants :
    -	Installation de divers éléments nécessaires (PHP-FPM, NGINX, Curl, git, NodeJS, Composer, Wkhtmltopdf...), des extensions PHP (php8.1-bz2 php8.1-cli php8.1-common php8.1-curl php8.1-fpm php8.1-gd php8.1-intl php8.1-mbstring php8.1-mysql php8.1-opcache php8.1-readline php8.1-xml php8.1-zip), ainsi qu'une base de données MySQL (ou MariaDB pour Rocky Linux) ;
    - Installation de Madis dans la version souhaitée ;
    - Paramétrage du .env ;
    - Ajout d'une structure et d'un utilisateur ;
    - Installation des modèles AIPD et des référentiels ;
    - Initialisation du Cron des notifications.

## 3. Installation

Vous devez être en root ou avoir les privilèges avec Sudo pour exécuter ce script.

**Attention : Si vous avez déjà une installation de Madis, celle-ci va être écrasée.**

[Télécharger le fichier d'installation Madis (SH - 16 Ko)](install.sh) ou utilisez la commande suivante :
```bash
wget --no-check-certificate https://gitlab.adullact.net/soluris/madis/-/raw/master/doc/installation/install.sh
```

Rendez le fichier exécutable.
```bash
chmod +x install.sh
```

Puis installez le fichier.
```bash
./install.sh
```

Une fois l'installation terminée, vous pouvez vous connecter sur l'URL paramétrée avec le compte créé lors de l'installation, [consulter la documentation utilisateur](https://documentation-madis.readthedocs.io/fr/latest/docutilisateur.html), ou paramétrer les [variables d'environnements](../maintenance-et-gestion-application/1-parametrages-complementaires-et-env.md#paramétrage-complémentaire-et-env).

**Note : Dans le fichier .env, pensez à modifier la chaîne de caractère utilisée dans la sécurité de votre application lors de votre première installation.**
```bash
APP_SECRET=a98f56b9ea67f189df8ed6a39c548503
```
