Installer Madis manuellement
============

1. [Pré-requis technique](#1-pré-requis-technique)
2. [Cloner le dépôt Git de Madis](#2-cloner-le-dépôt-git-de-madis)
3. [Configurer les services](#3-configurer-les-services)
    - [Configurer NGINX](#configurer-nginx)
    - [Configurer MySQL/MariaDB](#configurer-mysqlmariadb)
4. [Paramétrer et déployer l'application Madis](#4-paramétrer-et-déployer-lapplication-madis)
5. [Installer Wkhtmltopdf](#5-installer-wkhtmltopdf)
6. [(Option) Configuration API](#6-option-configuration-api)

## 1. Pré-requis technique

Madis peut être installé sur l'un des systèmes d'exploitation suivant :
-	Ubuntu 22 ;
-	Debian GNU/Linux 12 ;
-	Rocky Linux.

D'autres systèmes d'exploitation peuvent être compatibles avec l'application, cependant seuls ceux dans les versions énumérées ci-dessus ont été testés.

Configuration matérielle conseillée :
- 1 CPU ;
- Minimum 2 Go de mémoire ;
- 80 Go de stockage système.

Pour installer Madis manuellement, il vous faudra installer la stack technique dans les versions suivantes :
-	PHP-FPM v8.1 ([Pour Ubuntu](https://www.linuxcapable.com/how-to-install-php-8-1-on-ubuntu-22-04-lts/) / [Pour Debian v11 (Bullseye)](https://www.it-connect.fr/installation-de-php-8-1-sur-debian-11-pour-son-serveur-web/)) ;
-	NGINX v1.23 ;
-	MySQL v8.0 (ou MariaDB à partir de v10.8.3 : [Pour Debian v11 (Bullseye)](https://aymeric-cucherousset.fr/en/install-mariadb-on-debian-11/)) ;
-	[NodeJS v18.x](https://github.com/nodesource/distributions?tab=readme-ov-file#deb-supported-versions) ;
-	[Composer](https://getcomposer.org/download/) à partir de v2.7 ;
-	Git ;
-	npm à partir de v8 ;
- [Wkhtmltopdf v0.12.5](#installer-wkhtmltopdf) ;
- Ainsi que des extensions PHP : *php8.1-apcu php8.1-common php-fdomdocument php8.1-xml php8.1-cli php8.1-curl php8.1-fpm php8.1-gd php8.1-intl php8.1-json php8.1-mbstring php8.1-mysql php8.1-opcache php8.1-readline php8.1-bz2 php8.1-zip* ;
- Seulement pour Debian GNU/Linux 12 : [YARN v1+](https://classic.yarnpkg.com/en/docs/install#debian-stable).

L'application peut néanmoins être compatible avec d'autres versions, mais le support a lieu sur les versions énumérées ci-dessus.

Vérifier bien la version des binaires installés avant de continuer l'installation.

## 2. Cloner le dépôt Git de Madis

Modifier les permissions d'accès.
```bash
sudo chown -R www-data: /var/www
```

Pour cloner le projet, placez-vous dans le chemin suivant.
```bash
cd /var/www
```

Clonez Madis via la commande suivante.
```bash
sudo -u www-data git clone https://gitlab.adullact.net/soluris/madis.git /var/www/madis
```

Enfin, placez-vous dans le chemin suivant.
```bash
cd /var/www/madis
```

## 3. Configurer les services

### Configurer NGINX

NGINX doit être configuré pour recevoir Madis. La configuration devra respecter les conditions suivantes :
-	Toujours rediriger `/` sur `/index.php` si la ressource statique n’a pas su être servie ;
-	Utiliser « `/index.php` » lance PHP-FPM pour exécuter Symfony via son Front Controller ;
-	Rediriger tout autre appel en 404.

Tous les détails de la configuration sont exposés ici : [Configuring a Web Server (En anglais)](https://symfony.com/doc/current/setup/web_server_configuration.html#nginx).

Faire pointer NGINX sur le chemin `/var/www/madis/public`.

Pour tester rapidement la configuration, vous pouvez ajouter les fichiers correspondant dans le dossier « public » :
-	index.php (avec un `phpinfo();` ) par exemple ;
-	image.png (ou tout autre image statique).

Si vous avez suivi la documentation Symfony, vous obtiendrez les comportements suivants :
-	`/` vous amènera sur la page de connexion Madis ;
-	`/index.php` ne fonctionnera pas (la clause NGINX est en `internal`, donc inaccessible depuis l’URL) ;
-	`/image.png` vous affichera votre image.

Exemple de fichier de configuration NGINX :
```nginx
server {
    server_name madis.soluris.fr;
    root /var/www/madis/public;

    location / {
        # try to serve file directly, fallback to index.php
        try_files $uri /index.php$is_args$args;
    }
	client_max_body_size 10M;
    location ~ ^/index\.php(/|$) {
        fastcgi_pass unix:/var/run/php/php7.1-fpm.sock;
        fastcgi_split_path_info ^(.+\.php)(/.*)$;
        include fastcgi_params;

        fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
        fastcgi_param DOCUMENT_ROOT $realpath_root;

        internal;
    }

    # return 404 for all other php files not matching the front controller
    # this prevents access to other php files you don't want to be accessible.
    location ~ \.php$ {
        return 404;
    }

    error_log /var/log/nginx/madis_error.log;
    access_log /var/log/nginx/madis_access.log;
}
```

### Configurer MySQL/MariaDB

Lors de l’installation de MySQL ou MariaDB, vous avez dû créer un utilisateur root pour MySQL. Vous allez maintenant créer un utilisateur pour l’application Madis.

Connectez-vous avec ce compte root afin de pouvoir créer les accès utilisateur.

Créer une base de données pour Madis.
```mysql
CREATE DATABASE madis ;
```

Créer un utilisateur pour accéder à la base de données. Remplacer *MonMotDePasse* par un mot de passe fort.
```mysql
CREATE USER madis@'localhost' IDENTIFIED BY 'MonMotDePasse';
```

Donner les accès à la base de données au nouvel utilisateur :
```mysql
GRANT ALL PRIVILEGES ON madis.* TO 'madis'@'localhost';
```

Enfin, modifier le fichier `/etc/my.cnf` en y ajoutant ou remplaçant les lignes suivantes :
```mysql
[mysqld]
sql_mode=STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION
```

Votre base de données est maintenant opérationnelle. Tentez de vous connecter en ligne de commande via `mysql -u madis -p`. Si vous réussissez à vous connecter, tout est en place.

## 4. Paramétrer et déployer l'application Madis

Copier le fichier *.env.dist* en *.env*.
```bash
sudo -u www-data cp /var/www/madis/.env.dist /var/www/madis/.env
```

Supprimez la partie `###> DOCKER ###`, et complétez les informations restantes par celles vous concernant. Pour plus d'informations, consultez le [paramétrage complémentaire](../maintenance-et-gestion-application/1-parametrages-complementaires-et-env.md#paramétrage-complémentaire-et-env) du .env.

**Attention : Modifiez la chaîne de caractère utilisée dans la sécurité de votre application lors de votre première installation.**
```bash
APP_SECRET=a98f56b9ea67f189df8ed6a39c548503
```

Modifier les informations de connexion la base de données (En savoir plus sur le [format à utiliser](http://docs.doctrine-project.org/projects/doctrine-dbal/en/latest/reference/configuration.html#connecting-using-a-url)). Remplacer *MonMotDePasse* par un mot de passe fort.
```bash
DATABASE_URL=mysql://user:MonMotDePasse@db:3306/madis
```

Vérifiez que Madis communique bien avec la base de données avec la commande suivante.
```bash
bin/console doctrine:schema:update --dump-sql
```

Si des lignes SQL s’affichent, vous communiquez bien avec la base de données (cette commande vous affiche le différentiel entre le schéma de la base de données et Madis). N’en faites rien, on va créer le schéma à l’étape suivante.

Pour consulter la liste des versions disponibles.
```shell
sudo -u www-data git tag
```

Puis placez-vous dans la version à utiliser.
```bash
sudo -u www-data git checkout v2.4.9
```

Enfin, lancez la commande suivante pour finaliser l’installation de Madis.
```bash
sudo -u www-data ./bin/deploy
```

Il ne reste plus qu'à créer une structure et un utilisateur qui devra être associé à une structure (L'`id` de la structure doit correspondre au `collectivity_id` de l'utilisateur).

Connectez-vous à votre base de données.
```bash
mysql -u madis -p
```

**Les commandes suivantes peuvent être utilisées telles qu'elles. Modifier les informations directement dans l'application pour une saisie plus aisée.**

La commande suivante permet de créer une structure. Remplacer les différentes informations *NomDeLaStructure*, *NomCourtDeLaStructure*, *MonAdresse*, ou encore *MaVille*.
```mysql
INSERT INTO user_collectivity (id, name, short_name, type, siren, active, created_at, updated_at, address_line_one, address_city, address_zip_code, address_insee, different_dpo, different_it_manager) VALUES ('aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee', 'NomDeLaStructure', 'NomCourtDeLaStructure', 'other', 111111111, 1, NOW(), NOW(), 'MonAdresse', 'MaVille', 11111, '0000', 0, 0);
```

Générer un mot de passe chiffré. Remplacer *MonMotDePasse* par un mot de passe fort. Il devra être coller à la place de *MonMotDePasseChiffré* dans la commande suivante de création d'utilisateur.
```bash
php -r "echo (password_hash('MonMotDePasse',1));"
```

La commande suivante permet de créer un utilisateur pour accéder à Madis. Remplacer les différentes informations *MonPrénom*, *MonNom*, *MonAdresseEmail@Domaine.fr* ou encore *MonMotDePasseChiffré*.
```mysql
INSERT INTO user_user (id, collectivity_id, first_name, last_name, email, password, roles, enabled, forget_password_token) VALUES (UUID(), 'aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee', 'MonPrénom', 'MonNom', 'MonAdresseEmail@Domaine.fr', 'MonMotDePasseChiffré', '[\"ROLE_ADMIN\"]', 1, NULL);
```

Pour installer le modèle AIPD par défaut, lancez la commande suivante.
```bash
sudo -u www-data ./bin/console aipd:model:import fixtures/default/aipd/
```

Pour installer les référentiels de maturité par défaut, lancez la commande suivante.
```bash
sudo -u www-data ./bin/console maturity:referentiel:import fixtures/default/maturity/
```

Vous pouvez maintenant vous connecter à Madis avec le compte paramétré, [consulter la documentation utilisateur](https://documentation-madis.readthedocs.io/fr/latest/docutilisateur.html), ou paramétrer les [variables d'environnements](../maintenance-et-gestion-application/1-parametrages-complementaires-et-env.md#paramétrage-complémentaire-et-env).

## 5. Installer Wkhtmltopdf

Afin de permettre la génération des PDFs par l'application, l'installation du binaire wkhtmltopdf est nécessaire. Il est nécessaire, pour l'environnement de production de télécharger manuellement wkhtmltopdf.

Il faut d'abord vérifier la version de l'OS du serveur.
```bash
tail /etc/os-release
```
Résultat :
```bash
PRETTY_NAME="Debian GNU/Linux 10 (buster)"
NAME="Debian GNU/Linux"
VERSION_ID="10"
VERSION="10 (buster)"
VERSION_CODENAME=buster
ID=debian
HOME_URL="https://www.debian.org/"
SUPPORT_URL="https://www.debian.org/support"
BUG_REPORT_URL="https://bugs.debian.org/"
```

La version dans l'exemple ci-dessus est donc `buster`.

**Comme la version de wkhtmltopdf actuellement utilisée par l'application n'est pas la dernière, mais la 0.12.5**, il faut se rendre sur le GitHub [wkhtmltopdf](https://github.com/wkhtmltopdf/wkhtmltopdf/releases/0.12.5/) afin de sélectionner le bon binaire. On choisira le binaire pour amd64 correspondant à la version de l'OS.

Une fois le binaire sélectionné, copier le lien de téléchargement et téléchargez-le via wget.
```bash
wget https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.5/wkhtmltox_0.12.5-1.buster_amd64.deb
```

Puis, installer les pré-requis de wkhtmltopdf pour fonctionner.
```bash
apt-get install fontconfig libfreetype6 libjpeg62-turbo libpng16-16 libxrender1 xfonts-75dpi xfonts-base
```

Enfin, installer le binaire.
```bash
dpkg -i wkhtmltox_0.12.5-1.buster_amd64.deb
```

Vérifier la bonne configuration des variables d'environnement suivantes dans le .env, et modifier les chemins si nécessaire.
```
WKHTMLTOPDF_PATH=/usr/local/bin/wkhtmltopdf
WKHTMLTOIMAGE_PATH=/usr/local/bin/wkhtmltoimage
```

## 6. (Option) Configuration API

- [Documentation Api platform](https://api-platform.com/)
- [Documentation LexikJWTAuthentication](https://github.com/lexik/LexikJWTAuthenticationBundle)

Générer des clés publiques et privées pour le JWT.
```shell
sudo -u www-data php ./bin/console lexik:jwt:generate-keypair
```

La documentation de l'API est disponible depuis Madis en ajoutant à l’URL du **nom de domaine Madis** : `/api/docs`.

Exemple : `madis-exemple.fr/api/docs`

Pour utiliser la documentation, le chemin `/login` permet de récupérer son `Bearer token` afin de s'authentifier et de pouvoir accéder aux autres requêtes.
1. Lancez la requête `/login` ;
2. Remplissez votre identifiant et votre mot de passe ;
3. `Execute` ;
4. Copiez le token ;
5. Cliquez sur « **Authentification** » puis écrivez `Bearer collezVotreToken` ;
6. `Login` ;
7. Vous êtes authentifié. Vous pouvez ensuite utiliser les autres requêtes.
