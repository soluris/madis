#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#

extensions = ['myst_parser',
              'sphinxcontrib.opendataservices',
              'sphinxcontrib.jsonschema',
              'sphinx_rtd_theme',
              'sphinx.ext.todo',
              'sphinx.ext.autodoc',
              'sphinx_togglebutton',]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']


source_suffix = ['.rst', '.md']
master_doc = 'index'
project = 'Madis'
copyright = 'Madis Soluris'
author = 'Soluris'
html_logo = "images/logo_madis_2020_blanc.png"
html_title = "Documentation Madis"


language = "fr"
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']

pygments_style = 'sphinx'
todo_include_todos = False

html_theme = 'sphinx_rtd_theme'

html_css_files = [
    'custom.css',
]
html_static_path = ['_static']
htmlhelp_basename = 'sphinxdoc'

# -- Options for LaTeX output ---------------------------------------------

latex_elements = {
     # 'figure_align': 'htbp',
}

latex_documents = [
    (master_doc, 'sphinx.tex', 'Documentation Madis',
     'Soluris', 'manual'),
]
# One entry per manual page. List of tuples
# (source start file, name, description, authors, manual section).
man_pages = [
    (master_doc, 'sphinx', 'Documentation Madis',
     [author], 1)
]

# If true, show URL addresses after external links.
#
# man_show_urls = False


# -- Options for Texinfo output -------------------------------------------

# Grouping the document tree into Texinfo files. List of tuples
# (source start file, target name, title, author,
#  dir menu entry, description, category)
texinfo_documents = [
    (master_doc, 'sphinx', 'Documentation Madis',
     author, 'sphinx', 'Opensource Madis',
     'Miscellaneous'),
]

# Documents to append as an appendix to all manuals.
#
# texinfo_appendices = []

# If false, no module index is generated.
#
# texinfo_domain_indices = True

# How to display URL addresses: 'footnote', 'no', or 'inline'.
#
# texinfo_show_urls = 'footnote'

# If true, do not generate a @detailmenu in the "Top" node's menu.
#
# texinfo_no_detailmenu = False


locale_dirs = ['locale/']   # path is example but recommended.
gettext_compact = False     # optional.

togglebutton_hint = ""
