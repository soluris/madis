<?php

/**
 * This file is part of the MADIS - RGPD Management application.
 *
 * @copyright Copyright (c) 2018-2019 Soluris - Solutions Numériques Territoriales Innovantes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace App\Domain\Reporting\Controller;

use App\Application\Symfony\Security\UserProvider;
use App\Application\Traits\ServersideDatatablesTrait;
use App\Domain\Maturity\Repository as MaturityRepository;
use App\Domain\Registry\Repository;
use App\Domain\Reporting\Handler\WordHandler;
use App\Infrastructure\ORM\Registry\Repository\ConformiteOrganisation\Evaluation;
use App\Infrastructure\ORM\User\Repository\Collectivity as CollectivityRepository;
use PhpOffice\PhpWord\PhpWord;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\String\Slugger\AsciiSlugger;

class ReviewController extends AbstractController
{
    use ServersideDatatablesTrait;
    /**
     * @var WordHandler
     */
    private $wordHandler;

    /**
     * @var UserProvider
     */
    private $userProvider;

    /**
     * @var AuthorizationCheckerInterface
     */
    private $authorizationChecker;

    /**
     * @var Repository\Treatment;
     */
    private $treatmentRepository;

    /**
     * @var Repository\Contractor;
     */
    private $contractorRepository;

    /**
     * @var Repository\Tool;
     */
    private $toolRepository;

    /**
     * @var Repository\Mesurement;
     */
    private $mesurementRepository;

    /**
     * @var Repository\Request
     */
    private $requestRepository;

    /**
     * @var MaturityRepository\Survey;
     */
    private $surveyRepository;

    /**
     * @var Repository\Violation
     */
    private $violationRepository;

    /**
     * @var Evaluation
     */
    private $evaluationRepository;

    /**
     * @var Security
     */
    protected $security;

    public function __construct(
        WordHandler $wordHandler,
        UserProvider $userProvider,
        AuthorizationCheckerInterface $authorizationChecker,
        Repository\Treatment $treatmentRepository,
        Repository\Contractor $contractorRepository,
        Repository\Mesurement $mesurementRepository,
        Repository\Request $requestRepository,
        Repository\Violation $violationRepository,
        Repository\Tool $toolRepository,
        MaturityRepository\Survey $surveyRepository,
        CollectivityRepository $repository,
        Repository\ConformiteOrganisation\Evaluation $evaluationRepository,
        Security $security,
    ) {
        $this->wordHandler          = $wordHandler;
        $this->userProvider         = $userProvider;
        $this->authorizationChecker = $authorizationChecker;
        $this->treatmentRepository  = $treatmentRepository;
        $this->contractorRepository = $contractorRepository;
        $this->mesurementRepository = $mesurementRepository;
        $this->requestRepository    = $requestRepository;
        $this->violationRepository  = $violationRepository;
        $this->surveyRepository     = $surveyRepository;
        $this->repository           = $repository;
        $this->evaluationRepository = $evaluationRepository;
        $this->toolRepository       = $toolRepository;
        $this->security             = $security;
    }

    /**
     * Download an entire review.
     *
     * @throws \PhpOffice\PhpWord\Exception\Exception
     * @throws \Exception
     */
    public function indexAction(string $id): BinaryFileResponse
    {
        /**
         * @var CollectivityRepository $repo
         */
        $repo = $this->repository;
        $user = $this->userProvider->getAuthenticatedUser();
        // By default get the users collectivity, ignoring the ID passed in the URL
        $collectivity = $user->getCollectivity();

        if ($id && $this->security->isGranted('ROLE_REFERENT') && !$this->security->isGranted('ROLE_ADMIN')) {
            // If the user is a referent and they are part of this collectivity
            foreach ($user->getCollectivitesReferees() as $col) {
                if ($col->getId() === $id) {
                    $collectivity = $repo->findOneById($id);
                }
            }
        }

        if ($this->security->isGranted('ROLE_ADMIN')) {
            $collectivity = $repo->findOneById($id);
        }

        if (!$collectivity) {
            throw new NotFoundHttpException('No collectivity found');
        }

        $maturity = [];
        // Get most recent maturity index that has a referentiel
        $objects = $this->surveyRepository->findAllByCollectivity($collectivity, ['createdAt' => 'DESC'], 1, ['o.referentiel is not null']);

        if (1 <= \count($objects)) {
            $maturity['new'] = $objects[0];

            $previous = $this->surveyRepository->findAllByCollectivity($collectivity, ['createdAt' => 'DESC'], 2, ['referentiel' => $maturity['new']->getReferentiel()]);
            if (2 <= \count($previous)) {
                $maturity['old'] = $previous[1];
            }
        }

        $this->wordHandler->setCollectivity($collectivity);

        return $this->wordHandler->generateOverviewReport(
            $this->treatmentRepository->findAllActiveByCollectivity($collectivity),
            $this->contractorRepository->findAllByCollectivity($collectivity),
            $this->mesurementRepository->findAllByCollectivity($collectivity),
            $maturity,
            $this->requestRepository->findAllArchivedByCollectivity($collectivity, false),
            $this->violationRepository->findAllArchivedByCollectivity($collectivity, false),
            $this->toolRepository->findAllByCollectivity($collectivity),
            $this->evaluationRepository->findLastByOrganisation($collectivity)
        );
    }

    /**
     * Download a zip of all visible reviews.
     *
     * @throws \PhpOffice\PhpWord\Exception\Exception
     * @throws \Exception
     */
    public function zipAction(Request $request): Response
    {
        $ids = $request->get('ids');
        /** @var CollectivityRepository $repository */
        $repository     = $this->repository;
        $collectivities = $repository->findByIds($ids);

        $files = [];
        foreach ($collectivities as $collectivity) {
            $maturity = [];
            // Get most recent maturity index that has a referentiel
            $objects = $this->surveyRepository->findAllByCollectivity($collectivity, ['createdAt' => 'DESC'], 1, ['o.referentiel is not null']);

            if (1 <= \count($objects)) {
                $maturity['new'] = $objects[0];

                $previous = $this->surveyRepository->findAllByCollectivity($collectivity, ['createdAt' => 'DESC'], 2, ['referentiel' => $maturity['new']->getReferentiel()]);
                if (2 <= \count($previous)) {
                    $maturity['old'] = $previous[1];
                }
            }
            $this->wordHandler->setCollectivity($collectivity);
            $this->wordHandler->setDocument(new PhpWord());
            $response = $this->wordHandler->generateOverviewReport(
                $this->treatmentRepository->findAllActiveByCollectivity($collectivity),
                $this->contractorRepository->findAllByCollectivity($collectivity),
                $this->mesurementRepository->findAllByCollectivity($collectivity),
                $maturity,
                $this->requestRepository->findAllArchivedByCollectivity($collectivity, false),
                $this->violationRepository->findAllArchivedByCollectivity($collectivity, false),
                $this->toolRepository->findAllByCollectivity($collectivity),
                $this->evaluationRepository->findLastByOrganisation($collectivity),
            );

            $files[] = ['file' => $response->getFile(), 'collectivity' => $collectivity];
        }

        // Zip all files

        // Create new Zip Archive.
        $zip = new \ZipArchive();

        // The name of the Zip documents.
        $currentDate = (new \DateTimeImmutable())->format('Ymd');
        $zipName     = "bilans-{$currentDate}.zip";
        @unlink($zipName);
        $zip->open($zipName, \ZipArchive::CREATE);
        $used = [];
        foreach ($files as $file) {
            $slugger = new AsciiSlugger();
            $cname   = $slugger->slug($file['collectivity']->getShortName())->lower()->toString();
            $newname = $cname;
            $i       = 1;
            while (in_array($newname, $used)) {
                $newname = $cname . '-' . $i;
                ++$i;
            }
            $used[]      = $newname;
            $zipFileName = "{$currentDate}-bilan-{$newname}.doc";
            $zip->addFromString($zipFileName, file_get_contents($file['file']->getRealPath()));
        }
        $zip->close();

        $response = new Response(file_get_contents($zipName));

        $response->headers->set('Content-Type', 'application/zip');
        $response->headers->set('Content-length', filesize($zipName));
        $response->headers->set('Content-Disposition', 'attachment; filename="' . basename($zipName) . '";');

        @unlink($zipName);

        return $response;
    }

    /**
     * Download a zip of all visible reviews.
     *
     * @throws \PhpOffice\PhpWord\Exception\Exception
     * @throws \Exception
     */
    public function zipConfirmAction(Request $request): Response
    {
        $ids = $request->get('ids');
        /** @var CollectivityRepository $repository */
        $repository     = $this->repository;
        $collectivities = $repository->findByIds($ids);

        return $this->render('User/Collectivity/confirm_generate_bilans.html.twig', [ // delete_all
            'collectivities' => $collectivities,
        ]);
    }

    public function listDataTables(Request $request): JsonResponse
    {
        // Do nothing here
        return new JsonResponse();
    }

    protected function getLabelAndKeysArray(): array
    {
        return [
            'nom',
            'nom_court',
            'type',
            'informations_complementaires',
            'statut',
            'date_maj',
            'population',
            'nbr_agents',
            'nbr_cnil',
            'createdAt',
            'updatedAt',
            'actions',
            'id',
        ];
    }
}
