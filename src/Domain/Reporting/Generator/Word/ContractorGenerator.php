<?php

/**
 * This file is part of the MADIS - RGPD Management application.
 *
 * @copyright Copyright (c) 2018-2019 Soluris - Solutions Numériques Territoriales Innovantes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace App\Domain\Reporting\Generator\Word;

use App\Domain\Registry\Model\Contractor;
use App\Domain\User\Dictionary\ContactCivilityDictionary;
use PhpOffice\PhpWord\Element\Section;

class ContractorGenerator extends AbstractGenerator implements ImpressionGeneratorInterface
{
    /**
     * Global overview : data to display for contractors in overview report.
     *
     * @param Contractor[] $data
     */
    public function addGlobalOverview(Section $section, array $data): void
    {
        if ($this->collectivity) {
            $collectivity = $this->collectivity;
        } else {
            $collectivity = $this->userProvider->getAuthenticatedUser()->getCollectivity();
        }

        $overviews = [
            'Nom',
            'Référent',
            'Clauses contractuelles vérifiées',
            'A adopté les éléments de sécurité nécessaires',
            'Tient à jour un registre des traitements',
            'Les données restent dans l\'Union européenne',
        ];
        $nbContractors                = \count($data);
        $nbVerifiedContractualClauses = 0;
        $nbAdoptedSecurityFeatures    = 0;
        $nbMaintainsTreatmentRegister = 0;
        $nbSendingDataOutsideEu       = 0;

        foreach ($data as $contractor) {
            // Verified contractual clauses
            if ($contractor->isContractualClausesVerified()) {
                ++$nbVerifiedContractualClauses;
            }
            // Adopted security features
            if ($contractor->isAdoptedSecurityFeatures()) {
                ++$nbAdoptedSecurityFeatures;
            }
            // Maintains treatment register
            if ($contractor->isMaintainsTreatmentRegister()) {
                ++$nbMaintainsTreatmentRegister;
            }
            // Sending data outside EU
            if (!$contractor->isSendingDataOutsideEu()) {
                ++$nbSendingDataOutsideEu;
            }
        }

        $section->addTitle('Registre des sous-traitants', 2);

        if (empty($data)) {
            $section->addText('Il n’y a aucun sous-traitant identifié.');

            return;
        }

        $section->addText("Un recensement des sous-traitants gérants des données à caractère personnel de {$collectivity} a été effectué.");
        $section->addText('Il y a ' . (1 == $nbContractors ? ' un sous-traitant identifié.' : "{$nbContractors} sous-traitants identifiés."));
        $section->addListItem(
            0 == $nbVerifiedContractualClauses ? 'Aucune clause contractuelle n’a été vérifiée ;' :
            (1 == $nbVerifiedContractualClauses ? '1 clause contractuelle a été vérifiée ;' :
            "{$nbVerifiedContractualClauses} clauses contractuelles vérifiées ;")
        );
        $section->addListItem(
            0 == $nbAdoptedSecurityFeatures ? 'Aucun sous-traitant n’a adopté les éléments de sécurité nécessaires ;' :
            (1 == $nbAdoptedSecurityFeatures ? '1 sous-traitant a adopté les éléments de sécurité nécessaires ;' :
            "{$nbAdoptedSecurityFeatures} sous-traitants ont adopté les éléments de sécurité nécessaires ;")
        );
        $section->addListItem(
            0 == $nbMaintainsTreatmentRegister ? 'Aucun sous-traitant ne tient à jour un registre des traitements ;' :
            (1 == $nbMaintainsTreatmentRegister ? '1 sous-traitant tient à jour un registre des traitements ;' :
            "{$nbMaintainsTreatmentRegister} sous-traitants tiennent à jour un registre des traitements ;")
        );
        $section->addListItem(
            0 == $nbSendingDataOutsideEu ? "Aucun sous-traitant n'envoie des données hors Union européenne." :
            (1 == $nbSendingDataOutsideEu ? '1 sous-traitant envoie des données hors Union européenne.' :
            "{$nbSendingDataOutsideEu} sous-traitants envoient des données hors Union européenne.")
        );

        $ContractorsListTable = $section->addTable($this->tableStyle);
        $ContractorsListTable->addRow(null, ['tblHeader' => true, 'cantsplit' => true]);
        foreach ($overviews as $overviewRow) {
            $cell = $ContractorsListTable->addCell(1500, $this->cellHeadStyle);
            $cell->addText($overviewRow, $this->textHeadStyle);
        }

        // Make a loop to get all data. Make all data processing in one loop to avoid several loops
        foreach ($data as $contractor) {
            $ContractorsListTable->addRow(null, ['exactHeight' => true, 'cantsplit' => true]);
            $cell = $ContractorsListTable->addCell(1500);
            $cell->addText($contractor->getName());
            $cell = $ContractorsListTable->addCell(1500);
            $cell->addText($contractor->getReferent() ?? $this->parameterBag->get('APP_DEFAULT_REFERENT'));
            $cell = $ContractorsListTable->addCell(1500);
            $cell->addText($contractor->isContractualClausesVerified() ? 'Oui' : 'Non');
            $cell = $ContractorsListTable->addCell(1500);
            $cell->addText($contractor->isAdoptedSecurityFeatures() ? 'Oui' : 'Non');
            $cell = $ContractorsListTable->addCell(1500);
            $cell->addText($contractor->isMaintainsTreatmentRegister() ? 'Oui' : 'Non');
            $cell = $ContractorsListTable->addCell(1500);
            $cell->addText($contractor->isSendingDataOutsideEu() ? 'Oui' : 'Non');
        }
    }

    public function addSyntheticView(Section $section, array $data): void
    {
        $section->addTitle('Liste des sous-traitants', 1);

        // Table data
        // Add header
        $tableData = [
            [
                'Nom',
                'Référent',
                'Clauses contractuelles vérifiées',
                'Adopte les éléments de sécurité nécessaires',
                'Tient à jour un registre des traitements',
                'Les données restent dans l\'Union européenne',
            ],
        ];
        // Add content
        foreach ($data as $contractor) {
            $tableData[] = [
                $contractor->getName(),
                $contractor->getReferent() ?? $this->parameterBag->get('APP_DEFAULT_REFERENT'),
                $contractor->isContractualClausesVerified() ? 'Oui' : 'Non',
                $contractor->isAdoptedSecurityFeatures() ? 'Oui' : 'Non',
                $contractor->isMaintainsTreatmentRegister() ? 'Oui' : 'Non',
                $contractor->isSendingDataOutsideEu() ? 'Oui' : 'Non',
            ];
        }

        $this->addTable($section, $tableData, true, self::TABLE_ORIENTATION_VERTICAL);
        $section->addPageBreak();
    }

    public function addDetailedView(Section $section, array $data): void
    {
        $section->addTitle('Détail des sous-traitants', 1);

        /** @var Contractor $contractor */
        foreach ($data as $key => $contractor) {
            if (0 !== $key) {
                $section->addPageBreak();
            }
            $section->addTitle($contractor->getName(), 2);

            $generalInformationsData = [
                [
                    'Nom',
                    $contractor->getName(),
                ],
            ];

            // Ajouter les services si le module est actif
            if ($contractor->getCollectivity()->getIsServicesEnabled()) {
                $generalInformationsData[] = [
                    'Service',
                    $contractor->getService(),
                ];
            }

            $generalInformationsData = array_merge($generalInformationsData, [
                [
                    'Agent référent',
                    $contractor->getReferent() ?? $this->parameterBag->get('APP_DEFAULT_REFERENT'),
                ],
                [
                    'Clauses contractuelles vérifiées',
                    $contractor->isContractualClausesVerified() ? 'Oui' : 'Non',
                ],
                [
                    'Adopte les éléments de sécurité nécessaires',
                    $contractor->isAdoptedSecurityFeatures() ? 'Oui' : 'Non',
                ],
                [
                    'Tient à jour un registre des traitements',
                    $contractor->isMaintainsTreatmentRegister() ? 'Oui' : 'Non',
                ],
                [
                    'Les données restent dans l\'Union européenne',
                    $contractor->isSendingDataOutsideEu() ? 'Oui' : 'Non',
                ],
                [
                    'Autres informations',
                    $contractor->getOtherInformations(),
                ],
            ]);

            $addressData = [
                [
                    'Prénom',
                    $contractor->getLegalManager()->getFirstName(),
                ],
                [
                    'Nom',
                    $contractor->getLegalManager()->getLastName(),
                ],
                [
                    'Adresse',
                    $contractor->getAddress()->getLineOne(),
                ],
                [
                    'Complément d\'adresse',
                    $contractor->getAddress()->getLineTwo(),
                ],
                [
                    'Code postal',
                    $contractor->getAddress()->getZipCode(),
                ],
                [
                    'Ville',
                    $contractor->getAddress()->getCity(),
                ],
                [
                    'Pays',
                    $contractor->getAddress()->getCountry(),
                ],
                [
                    'Email',
                    $contractor->getAddress()->getMail(),
                ],
                [
                    'Téléphone',
                    $contractor->getAddress()->getPhoneNumber(),
                ],
            ];

            $dpoData = [
                [
                    'Le sous-traitant a désigné un DPD',
                    $contractor->isHasDpo() ? 'Oui' : 'Non',
                ],
            ];

            if ($contractor->isHasDpo()) {
                $dpoData = array_merge($dpoData, [
                    [
                        'Civilité',
                        !\is_null($contractor->getDpo()->getCivility()) ? ContactCivilityDictionary::getCivilities()[$contractor->getDpo()->getCivility()] : '',
                    ],
                    [
                        'Prénom',
                        $contractor->getDpo()->getFirstName(),
                    ],
                    [
                        'Nom',
                        $contractor->getDpo()->getLastName(),
                    ],
                    [
                        'Fonction',
                        $contractor->getDpo()->getJob(),
                    ],
                    [
                        'Email',
                        $contractor->getDpo()->getMail(),
                    ],
                    [
                        'Téléphone',
                        $contractor->getDpo()->getPhoneNumber(),
                    ],
                ]);
            }

            $historyData = [
                [
                    'Date de création',
                    $this->getDate($contractor->getCreatedAt()),
                ],
                [
                    'Date de modification',
                    $this->getDate($contractor->getUpdatedAt()),
                ],
                [
                    'Modifié par',
                    $contractor->getUpdatedBy(),
                ],
            ];

            $section->addTitle('Informations générales', 3);
            $this->addTable($section, $generalInformationsData, false, self::TABLE_ORIENTATION_VERTICAL);

            $section->addTitle('Coordonnées', 3);
            $this->addTable($section, $addressData, false, self::TABLE_ORIENTATION_VERTICAL);

            $section->addTitle('Délégué à la protection des données', 3);
            $this->addTable($section, $dpoData, false, self::TABLE_ORIENTATION_VERTICAL);

            $section->addTitle('Éléments associés', 3);
            $this->addLinkedData($section, $contractor);

            $section->addTitle('Historique', 3);
            $this->addTable($section, $historyData, false, self::TABLE_ORIENTATION_VERTICAL);
        }
    }
}
