<?php

/**
 * This file is part of the MADIS - RGPD Management application.
 *
 * @copyright Copyright (c) 2018-2019 Soluris - Solutions Numériques Territoriales Innovantes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace App\Domain\Reporting\Generator\Word;

use App\Application\Symfony\Security\UserProvider;
use App\Domain\Registry\Model\ConformiteOrganisation\Evaluation;
use App\Domain\User\Dictionary\ContactCivilityDictionary;
use App\Domain\User\Model\Collectivity;
use App\Domain\User\Model\ReviewData;
use PhpOffice\PhpWord\Element\Section;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class OverviewGenerator extends AbstractGenerator
{
    /**
     * @var TreatmentGenerator
     */
    protected $treatmentGenerator;

    /**
     * @var ContractorGenerator
     */
    protected $contractorGenerator;

    /**
     * @var MaturityGenerator
     */
    protected $maturityGenerator;

    /**
     * @var MesurementGenerator
     */
    protected $mesurementGenerator;

    /**
     * @var RequestGenerator
     */
    protected $requestGenerator;

    /**
     * @var ViolationGenerator
     */
    protected $violationGenerator;

    /**
     * @var ToolGenerator
     */
    protected $toolGenerator;

    /**
     * @var ConformiteTraitementGenerator
     */
    protected $conformiteTraitementGenerator;

    /**
     * @var ConformiteOrganisationGenerator
     */
    protected $conformiteOrganisationGenerator;

    /**
     * @var UserGenerator
     */
    protected $userGenerator;

    /**
     * @var ProofGenerator
     */
    protected $proofGenerator;

    /**
     * @var string
     */
    protected $logoDir;

    /**
     * @var string
     */
    protected $dpdLogo;

    public function __construct(
        UserProvider $userProvider,
        ParameterBagInterface $parameterBag,
        TreatmentGenerator $treatmentGenerator,
        ContractorGenerator $contractorGenerator,
        MaturityGenerator $maturityGenerator,
        MesurementGenerator $mesurementGenerator,
        RequestGenerator $requestGenerator,
        ViolationGenerator $violationGenerator,
        ConformiteTraitementGenerator $conformiteTraitementGenerator,
        ConformiteOrganisationGenerator $conformiteOrganisationGenerator,
        UserGenerator $userGenerator,
        ProofGenerator $proofGenerator,
        ToolGenerator $toolGenerator,
        string $logoDir,
        string $dpdLogo,
    ) {
        parent::__construct($userProvider, $parameterBag);
        $this->treatmentGenerator              = $treatmentGenerator;
        $this->contractorGenerator             = $contractorGenerator;
        $this->maturityGenerator               = $maturityGenerator;
        $this->mesurementGenerator             = $mesurementGenerator;
        $this->requestGenerator                = $requestGenerator;
        $this->violationGenerator              = $violationGenerator;
        $this->conformiteTraitementGenerator   = $conformiteTraitementGenerator;
        $this->conformiteOrganisationGenerator = $conformiteOrganisationGenerator;
        $this->userGenerator                   = $userGenerator;
        $this->proofGenerator                  = $proofGenerator;
        $this->toolGenerator                   = $toolGenerator;
        $this->logoDir                         = $logoDir;
        $this->dpdLogo                         = $dpdLogo;
    }

    public function setCollectivity(Collectivity $collectivity)
    {
        parent::setCollectivity($collectivity);
        $this->treatmentGenerator->setCollectivity($collectivity);
        $this->contractorGenerator->setCollectivity($collectivity);
        $this->maturityGenerator->setCollectivity($collectivity);
        $this->mesurementGenerator->setCollectivity($collectivity);
        $this->requestGenerator->setCollectivity($collectivity);
        $this->violationGenerator->setCollectivity($collectivity);
        $this->conformiteTraitementGenerator->setCollectivity($collectivity);
        $this->conformiteOrganisationGenerator->setCollectivity($collectivity);
        $this->userGenerator->setCollectivity($collectivity);
        $this->proofGenerator->setCollectivity($collectivity);
    }

    public function generateObjectPart(Section $section): void
    {
        if ($this->collectivity) {
            $collectivity = $this->collectivity;
        } else {
            $collectivity = $this->userProvider->getAuthenticatedUser()->getCollectivity();
        }

        $section->addTitle('Objet', 1);

        $section->addText(
            "Ce document constitue le bilan de gestion des données à caractère personnel de la structure {$collectivity->getName()}."
        );
    }

    public function generateOrganismIntroductionPart(Section $section): void
    {
        if ($this->collectivity) {
            $collectivity = $this->collectivity;
        } else {
            $collectivity = $this->userProvider->getAuthenticatedUser()->getCollectivity();
        }

        $section->addTitle('Présentation de la structure', 1);

        $section->addTitle('Identité de la structure', 2);
        $section->addText(
            \ucfirst($collectivity->getName()) . ' (' . \ucfirst($collectivity->getType()) . ')' .
            (!empty($collectivity->getPopulation()) ? ', de ' . $collectivity->getPopulation() . ' habitants' : '') .
            (!empty($collectivity->getNbrAgents()) ? ' comprenant ' . $collectivity->getNbrAgents() . ' salariés' : '') .
            " a pour SIREN {$collectivity->getSiren()}" .
            (!empty($collectivity->getFinessGeo()) ? ' (' . $collectivity->getFinessGeo() . ')' : '') .
            " et est domicilié au {$collectivity->getAddress()->getLineOne()}" .
            (!empty($collectivity->getAddress()->getLineTwo()) ? ', ' . $collectivity->getAddress()->getLineTwo() : '') .
            ", {$collectivity->getAddress()->getZipCode()}, {$collectivity->getAddress()->getCity()}."
        );

        if (!empty($collectivity->getWebsite())) {
            $section->addText("Pour consulter le site Internet : {$collectivity->getWebsite()}.");
        }

        $section->addTitle('Engagement de la direction', 2);

        if (!empty($collectivity->getReportingBlockManagementCommitment())) {
            \PhpOffice\PhpWord\Shared\Html::addHtml($section, $collectivity->getReportingBlockManagementCommitment(), false, false);
        } else {
            $section->addText("La direction de {$collectivity->getName()} a établi, documenté, mis en œuvre une politique de gestion des données à caractère personnel.");
            $section->addText('Cette politique décrit les mesures techniques et organisationnelles.');
            $section->addText("Cette politique a pour objectif de permettre à {$collectivity->getName()} de respecter dans le temps les exigences du RGPD et de pouvoir le démontrer.");
        }

        $section->addTitle('Composition du comité Informatique et Liberté', 2);

        $section->addText("Le comité informatique et liberté est chargé de suivre l'amélioration de la mise en conformité dans le temps. Il est composé de :");

        $legalManager         = $collectivity->getLegalManager();
        $legalManagerCivility = ContactCivilityDictionary::getCivilities()[$legalManager->getCivility()];
        $section->addListItem("{$legalManagerCivility} {$legalManager->getFullName()}, {$legalManager->getJob()}");

        $referent         = $collectivity->getReferent();
        $referentCivility = $referent->getCivility() ? ContactCivilityDictionary::getCivilities()[$referent->getCivility()] : null;
        $section->addListItem("{$referentCivility} {$referent->getFullName()}, {$referent->getJob()}");

        $itManager = $collectivity->getItManager();
        if ($collectivity->isDifferentItManager()) {
            $itManagerCivility = ContactCivilityDictionary::getCivilities()[$itManager->getCivility()];
            $section->addListItem("{$itManagerCivility} {$itManager->getFullName()}, {$itManager->getJob()}");
        }

        $dpo = $collectivity->getDpo();
        if ($collectivity->isDifferentDpo()) {
            $dpoCivility = ContactCivilityDictionary::getCivilities()[$dpo->getCivility()];
            $section->addListItem("{$dpoCivility} {$dpo->getFullName()}, {$dpo->getJob()}");
        }

        foreach ($collectivity->getComiteIlContacts() as $comiteIlContact) {
            $contact  = $comiteIlContact->getContact();
            $civility = ContactCivilityDictionary::getCivilities()[$contact->getCivility()];
            $section->addListItem("{$civility} {$contact->getFullName()}, {$contact->getJob()}");
        }
    }

    public function generateRegistries(
        Section $section,
        array $treatments = [],
        array $contractors = [],
        array $requests = [],
        array $violations = [],
        array $tools = [],
    ): void {
        if ($this->collectivity) {
            $collectivity = $this->collectivity;
        } else {
            $collectivity = $this->userProvider->getAuthenticatedUser()->getCollectivity();
        }

        $registries = [
            ReviewData::TREATMENT_REGISTRY,
            ReviewData::CONTRACTOR_REGISTRY,
            ReviewData::TOOL_REGISTRY,
            ReviewData::REQUEST_REGISTRY,
            ReviewData::VIOLATION_REGISTRY,
        ];
        $registryCount = 0;
        foreach ($registries as $registry) {
            if ($collectivity && $collectivity->getReviewData() && in_array($registry, $collectivity->getReviewData()->getSections())) {
                // cancel registry if it is tool registry but the module is not active
                if (ReviewData::TOOL_REGISTRY === $registry && !$collectivity->isHasModuleTools()) {
                    --$registryCount;
                }
                ++$registryCount;
            }
        }
        if ($registryCount) {
            $section->addTitle('Bilan des registres', 1);
            $section->addText("{$collectivity->getName()} recense {$registryCount} registres : ");
        }
        if (in_array(ReviewData::TREATMENT_REGISTRY, $collectivity->getReviewData()->getSections())) {
            $section->addListItem('Traitements');
        }
        if (in_array(ReviewData::CONTRACTOR_REGISTRY, $collectivity->getReviewData()->getSections())) {
            $section->addListItem('Sous-traitants');
        }
        if ($collectivity->isHasModuleTools() && in_array(ReviewData::TOOL_REGISTRY, $collectivity->getReviewData()->getSections())) {
            $section->addListItem('Logiciels et supports');
        }
        if (in_array(ReviewData::REQUEST_REGISTRY, $collectivity->getReviewData()->getSections())) {
            $section->addListItem('Demandes des personnes concernées');
        }
        if (in_array(ReviewData::VIOLATION_REGISTRY, $collectivity->getReviewData()->getSections())) {
            $section->addListItem('Violations de données');
        }

        if (in_array(ReviewData::TREATMENT_REGISTRY, $collectivity->getReviewData()->getSections())) {
            $this->treatmentGenerator->addGlobalOverview($section, $treatments);
        }
        if (in_array(ReviewData::CONTRACTOR_REGISTRY, $collectivity->getReviewData()->getSections())) {
            $this->contractorGenerator->addGlobalOverview($section, $contractors);
        }
        if (in_array(ReviewData::TOOL_REGISTRY, $collectivity->getReviewData()->getSections()) && $collectivity->isHasModuleTools()) {
            $this->toolGenerator->addSyntheticView($section, $tools, true, true, $collectivity);
        }
        if (in_array(ReviewData::REQUEST_REGISTRY, $collectivity->getReviewData()->getSections())) {
            $this->requestGenerator->addGlobalOverview($section, $requests);
        }
        if (in_array(ReviewData::VIOLATION_REGISTRY, $collectivity->getReviewData()->getSections())) {
            $this->violationGenerator->addGlobalOverview($section, $violations);
        }
    }

    public function generateManagementSystemAndCompliance(
        Section $section,
        array $maturity = [],
        array $treatments = [],
        array $mesurements = [],
        ?Evaluation $evaluation = null,
    ): void {
        $maturity['bilanReport'] = true;

        if ($this->collectivity) {
            $collectivity = $this->collectivity;
        } else {
            $collectivity = $this->userProvider->getAuthenticatedUser()->getCollectivity();
        }

        if (
            ($collectivity->getReviewData() && in_array(ReviewData::CONFORMITY_EVALUATION, $collectivity->getReviewData()->getSections()))
            || ($collectivity->isHasModuleConformiteTraitement() && $collectivity->getReviewData() && in_array(ReviewData::TREATMENT_CONFORMITY, $collectivity->getReviewData()->getSections()))
            || ($collectivity->isHasModuleConformiteOrganisation() && $collectivity->getReviewData() && in_array(ReviewData::COLLECTIVITY_CONFORMITY, $collectivity->getReviewData()->getSections()))
            || ($collectivity->getReviewData() && in_array(ReviewData::PROTECT_ACTIONS, $collectivity->getReviewData()->getSections()))
            || ($collectivity->isHasModuleConformiteTraitement() && $collectivity->getReviewData() && in_array(ReviewData::AIPD, $collectivity->getReviewData()->getSections()))
        ) {
            $section->addTitle('Système de management des données à caractère personnel et conformité', 1);
        }

        if ($collectivity->getReviewData() && in_array(ReviewData::CONFORMITY_EVALUATION, $collectivity->getReviewData()->getSections())) {
            $this->maturityGenerator->addContextView($section, $maturity);
            $this->maturityGenerator->addSyntheticView($section, $maturity);
        }

        if ($collectivity->isHasModuleConformiteTraitement() && $collectivity->getReviewData() && (in_array(ReviewData::AIPD, $collectivity->getReviewData()->getSections()) || in_array(ReviewData::TREATMENT_CONFORMITY, $collectivity->getReviewData()->getSections()))) {
            $withAIPD    = in_array(ReviewData::AIPD, $collectivity->getReviewData()->getSections());
            $withConform = in_array(ReviewData::TREATMENT_CONFORMITY, $collectivity->getReviewData()->getSections());
            $this->conformiteTraitementGenerator->addGlobalOverview($section, $treatments, $withAIPD, $withConform);
        }
        if ($collectivity->isHasModuleConformiteOrganisation() && $collectivity->getReviewData() && in_array(ReviewData::COLLECTIVITY_CONFORMITY, $collectivity->getReviewData()->getSections())) {
            $this->conformiteOrganisationGenerator->addGlobalOverview($section, $evaluation);
        }
        if ($collectivity->getReviewData() && in_array(ReviewData::PROTECT_ACTIONS, $collectivity->getReviewData()->getSections())) {
            $this->mesurementGenerator->addGlobalOverview($section, $mesurements);
        }
    }

    public function generateContinuousImprovements(Section $section): void
    {
        if ($this->collectivity) {
            $collectivity = $this->collectivity;
        } else {
            $collectivity = $this->userProvider->getAuthenticatedUser()->getCollectivity();
        }
        if ($collectivity->getReviewData() && in_array(ReviewData::CONTINUOUS_AMELIORATION, $collectivity->getReviewData()->getSections())) {
            $section->addTitle("Principe d'amélioration continue", 1);
            $section->addText('Le système de management des données à caractère personnel s’inscrit dans un principe d’amélioration continue. En conséquence :');
            if (!empty($collectivity->getReportingBlockManagementCommitment())) {
                \PhpOffice\PhpWord\Shared\Html::addHtml($section, $collectivity->getReportingBlockContinuousImprovement(), false, false);
            } else {
                $section->addListItem('Le référent opérationnel continue de mettre à jour le registre avec les éventuels nouveaux traitements effectués.');
                $section->addListItem('Le référent opérationnel continue de mettre à jour le registre avec les éventuels nouveaux sous-traitants.');
                $section->addListItem('Le comité génère un bilan chaque année et met en place les mesures correctives adéquates.');
            }
            $section->addText('Le responsable du traitement atteste avoir pris connaissance de l’ensemble des documents, approuve le bilan et s’engage à mettre en œuvre le plan d’action.');
            $section->addText('Signature du responsable du traitement');
            $section->addTextBreak(3);
            $section->addPageBreak();
        }
    }

    public function generateAnnexeMention($document, Section $section, array $treatments = [], array $violations = [], array $mesurements = []): void
    {
        if (
            $this->collectivity && $this->collectivity->getReviewData() && (
                in_array(ReviewData::TREATMENT_REGISTRY, $this->collectivity->getReviewData()->getSections())
                || in_array(ReviewData::VIOLATION_REGISTRY, $this->collectivity->getReviewData()->getSections())
                || (in_array(ReviewData::TREATMENT_CONFORMITY, $this->collectivity->getReviewData()->getSections()) && $this->collectivity->isHasModuleConformiteTraitement())
                || in_array(ReviewData::PROOF_LIST, $this->collectivity->getReviewData()->getSections())
                || in_array(ReviewData::PROTECT_ACTIONS, $this->collectivity->getReviewData()->getSections())
                || (in_array(ReviewData::AIPD, $this->collectivity->getReviewData()->getSections()) && $this->collectivity->isHasModuleConformiteTraitement())
                || in_array(ReviewData::USER_LIST, $this->collectivity->getReviewData()->getSections())
            )
        ) {
            $section->addTitle('Annexes');
        }
        if ($this->collectivity && $this->collectivity->getReviewData() && in_array(ReviewData::TREATMENT_REGISTRY, $this->collectivity->getReviewData()->getSections())) {
            $AnnexeTreatmentListSection = $document->addSection(['orientation' => 'portrait']);
            $this->treatmentGenerator->TreatmentAnnexeList($AnnexeTreatmentListSection, $treatments);
        }

        if ($this->collectivity && $this->collectivity->getReviewData() && in_array(ReviewData::VIOLATION_REGISTRY, $this->collectivity->getReviewData()->getSections())) {
            $RiskAnnexeSection = $document->addSection(['orientation' => 'landscape']);
            $this->violationGenerator->AnnexeList($RiskAnnexeSection, $violations);
        }
        if ($this->collectivity && $this->collectivity->getReviewData() && in_array(ReviewData::PROOF_LIST, $this->collectivity->getReviewData()->getSections())) {
            $ProofAnnexeSection = $document->addSection(['orientation' => 'portrait']);
            $this->proofGenerator->ProofList($ProofAnnexeSection);
        }
        if ($this->collectivity && $this->collectivity->getReviewData() && in_array(ReviewData::PROTECT_ACTIONS, $this->collectivity->getReviewData()->getSections())) {
            $protectionActionSection = $document->addSection(['orientation' => 'portrait']);
            $this->mesurementGenerator->ProtectionActionAppliedAnnexeTable($protectionActionSection, $mesurements);
        }
        if ($this->collectivity && $this->collectivity->isHasModuleConformiteTraitement() && $this->collectivity->getReviewData() && in_array(ReviewData::TREATMENT_CONFORMITY, $this->collectivity->getReviewData()->getSections())) {
            $RiskAnnexeSection = $document->addSection(['orientation' => 'landscape']);
            $this->conformiteTraitementGenerator->SyntheticAnnexeList($RiskAnnexeSection, $treatments);
        }
        if ($this->collectivity && $this->collectivity->isHasModuleConformiteTraitement() && $this->collectivity->getReviewData() && in_array(ReviewData::AIPD, $this->collectivity->getReviewData()->getSections())) {
            $riskAipdSectionBis = $document->addSection(['orientation' => 'landscape']);
            $this->treatmentGenerator->RiskTreatmentAnnexeList($riskAipdSectionBis, $treatments);
        }
        if ($this->collectivity && $this->collectivity->getReviewData() && in_array(ReviewData::USER_LIST, $this->collectivity->getReviewData()->getSections())) {
            $AnnexeUserListSection = $document->addSection(['orientation' => 'portrait']);
            $this->userGenerator->UserList($AnnexeUserListSection);
        }
    }
}
