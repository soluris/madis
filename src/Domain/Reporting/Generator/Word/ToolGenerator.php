<?php

/**
 * This file is part of the MADIS - RGPD Management application.
 *
 * @copyright Copyright (c) 2018-2019 Soluris - Solutions Numériques Territoriales Innovantes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace App\Domain\Reporting\Generator\Word;

use App\Application\Symfony\Security\UserProvider;
use App\Domain\Registry\Dictionary\ToolTypeDictionary;
use App\Domain\Registry\Model\Tool;
use PhpOffice\PhpWord\Element\Section;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class ToolGenerator extends AbstractGenerator implements ImpressionGeneratorInterface
{
    protected TranslatorInterface $translator;

    public function __construct(
        UserProvider $userProvider,
        ParameterBagInterface $parameterBag,
        TranslatorInterface $translator,
    ) {
        parent::__construct(
            $userProvider,
            $parameterBag
        );

        $this->translator = $translator;
    }

    public function addSyntheticView(Section $section, array $data, bool $forOverviewReport = false, bool $bilan = false, $collectivity = null): void
    {
        // Break page for overview report
        if ($forOverviewReport && !$bilan) {
            $section->addPageBreak();
        }

        $section->addTitle('Registre des logiciels et supports', $forOverviewReport ? 2 : 1);

        if (empty($data)) {
            $section->addText('Un registre des logiciels et des supports est tenu à jour par la structure ' . $collectivity->getName() . '. À ce jour, il n’y a pas eu de logiciel ou support renseigné.');
        } else {
            $section->addText('Un recensement des logiciels et des supports de ' . $collectivity->getName() . ' a été effectué.');
            $l = 0;
            $s = 0;
            foreach ($data as $tool) {
                /* @var Tool $tool */
                if (ToolTypeDictionary::SOFTWARE === $tool->getType()) {
                    ++$l;
                }
                if (ToolTypeDictionary::SUPPORT === $tool->getType()) {
                    ++$s;
                }
            }
            $section->addText('Il y a ' . $l . ' logiciel(s) identifié(s) et ' . $s . ' support(s) identifié(s).');

            // Table data
            // Add header
            $tableData = [
                [
                    'Nom',
                    'Type',
                    'Éditeur',
                    'Sous-traitants',
                ],
            ];
            // Add content
            foreach ($data as $tool) {
                /* @var Tool $tool */
                $tableData[] = [
                    $tool->getName(),
                    ToolTypeDictionary::getTypes()[$tool->getType()],
                    $tool->getEditor(),
                    Tool::generateLinkedDataColumn($tool->getContractors()),
                ];
            }

            $this->addTable($section, $tableData, true, self::TABLE_ORIENTATION_HORIZONTAL);

            // Don't break page if it's overview report
            if (!$forOverviewReport && !$bilan) {
                $section->addPageBreak();
            }
        }
    }

    public function addDetailedView(Section $section, array $data): void
    {
        $section->addTitle('Détail des logiciels et supports', 1);

        foreach ($data as $key => $tool) {
            /* @var Tool $tool */
            if (0 !== $key) {
                $section->addPageBreak();
            }
            $section->addTitle($tool->getName(), 2);

            $generalInformationsData = [
                [
                    'Nom',
                    $tool->getName(),
                ],
            ];

            // Ajouter les services si le module est actif
            if ($tool->getCollectivity()->getIsServicesEnabled()) {
                $generalInformationsData[] = [
                    'Service',
                    $tool->getService(),
                ];
            }

            $generalInformationsData = array_merge($generalInformationsData, [
                [
                    'Type',
                    ToolTypeDictionary::getTypes()[$tool->getType()],
                ],
                [
                    'Description',
                    $tool->getDescription(),
                ],
                [
                    'Éditeur',
                    $tool->getEditor(),
                ],
                [
                    'Sous-traitants',
                    Tool::generateLinkedDataColumn($tool->getContractors()),
                ],
                [
                    'Date de mise en production',
                    $tool->getProdDate() ? $tool->getProdDate()->format('d/m/Y') : '',
                ],
                [
                    'Pays d\'hébergement ou de stockage',
                    $this->translator->trans($tool->getCountryType()),
                ],
            ]);

            if (Tool::COUNTRY_FRANCE !== $tool->getCountryType()) {
                $generalInformationsData[] = [
                    'Nom du pays',
                    $tool->getCountryName(),
                ];
            }

            if (Tool::COUNTRY_OTHER === $tool->getCountryType()) {
                $generalInformationsData[] = [
                    'Garanties pour le transfert',
                    $tool->getCountryGuarantees(),
                ];
            }

            $generalInformationsData[] = [
                'Personne en charge',
                $tool->getManager(),
            ];

            $generalInformationsData[] = [
                'Autres informations',
                $tool->getOtherInfo(),
            ];

            $securityData = [
                [
                    'Archivage',
                    $tool->getArchival()->isCheck() ? 'Oui' : 'Non',
                    $tool->getArchival()->getComment(),
                ],
                [
                    'Chiffrement',
                    $tool->getEncrypted()->isCheck() ? 'Oui' : 'Non',
                    $tool->getEncrypted()->getComment(),
                ],
                [
                    'Contrôle d\'accès',
                    $tool->getAccessControl()->isCheck() ? 'Oui' : 'Non',
                    $tool->getAccessControl()->getComment(),
                ],
                [
                    'Mise à jour',
                    $tool->getUpdate()->isCheck() ? 'Oui' : 'Non',
                    $tool->getUpdate()->getComment(),
                ],
                [
                    'Sauvegarde',
                    $tool->getBackup()->isCheck() ? 'Oui' : 'Non',
                    $tool->getBackup()->getComment(),
                ],
                [
                    'Suppression',
                    $tool->getDeletion()->isCheck() ? 'Oui' : 'Non',
                    $tool->getDeletion()->getComment(),
                ],
                [
                    'Traçabilité',
                    $tool->getTracking()->isCheck() ? 'Oui' : 'Non',
                    $tool->getTracking()->getComment(),
                ],
                [
                    'Zone de commentaire libre',
                    $tool->getHasComment()->isCheck() ? 'Oui' : 'Non',
                    $tool->getHasComment()->getComment(),
                ],
                [
                    'Autres',
                    $tool->getOther()->isCheck() ? 'Oui' : 'Non',
                    $tool->getOther()->getComment(),
                ],
            ];

            $historyData = [
                [
                    'Date de création',
                    $this->getDate($tool->getCreatedAt()),
                ],
                [
                    'Dernière de modification',
                    $this->getDate($tool->getUpdatedAt()),
                ],
                [
                    'Modifié par',
                    $tool->getUpdatedBy(),
                ],
            ];

            $section->addTitle('Informations générales', 3);
            $this->addTable($section, $generalInformationsData, false, self::TABLE_ORIENTATION_VERTICAL);

            $section->addTitle('Mesures de sécurité et confidentialité', 3);
            $this->addTable($section, $securityData, false, self::TABLE_ORIENTATION_VERTICAL);

            $section->addTitle('Éléments associés', 3);
            $this->addLinkedData($section, $tool);

            $section->addTitle('Historique', 3);
            $this->addTable($section, $historyData, false, self::TABLE_ORIENTATION_VERTICAL);
        }
    }
}
