<?php

/**
 * This file is part of the MADIS - RGPD Management application.
 *
 * @copyright Copyright (c) 2018-2019 Soluris - Solutions Numériques Territoriales Innovantes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace App\Domain\Registry\Form\Type;

use App\Application\Form\Type\LinkableType;
use App\Domain\Registry\Form\Type\Embeddable\AddressType;
use App\Domain\Registry\Model\Contractor;
use App\Domain\Registry\Model\Mesurement;
use App\Domain\Registry\Model\Proof;
use App\Domain\Registry\Model\Request;
use App\Domain\Registry\Model\Tool;
use App\Domain\Registry\Model\Treatment;
use App\Domain\Registry\Model\Violation;
use App\Domain\User\Form\Type\ContactType;
use App\Domain\User\Model\Service;
use App\Domain\User\Model\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Security;

class ContractorType extends LinkableType
{
    /**
     * @var Security
     */
    private $security;

    /**
     * @var AuthorizationCheckerInterface
     */
    private $authorizationChecker;

    public function __construct(Security $security, AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->security             = $security;
        $this->authorizationChecker = $authorizationChecker;
        parent::__construct($security);
    }

    /**
     * Build type form.
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $contractor   = $options['data'];
        $collectivity = null;
        if ($contractor) {
            $collectivity = $contractor->getCollectivity();
        }
        /** @var User $user */
        $user = $this->security->getUser();

        if (!$collectivity) {
            $collectivity = $user->getCollectivity();
        }
        $builder
            ->add('name', TextType::class, [
                'label'    => 'registry.contractor.label.name',
                'required' => true,
                'attr'     => [
                    'maxlength' => 255,
                ],
                'purify_html' => true,
            ])
        ;
        if ($contractor->getCollectivity()->getIsServicesEnabled()) {
            $builder
                ->add('service', EntityType::class, [
                    'class'         => Service::class,
                    'label'         => 'registry.label.service',
                    'query_builder' => function (EntityRepository $er) use ($contractor) {
                        /** @var User $authenticatedUser */
                        $authenticatedUser = $this->security->getUser();
                        $collectivity      = $contractor->getCollectivity();

                        $qb = $er->createQueryBuilder('s')
                            ->where('s.collectivity = :collectivity')
                            ->setParameter(':collectivity', $collectivity)
                        ;
                        if (!$this->authorizationChecker->isGranted('ROLE_ADMIN') && $authenticatedUser->getServices()->getValues()) {
                            $qb->leftJoin('s.users', 'users')
                                ->andWhere('users.id = :id')
                                ->setParameter('id', $authenticatedUser->getId())
                            ;
                        }
                        $qb
                            ->orderBy('s.name', 'ASC');

                        return $qb;
                    },
                    'required' => false,
                ])
            ;
        }
        /** @var User $user */
        $user = $this->security->getUser();
        $builder
            ->add('referent', TextType::class, [
                'label'    => 'registry.contractor.label.referent',
                'required' => false,
                'attr'     => [
                    'maxlength' => 255,
                ],
                'purify_html' => true,
            ])
            ->add('contractualClausesVerified', CheckboxType::class, [
                'label'    => 'registry.contractor.label.contractual_clauses_verified',
                'required' => false,
            ])
            ->add('adoptedSecurityFeatures', CheckboxType::class, [
                'label'    => 'registry.contractor.label.adopted_security_features',
                'required' => false,
            ])
            ->add('maintainsTreatmentRegister', CheckboxType::class, [
                'label'    => 'registry.contractor.label.maintains_treatment_register',
                'required' => false,
            ])
            ->add('sendingDataOutsideEu', CheckboxType::class, [
                'label'    => 'registry.contractor.label.sending_data_outside_eu',
                'required' => false,
            ])
            ->add('otherInformations', TextareaType::class, [
                'label'    => 'registry.contractor.label.other_informations',
                'required' => false,
                'attr'     => [
                    'rows' => 4,
                ],
                'purify_html' => true,
            ])
            ->add('address', AddressType::class, [
                'label'             => 'registry.contractor.form.address',
                'required'          => false,
                'validation_groups' => ['default', 'contractor'],
            ])
            ->add('legalManager', ContactType::class, [
                'label'    => 'registry.contractor.form.legal_manager',
                'required' => false,
            ])
            ->add('hasDpo', CheckboxType::class, [
                'label'    => 'registry.contractor.label.has_dpo',
                'required' => false,
            ])
            ->add('dpo', ContactType::class, [
                'label'    => 'registry.contractor.form.dpo',
                'required' => false,
            ])
            ->add('mesurements',
                EntityType::class,
                $this->getLinkedFormField(
                    'global.label.linked_mesurement',
                    Mesurement::class,
                    $contractor,
                    'Actions de protection',
                    ['status' => 'asc', 'name' => 'asc'],
                ),
            )
            ->add('treatments', EntityType::class, $this->getLinkedFormField(
                'global.label.linked_treatment',
                Treatment::class,
                $contractor,
                'Traitements',
                ['active' => 'desc', 'name' => 'asc'],
            ),
            )
            ->add('violations', EntityType::class, $this->getLinkedFormField(
                'global.label.linked_violation',
                Violation::class,
                $contractor,
                'Violations',
                ['deletedAt' => 'asc', 'date' => 'asc'],
            ),
            )
            ->add('proofs', EntityType::class, $this->getLinkedFormField(
                'global.label.linked_proof',
                Proof::class,
                $contractor,
                'Preuves',
                ['deletedAt' => 'asc', 'name' => 'asc'],
            ),
            )
            ->add('requests', EntityType::class, $this->getLinkedFormField(
                'global.label.linked_request',
                Request::class,
                $contractor,
                'Demandes',
                ['deletedAt' => 'asc', 'date' => 'asc'],
            ),
            )
            ->add('contractors', EntityType::class, $this->getLinkedFormField(
                'global.label.linked_contractor',
                Contractor::class,
                $contractor,
                'Sous-traitants',
                ['name' => 'asc'],
            ),
            )
            ->add('updatedBy', HiddenType::class, [
                'required' => false,
                'data'     => $user ? $user->getFirstName() . ' ' . strtoupper($user->getLastName()) : '',
            ])
        ;

        if ($collectivity && $collectivity->isHasModuleTools()) {
            $builder->add('tools', EntityType::class, $this->getLinkedFormField(
                'global.label.linked_tool',
                Tool::class,
                $contractor,
                'Logiciels et supports',
                'name',
            ),
            );
        }
    }

    /**
     * Provide type options.
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class'        => Contractor::class,
                'validation_groups' => [
                    'default',
                    'contractor',
                ],
            ]);
    }
}
