<?php

/**
 * This file is part of the MADIS - RGPD Management application.
 *
 * @copyright Copyright (c) 2018-2019 Soluris - Solutions Numériques Territoriales Innovantes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace App\Domain\Registry\Form\Type;

use App\Application\Form\Type\LinkableType;
use App\Domain\Registry\Form\Type\Embeddable\ComplexChoiceAreaType;
use App\Domain\Registry\Model\Contractor;
use App\Domain\Registry\Model\Mesurement;
use App\Domain\Registry\Model\Proof;
use App\Domain\Registry\Model\Request;
use App\Domain\Registry\Model\Tool;
use App\Domain\Registry\Model\Treatment;
use App\Domain\Registry\Model\Violation;
use App\Domain\User\Model\Service;
use App\Domain\User\Model\User;
use Doctrine\ORM\EntityRepository;
use Knp\DictionaryBundle\Form\Type\DictionaryType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class ToolType extends LinkableType
{
    /**
     * @var Security
     */
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
        parent::__construct($security);
    }

    /**
     * Build type form.
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var User $user */
        $user = $this->security->getUser();

        /** @var Tool $tool */
        $tool         = $options['data'] ?? null;
        $collectivity = null;
        if ($tool) {
            $collectivity = $tool->getCollectivity();
        }
        /** @var User $user */
        $user = $this->security->getUser();

        if (!$collectivity) {
            $collectivity = $user->getCollectivity();
        }
        $builder
            ->add('name', TextType::class, [
                'label'    => 'registry.tool.label.name',
                'required' => true,
                'attr'     => [
                    'maxlength' => 255,
                ],
                'purify_html' => true,
                'constraints' => [
                    new NotBlank(),
                    new Length(['min' => 3]),
                ],
            ])

            ->add('type', DictionaryType::class, [
                'label'    => 'registry.tool.label.type',
                'name'     => 'registry_tool_type',
                'required' => true,
                'multiple' => false,
                'expanded' => true,
            ])
            ->add('description', TextareaType::class, [
                'label'    => 'registry.tool.label.description',
                'required' => false,
                'attr'     => [
                    'rows' => 2,
                ],
                'purify_html' => true,
            ])
            ->add('other_info', TextareaType::class, [
                'label'    => 'registry.tool.label.other_info',
                'required' => false,
                'attr'     => [
                    'rows' => 2,
                ],
                'purify_html' => true,
            ])
            ->add('editor', TextType::class, [
                'label'    => 'registry.tool.label.editor',
                'required' => false,
                'attr'     => [
                    'maxlength' => 255,
                ],
                'purify_html' => true,
            ])
            ->add('manager', TextType::class, [
                'label'    => 'registry.tool.label.manager',
                'required' => false,
                'attr'     => [
                    'maxlength' => 255,
                ],
                'purify_html' => true,
            ])

            ->add('mesurements',
                EntityType::class,
                $this->getLinkedFormField(
                    'global.label.linked_mesurement',
                    Mesurement::class,
                    $tool,
                    'Actions de protection',
                    ['status' => 'asc', 'name' => 'asc'],
                ),
            )
            ->add('treatments', EntityType::class, $this->getLinkedFormField(
                'global.label.linked_treatment',
                Treatment::class,
                $tool,
                'Traitements',
                ['active' => 'desc', 'name' => 'asc'],
            ),
            )
            ->add('violations', EntityType::class, $this->getLinkedFormField(
                'global.label.linked_violation',
                Violation::class,
                $tool,
                'Violations',
                ['deletedAt' => 'asc', 'date' => 'asc'],
            ),
            )
            ->add('proofs', EntityType::class, $this->getLinkedFormField(
                'global.label.linked_proof',
                Proof::class,
                $tool,
                'Preuves',
                ['deletedAt' => 'asc', 'name' => 'asc'],
            ),
            )
            ->add('requests', EntityType::class, $this->getLinkedFormField(
                'global.label.linked_request',
                Request::class,
                $tool,
                'Demandes',
                ['deletedAt' => 'asc', 'date' => 'asc'],
            ),
            )
            ->add('contractors', EntityType::class, $this->getLinkedFormField(
                'global.label.linked_contractor',
                Contractor::class,
                $tool,
                'Sous-traitants',
                ['name' => 'asc'],
            ),
            )
            ->add('tools', EntityType::class, $this->getLinkedFormField(
                'global.label.linked_tool',
                Tool::class,
                $tool,
                'Logiciels et supports',
                ['name' => 'asc'],
            ),
            )

            ->add('prod_date', DateType::class, [
                'label'    => 'registry.tool.label.prod_date',
                'required' => false,
                'widget'   => 'single_text',
                'format'   => 'dd/MM/yyyy',
                'html5'    => false,
                'attr'     => [
                    'class' => 'datepicker',
                ],
            ])

            ->add('country_type', ChoiceType::class, [
                'label'    => 'registry.tool.label.country_type',
                'choices'  => Tool::COUNTRY_TYPES,
                'required' => false,
            ])

            ->add('country_name', TextType::class, [
                'label'       => 'registry.tool.label.country_name',
                'required'    => false,
                'purify_html' => true,
            ])

            ->add('country_guarantees', TextType::class, [
                'label'       => 'registry.tool.label.country_guarantees',
                'required'    => true,
                'purify_html' => true,
            ])

            ->add('archival', ComplexChoiceAreaType::class, [
                'label'    => 'registry.tool.label.archival',
                'required' => false,
            ])
            ->add('encrypted', ComplexChoiceAreaType::class, [
                'label'    => 'registry.tool.label.encrypted',
                'required' => false,
            ])
            ->add('access_control', ComplexChoiceAreaType::class, [
                'label'    => 'registry.tool.label.access_control',
                'required' => false,
            ])
            ->add('update', ComplexChoiceAreaType::class, [
                'label'    => 'registry.tool.label.update',
                'required' => false,
            ])
            ->add('backup', ComplexChoiceAreaType::class, [
                'label'    => 'registry.tool.label.backup',
                'required' => false,
            ])
            ->add('deletion', ComplexChoiceAreaType::class, [
                'label'    => 'registry.tool.label.deletion',
                'required' => false,
            ])
            ->add('tracking', ComplexChoiceAreaType::class, [
                'label'    => 'registry.tool.label.tracking',
                'required' => false,
            ])
            ->add('has_comment', ComplexChoiceAreaType::class, [
                'label'    => 'registry.tool.label.has_comment',
                'required' => false,
            ])
            ->add('other', ComplexChoiceAreaType::class, [
                'label'    => 'registry.tool.label.other',
                'required' => false,
            ])
            ->add('updatedBy', HiddenType::class, [
                'required' => false,
                'data'     => $user ? $user->getFirstName() . ' ' . strtoupper($user->getLastName()) : '',
            ])
        ;
        // Check if services are enabled for the collectivity's treatment
        if ($options['data']->getCollectivity()->getIsServicesEnabled()) {
            $builder->add('service', EntityType::class, [
                'class'         => Service::class,
                'label'         => 'registry.label.service',
                'query_builder' => function (EntityRepository $er) use ($tool) {
                    if ($tool->getCollectivity()) {
                        /** @var User $authenticatedUser */
                        $authenticatedUser = $this->security->getUser();
                        $collectivity      = $tool->getCollectivity();

                        $qb = $er->createQueryBuilder('s')
                            ->where('s.collectivity = :collectivity')
                            ->setParameter(':collectivity', $collectivity)
                        ;

                        if (!$this->security->isGranted('ROLE_ADMIN') && $authenticatedUser->getServices()->getValues()) {
                            $qb->leftJoin('s.users', 'users')
                                ->andWhere('users.id = :id')
                                ->setParameter('id', $authenticatedUser->getId())
                            ;
                        }

                        $qb
                            ->orderBy('s.name', 'ASC');

                        return $qb;
                    }

                    return $er->createQueryBuilder('s')
                        ->orderBy('s.name', 'ASC');
                },
                'required' => false,
            ]);
        }
    }

    /**
     * Provide type options.
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class'        => Tool::class,
                'validation_groups' => [
                    'default',
                    'tool',
                ],
            ]);
    }
}
