<?php

/**
 * This file is part of the MADIS - RGPD Management application.
 *
 * @copyright Copyright (c) 2018-2019 Soluris - Solutions Numériques Territoriales Innovantes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace App\Domain\Registry\Form\Type;

use App\Application\Form\Type\LinkableType;
use App\Domain\Registry\Model\Contractor;
use App\Domain\Registry\Model\Mesurement;
use App\Domain\Registry\Model\Proof;
use App\Domain\Registry\Model\Request;
use App\Domain\Registry\Model\Tool;
use App\Domain\Registry\Model\Treatment;
use App\Domain\Registry\Model\Violation;
use App\Domain\User\Model\Service;
use App\Domain\User\Model\User;
use Doctrine\ORM\EntityRepository;
use Knp\DictionaryBundle\Form\Type\DictionaryType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Security;

class ViolationType extends LinkableType
{
    /**
     * @var Security
     */
    private $security;

    /**
     * @var AuthorizationCheckerInterface
     */
    private $authorizationChecker;

    public function __construct(Security $security, AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->security             = $security;
        $this->authorizationChecker = $authorizationChecker;
        parent::__construct($security);
    }

    /**
     * Build type form.
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $violation = $options['data'] ?? null;

        $collectivity = null;
        if ($violation) {
            $collectivity = $violation->getCollectivity();
        }
        /** @var User $user */
        $user = $this->security->getUser();

        if (!$collectivity) {
            $collectivity = $user->getCollectivity();
        }
        $builder
            ->add('date', DateType::class, [
                'label'    => 'registry.violation.label.date',
                'required' => true,
                'widget'   => 'single_text',
                'format'   => 'dd/MM/yyyy',
                'html5'    => false,
                'attr'     => [
                    'class' => 'datepicker',
                ],
            ])
        ;
        if ($collectivity->getIsServicesEnabled()) {
            $builder->add('service', EntityType::class, [
                'class'         => Service::class,
                'label'         => 'registry.label.service',
                'query_builder' => function (EntityRepository $er) use ($violation) {
                    /** @var User $authenticatedUser */
                    $authenticatedUser = $this->security->getUser();
                    $collectivity      = $violation->getCollectivity();

                    $qb = $er->createQueryBuilder('s')
                        ->where('s.collectivity = :collectivity')
                        ->setParameter(':collectivity', $collectivity)
                    ;
                    if (!$this->authorizationChecker->isGranted('ROLE_ADMIN') && $authenticatedUser->getServices()->getValues()) {
                        $qb->leftJoin('s.users', 'users')
                            ->andWhere('users.id = :id')
                            ->setParameter('id', $authenticatedUser->getId())
                        ;
                    }
                    $qb
                        ->orderBy('s.name', 'ASC');

                    return $qb;
                },
                'required' => false,
            ]);
        }
        /** @var User $user */
        $user = $this->security->getUser();

        $builder
            ->add('inProgress', CheckboxType::class, [
                'label'    => 'registry.violation.label.in_progress',
                'required' => false,
            ])
            ->add('violationNatures', DictionaryType::class, [
                'label'    => 'registry.violation.label.violation_natures',
                'name'     => 'registry_violation_nature',
                'expanded' => false,
                'multiple' => true,
                'attr'     => [
                    'class'            => 'selectpicker',
                    'data-live-search' => 'true',
                    'title'            => 'global.placeholder.multiple_select',
                    'aria-label'       => 'Natures de la violation',
                ],
            ])
            ->add('origins', DictionaryType::class, [
                'label'    => 'registry.violation.label.origins',
                'name'     => 'registry_violation_origin',
                'expanded' => false,
                'multiple' => true,
                'attr'     => [
                    'class'            => 'selectpicker',
                    'data-live-search' => 'true',
                    'title'            => 'global.placeholder.multiple_select',
                    'aria-label'       => 'Origine de la perte de données',
                ],
            ])
            ->add('cause', DictionaryType::class, [
                'label'    => 'registry.violation.label.cause',
                'name'     => 'registry_violation_cause',
                'expanded' => true,
                'multiple' => false,
            ])
            ->add('concernedDataNature', DictionaryType::class, [
                'label'    => 'registry.violation.label.concerned_data_nature',
                'name'     => 'registry_violation_concerned_data',
                'expanded' => false,
                'multiple' => true,
                'attr'     => [
                    'class'            => 'selectpicker',
                    'data-live-search' => 'true',
                    'title'            => 'global.placeholder.multiple_select',
                    'aria-label'       => 'Nature des données concernées',
                ],
            ])
            ->add('concernedPeopleCategories', DictionaryType::class, [
                'label'    => 'registry.violation.label.concerned_people_categories',
                'name'     => 'registry_violation_concerned_people',
                'expanded' => false,
                'multiple' => true,
                'attr'     => [
                    'class'            => 'selectpicker',
                    'data-live-search' => 'true',
                    'title'            => 'global.placeholder.multiple_select',
                    'aria-label'       => 'Catégorie des personnes concernées',
                ],
            ])
            ->add('nbAffectedRows', IntegerType::class, [
                'label' => 'registry.violation.label.nb_affected_rows',
                'attr'  => [
                    'min' => 0,
                    'max' => 999999999,
                ],
            ])
            ->add('nbAffectedPersons', IntegerType::class, [
                'label' => 'registry.violation.label.nb_affected_persons',
                'attr'  => [
                    'min' => 0,
                    'max' => 999999999,
                ],
            ])
            ->add('potentialImpactsNature', DictionaryType::class, [
                'label'    => 'registry.violation.label.potential_impacts_nature',
                'name'     => 'registry_violation_impact',
                'expanded' => false,
                'multiple' => true,
                'attr'     => [
                    'class'            => 'selectpicker',
                    'data-live-search' => 'true',
                    'title'            => 'global.placeholder.multiple_select',
                    'aria-label'       => 'Nature des impacts potentiels pour les personnes',
                ],
            ])
            ->add('gravity', DictionaryType::class, [
                'label'    => 'registry.violation.label.gravity',
                'name'     => 'registry_violation_gravity',
                'expanded' => true,
                'multiple' => false,
            ])
            ->add('communication', DictionaryType::class, [
                'label'    => 'registry.violation.label.communication',
                'name'     => 'registry_violation_communication',
                'expanded' => true,
                'multiple' => false,
            ])
            ->add('communicationPrecision', TextareaType::class, [
                'label'    => 'registry.violation.label.communication_precision',
                'required' => false,
                'attr'     => [
                    'rows' => 5,
                ],
                'purify_html' => true,
            ])
            ->add('appliedMeasuresAfterViolation', TextareaType::class, [
                'label' => 'registry.violation.label.applied_measures_after_violation',
                'attr'  => [
                    'rows' => 5,
                ],
                'purify_html' => true,
            ])
            ->add('notification', DictionaryType::class, [
                'label'    => 'registry.violation.label.notification',
                'name'     => 'registry_violation_notification',
                'required' => true,
                'expanded' => true,
                'multiple' => false,
            ])
            ->add('notificationDetails', TextType::class, [
                'label'    => 'registry.violation.label.notification_details',
                'required' => false,
                'attr'     => [
                    'maxlength' => 255,
                ],
                'purify_html' => true,
            ])
            ->add('comment', TextareaType::class, [
                'label'    => 'registry.violation.label.comment',
                'required' => false,
                'attr'     => [
                    'rows' => 5,
                ],
                'purify_html' => true,
            ])
            ->add('mesurements',
                EntityType::class,
                $this->getLinkedFormField(
                    'global.label.linked_mesurement',
                    Mesurement::class,
                    $violation,
                    'Actions de protection',
                    ['status' => 'asc', 'name' => 'asc'],
                ),
            )
            ->add('treatments', EntityType::class, $this->getLinkedFormField(
                'global.label.linked_treatment',
                Treatment::class,
                $violation,
                'Traitements',
                ['active' => 'desc', 'name' => 'asc'],
            ),
            )
            ->add('violations', EntityType::class, $this->getLinkedFormField(
                'global.label.linked_violation',
                Violation::class,
                $violation,
                'Violations',
                ['deletedAt' => 'asc', 'date' => 'asc'],
            ),
            )
            ->add('proofs', EntityType::class, $this->getLinkedFormField(
                'global.label.linked_proof',
                Proof::class,
                $violation,
                'Preuves',
                ['deletedAt' => 'asc', 'name' => 'asc'],
            ),
            )
            ->add('requests', EntityType::class, $this->getLinkedFormField(
                'global.label.linked_request',
                Request::class,
                $violation,
                'Demandes',
                ['deletedAt' => 'asc', 'date' => 'asc'],
            ),
            )
            ->add('contractors', EntityType::class, $this->getLinkedFormField(
                'global.label.linked_contractor',
                Contractor::class,
                $violation,
                'Sous-traitants',
                ['name' => 'asc'],
            ),
            )

            ->add('updatedBy', HiddenType::class, [
                'required' => false,
                'data'     => $user ? $user->getFirstName() . ' ' . strtoupper($user->getLastName()) : '',
            ])
        ;
        if ($collectivity->isHasModuleTools()) {
            $builder
                ->add('tools', EntityType::class, $this->getLinkedFormField(
                    'global.label.linked_tool',
                    Tool::class,
                    $violation,
                    'Logiciels et supports',
                ),
                );
        }
    }

    /**
     * Provide type options.
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class'        => Violation::class,
                'validation_groups' => [
                    'default',
                    'violation',
                ],
            ]);
    }
}
