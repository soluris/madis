<?php

/**
 * This file is part of the MADIS - RGPD Management application.
 *
 * @copyright Copyright (c) 2018-2019 Soluris - Solutions Numériques Territoriales Innovantes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace App\Domain\Registry\Form\Type;

use App\Application\Form\Type\LinkableType;
use App\Domain\Registry\Model\Contractor;
use App\Domain\Registry\Model\Mesurement;
use App\Domain\Registry\Model\Proof;
use App\Domain\Registry\Model\Request;
use App\Domain\Registry\Model\Tool;
use App\Domain\Registry\Model\Treatment;
use App\Domain\Registry\Model\Violation;
use App\Domain\User\Model\Service;
use App\Domain\User\Model\User;
use Doctrine\ORM\EntityRepository;
use Knp\DictionaryBundle\Form\Type\DictionaryType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\Constraints\File;

class ProofType extends LinkableType
{
    protected string $maxSize;

    /**
     * @var Security
     */
    private $security;

    public function __construct(Security $security, string $maxSize)
    {
        $this->security = $security;
        $this->maxSize  = $maxSize;
        parent::__construct($security);
    }

    /**
     * Build type form.
     *
     * @throws \Exception
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Proof $proof */
        $proof = $options['data'];
        if (!\is_null($proof->getCollectivity())) {
            $collectivity = $proof->getCollectivity();
        } else {
            /** @var User $authenticatedUser */
            $authenticatedUser = $this->security->getUser();
            $collectivity      = $authenticatedUser->getCollectivity();
        }

        $builder
            ->add('name', TextType::class, [
                'label'    => 'registry.proof.label.name',
                'required' => true,
                'attr'     => [
                    'maxlength' => 255,
                ],
                'purify_html' => true,
            ])
            ->add('type', DictionaryType::class, [
                'label'    => 'registry.proof.label.type',
                'name'     => 'registry_proof_type',
                'required' => true,
            ])
            ->add('documentFile', FileType::class, [
                'label'       => 'registry.proof.label.file',
                'required'    => !$proof->getDocument(),
                'constraints' => [
                    new File([
                        'maxSize'          => $this->maxSize,
                        'mimeTypesMessage' => 'registry_proof.document_file.file',
                        'mimeTypes'        => [
                            // JPG / PNG
                            'image/jpeg',
                            'image/png',
                            // PDF
                            'application/pdf',
                            // DOC
                            'application/msword',
                            // DOCX
                            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                            // Lors de la génération d'un fichier (Bilan) word son mimetype est doublé.
                            // On conserve le mimetype suivant car il y avait des bugs avec iOS (ipad et iphone) lors du téléchargement
                            'application/vnd.openxmlformats-officedocument.wordprocessingml.documentapplication/vnd.openxmlformats-officedocument.wordprocessingml.document',
                            // ODT
                            'application/vnd.oasis.opendocument.text',
                            // XLS
                            'application/vnd.ms-excel',
                            // XLSX
                            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                            // ODS
                            'application/vnd.oasis.opendocument.spreadsheet',
                            // PPT / PPTX
                            'application/vnd.ms-powerpoint',
                            'application/vnd.openxmlformats-officedocument.presentationml.presentation',
                            // TXT / LOG / CSV / MD
                            'text/plain',
                        ],
                        'groups' => ['default'],
                    ]),
                ],
            ])
            ->add('comment', TextType::class, [
                'label'    => 'registry.proof.label.comment',
                'required' => false,
                'attr'     => [
                    'maxlength' => 255,
                ],
                'purify_html' => true,
            ])
            ->add('mesurements',
                EntityType::class,
                $this->getLinkedFormField(
                    'global.label.linked_mesurement',
                    Mesurement::class,
                    $proof,
                    'Actions de protection',
                    ['status' => 'asc', 'name' => 'asc'],
                ),
            )
            ->add('treatments', EntityType::class, $this->getLinkedFormField(
                'global.label.linked_treatment',
                Treatment::class,
                $proof,
                'Traitements',
                ['active' => 'desc', 'name' => 'asc'],
            ),
            )
            ->add('violations', EntityType::class, $this->getLinkedFormField(
                'global.label.linked_violation',
                Violation::class,
                $proof,
                'Violations',
                ['deletedAt' => 'asc', 'date' => 'asc'],
            ),
            )
            ->add('proofs', EntityType::class, $this->getLinkedFormField(
                'global.label.linked_proof',
                Proof::class,
                $proof,
                'Preuves',
                ['deletedAt' => 'asc', 'name' => 'asc'],
            ),
            )
            ->add('requests', EntityType::class, $this->getLinkedFormField(
                'global.label.linked_request',
                Request::class,
                $proof,
                'Demandes',
                ['deletedAt' => 'asc', 'date' => 'asc'],
            ),
            )
            ->add('contractors', EntityType::class, $this->getLinkedFormField(
                'global.label.linked_contractor',
                Contractor::class,
                $proof,
                'Sous-traitants',
                ['name' => 'asc'],
            ),
            )
        ;

        if ($collectivity && $collectivity->isHasModuleTools()) {
            $builder->add('tools', EntityType::class, $this->getLinkedFormField(
                'global.label.linked_tool',
                Tool::class,
                $proof,
                'Logiciels et supports',
                'name',
            ),
            );
        }

        // Check if services are enabled for the collectivity's treatment
        if ($collectivity && $collectivity->getIsServicesEnabled()) {
            $builder->add('service', EntityType::class, [
                'class'         => Service::class,
                'label'         => 'registry.label.service',
                'query_builder' => function (EntityRepository $er) use ($proof) {
                    if ($proof->getCollectivity()) {
                        /** @var User $authenticatedUser */
                        $authenticatedUser = $this->security->getUser();
                        $collectivity      = $proof->getCollectivity();

                        $qb = $er->createQueryBuilder('s')
                            ->where('s.collectivity = :collectivity')
                            ->setParameter(':collectivity', $collectivity)
                        ;

                        if (!$this->security->isGranted('ROLE_ADMIN') && $authenticatedUser->getServices()->getValues()) {
                            $qb->leftJoin('s.users', 'users')
                                ->andWhere('users.id = :id')
                                ->setParameter('id', $authenticatedUser->getId())
                            ;
                        }

                        $qb
                            ->orderBy('s.name', 'ASC');

                        return $qb;
                    }

                    return $er->createQueryBuilder('s')
                        ->orderBy('s.name', 'ASC');
                },
                'required' => false,
            ]);
        }
    }

    /**
     * Provide type options.
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class'        => Proof::class,
                'validation_groups' => ['default', 'proof'],
            ]);
    }
}
