<?php

/**
 * This file is part of the MADIS - RGPD Management application.
 *
 * @copyright Copyright (c) 2018-2019 Soluris - Solutions Numériques Territoriales Innovantes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace App\Domain\Registry\Form\Type;

use App\Application\Form\Type\LinkableType;
use App\Domain\Registry\Model\Contractor;
use App\Domain\Registry\Model\Mesurement;
use App\Domain\Registry\Model\Proof;
use App\Domain\Registry\Model\Request;
use App\Domain\Registry\Model\Tool;
use App\Domain\Registry\Model\Treatment;
use App\Domain\Registry\Model\Violation;
use App\Domain\User\Model\Service;
use App\Domain\User\Model\User;
use Doctrine\ORM\EntityRepository;
use Knp\DictionaryBundle\Form\Type\DictionaryType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class MesurementType extends LinkableType
{
    /**
     * @var Security
     */
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
        parent::__construct($security);
    }

    /**
     * Build type form.
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Mesurement $mesurement */
        $mesurement   = $options['data'] ?? null;
        $collectivity = null;
        if ($mesurement) {
            $collectivity = $mesurement->getCollectivity();
        }
        /** @var User $user */
        $user = $this->security->getUser();

        if (!$collectivity) {
            $collectivity = $user->getCollectivity();
        }
        $builder
            ->add('name', TextType::class, [
                'label'    => 'registry.mesurement.label.name',
                'required' => true,
                'attr'     => [
                    'maxlength' => 255,
                ],
                'purify_html' => true,
            ])
            /*
            ->add('type', DictionaryType::class, [
                'label'    => 'registry.mesurement.form.type',
                'name'     => 'registry_mesurement_type',
                'required' => true,
                'multiple' => false,
                'expanded' => true,
            ])
            */
            ->add('description', TextareaType::class, [
                'label'    => 'registry.mesurement.label.description',
                'required' => false,
                'attr'     => [
                    'rows' => 3,
                ],
                'purify_html' => true,
            ])
            ->add('cost', TextType::class, [
                'label'    => 'registry.mesurement.label.cost',
                'required' => false,
                'attr'     => [
                    'maxlength' => 255,
                ],
                'purify_html' => true,
            ])
            ->add('charge', TextType::class, [
                'label'    => 'registry.mesurement.label.charge',
                'required' => false,
                'attr'     => [
                    'maxlength' => 255,
                ],
                'purify_html' => true,
            ])
            ->add('status', DictionaryType::class, [
                'label'    => 'registry.mesurement.label.status',
                'name'     => 'registry_mesurement_status',
                'required' => true,
                'multiple' => false,
                'expanded' => true,
            ])
            ->add('planificationDate', DateType::class, [
                'label'    => 'registry.mesurement.label.planification_date',
                'required' => false,
                'widget'   => 'single_text',
                'format'   => 'dd/MM/yyyy',
                'html5'    => false,
                'attr'     => [
                    'class' => 'datepicker',
                ],
            ])
            ->add('comment', TextareaType::class, [
                'label'    => 'registry.mesurement.label.comment',
                'required' => false,
                'attr'     => [
                    'rows' => 3,
                ],
                'purify_html' => true,
            ])
            ->add('priority', DictionaryType::class, [
                'label'    => 'registry.mesurement.label.priority',
                'name'     => 'registry_mesurement_priority',
                'required' => false,
                'multiple' => false,
            ])
            ->add('manager', TextType::class, [
                'label'    => 'registry.mesurement.label.manager',
                'required' => false,
                'attr'     => [
                    'maxlength' => 255,
                ],
                'purify_html' => true,
            ])
            ->add('mesurements',
                EntityType::class,
                $this->getLinkedFormField(
                    'global.label.linked_mesurement',
                    Mesurement::class,
                    $mesurement,
                    'Actions de protection',
                    ['status' => 'asc', 'name' => 'asc'],
                ),
            )
            ->add('treatments', EntityType::class, $this->getLinkedFormField(
                'global.label.linked_treatment',
                Treatment::class,
                $mesurement,
                'Traitements',
                ['active' => 'desc', 'name' => 'asc'],
            ),
            )
            ->add('violations', EntityType::class, $this->getLinkedFormField(
                'global.label.linked_violation',
                Violation::class,
                $mesurement,
                'Violations',
                ['deletedAt' => 'asc', 'date' => 'asc'],
            ),
            )
            ->add('proofs', EntityType::class, $this->getLinkedFormField(
                'global.label.linked_proof',
                Proof::class,
                $mesurement,
                'Preuves',
                ['deletedAt' => 'asc', 'name' => 'asc'],
            ),
            )
            ->add('requests', EntityType::class, $this->getLinkedFormField(
                'global.label.linked_request',
                Request::class,
                $mesurement,
                'Demandes',
                ['deletedAt' => 'asc', 'date' => 'asc'],
            ),
            )
            ->add('contractors', EntityType::class, $this->getLinkedFormField(
                'global.label.linked_contractor',
                Contractor::class,
                $mesurement,
                'Sous-traitants',
                ['name' => 'asc'],
            ),
            )
            ->add('updatedBy', HiddenType::class, [
                'required' => false,
                'data'     => $user ? $user->getFirstName() . ' ' . strtoupper($user->getLastName()) : '',
            ])
        ;

        if ($collectivity && $collectivity->isHasModuleTools()) {
            $builder->add('tools', EntityType::class, $this->getLinkedFormField(
                'global.label.linked_tool',
                Tool::class,
                $mesurement,
                'Logiciels et supports',
                'name',
            ),
            );
        }

        // Check if services are enabled for the collectivity's treatment
        if ($collectivity && $collectivity->getIsServicesEnabled()) {
            $builder->add('service', EntityType::class, [
                'class'         => Service::class,
                'label'         => 'registry.label.service',
                'query_builder' => function (EntityRepository $er) use ($mesurement) {
                    if ($mesurement->getCollectivity()) {
                        /** @var User $authenticatedUser */
                        $authenticatedUser = $this->security->getUser();
                        $collectivity      = $mesurement->getCollectivity();

                        $qb = $er->createQueryBuilder('s')
                            ->where('s.collectivity = :collectivity')
                            ->setParameter(':collectivity', $collectivity)
                        ;

                        if (!$this->security->isGranted('ROLE_ADMIN') && $authenticatedUser->getServices()->getValues()) {
                            $qb->leftJoin('s.users', 'users')
                                ->andWhere('users.id = :id')
                                ->setParameter('id', $authenticatedUser->getId())
                            ;
                        }

                        $qb
                            ->orderBy('s.name', 'ASC');

                        return $qb;
                    }

                    return $er->createQueryBuilder('s')
                        ->orderBy('s.name', 'ASC');
                },
                'required' => false,
            ]);
        }
    }

    /**
     * Provide type options.
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class'        => Mesurement::class,
                'validation_groups' => [
                    'default',
                    'mesurement',
                ],
            ]);
    }
}
