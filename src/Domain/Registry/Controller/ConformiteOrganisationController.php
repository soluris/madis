<?php

namespace App\Domain\Registry\Controller;

use App\Application\Controller\CRUDController;
use App\Application\Symfony\Security\UserProvider;
use App\Application\Traits\RepositoryUtils;
use App\Application\Traits\ServersideDatatablesTrait;
use App\Domain\Documentation\Model\Category;
use App\Domain\Registry\Form\Type\ConformiteOrganisation\EvaluationPiloteType;
use App\Domain\Registry\Form\Type\ConformiteOrganisation\EvaluationType;
use App\Domain\Registry\Model\ConformiteOrganisation\Conformite;
use App\Domain\Registry\Model\ConformiteOrganisation\Evaluation;
use App\Domain\Registry\Model\ConformiteOrganisation\Reponse;
use App\Domain\Registry\Repository\ConformiteOrganisation as Repository;
use App\Domain\Registry\Symfony\EventSubscriber\Event\ConformiteOrganisationEvent;
use App\Domain\Reporting\Handler\WordHandler;
use App\Domain\User\Dictionary\UserRoleDictionary;
use App\Domain\User\Model\User;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Snappy\Pdf;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\SubmitButton;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @property Repository\Evaluation $repository
 */
class ConformiteOrganisationController extends CRUDController
{
    use ServersideDatatablesTrait;
    use RepositoryUtils;

    /**
     * @var Repository\Question
     */
    private $questionRepository;

    /**
     * @var Repository\Processus
     */
    private $processusRepository;

    /**
     * @var Repository\Conformite
     */
    private $conformiteRepository;

    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * @var WordHandler
     */
    private $wordHandler;
    private ManagerRegistry $registry;
    private RouterInterface $router;

    public function __construct(
        EntityManagerInterface $entityManager,
        TranslatorInterface $translator,
        Repository\Evaluation $repository,
        Repository\Question $questionRepository,
        Repository\Processus $processusRepository,
        Repository\Conformite $conformiteRepository,
        UserProvider $userProvider,
        AuthorizationCheckerInterface $authorizationChecker,
        EventDispatcherInterface $dispatcher,
        WordHandler $wordHandler,
        Pdf $pdf,
        RouterInterface $router,
        ManagerRegistry $registry,
    ) {
        parent::__construct($entityManager, $translator, $repository, $pdf, $userProvider, $authorizationChecker);
        $this->questionRepository   = $questionRepository;
        $this->processusRepository  = $processusRepository;
        $this->conformiteRepository = $conformiteRepository;
        $this->dispatcher           = $dispatcher;
        $this->wordHandler          = $wordHandler;
        $this->router               = $router;
        $this->registry             = $registry;
        // Deny access to single collectivity users if conformite organisation module is disabled
        // Fixes https://gitlab.adullact.net/soluris/madis/-/issues/949
        $user = $userProvider->getAuthenticatedUser();
        if ($user && !$user->hasModuleConformiteOrganisation()) {
            throw new AccessDeniedHttpException('Ce module est désactivé sur votre structure');
        }
    }

    public function createAction(Request $request): Response
    {
        $organisation = $this->userProvider->getAuthenticatedUser()->getCollectivity();

        if ($organisation && !$organisation->isHasModuleConformiteOrganisation()) {
            throw new AccessDeniedHttpException('Ce module est désactivé sur la structure');
        }

        $evaluation = $this->repository->findLastByOrganisation($organisation);
        if (null !== $evaluation) {
            $evaluation = clone $evaluation;
            $this->addMissingNewQuestionsAndProcessus($evaluation);
        } else {
            $evaluation = new Evaluation();

            foreach ($this->processusRepository->findAll(['position' => 'asc']) as $processus) {
                $conformite = new Conformite();
                $conformite->setProcessus($processus);
                foreach ($this->questionRepository->findAllByProcessus($processus) as $question) {
                    $reponse = new Reponse();
                    $reponse->setConformite($conformite);
                    $reponse->setQuestion($question);
                    $conformite->addReponse($reponse);
                }
                $evaluation->addConformite($conformite);
            }
            $evaluation->setCollectivity($organisation);
        }
        $evaluation->setDate(new \DateTime());

        $form = $this->createForm($this->getFormType(), $evaluation);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            /** @var SubmitButton $button */
            $button = $form->get('save');
            if ($button->isClicked()) {
                $evaluation->setIsDraft(false);
            }
            $em->persist($evaluation);
            $em->flush();

            $this->addFlash('success', $this->getFlashbagMessage('success', 'create', $evaluation));

            return $this->redirectToRoute($this->getRouteName('list'));
        }

        return $this->render($this->getTemplatingBasePath('create'), [
            'form' => $form->createView(),
        ]);
    }

    public function showAction(string $id): Response
    {
        /** @var Conformite $object */
        $object = $this->repository->findOneById($id);

        if ($object->getCollectivity()->isHasModuleConformiteOrganisation()) {
            return parent::showAction($id);
        }

        return $this->redirectToRoute('registry_conformite_organisation_list');
    }

    public function deleteAction(string $id): Response
    {
        /** @var Conformite $object */
        $object = $this->repository->findOneById($id);

        if ($object->getCollectivity()->isHasModuleConformiteOrganisation()) {
            return parent::deleteAction($id);
        }

        return $this->redirectToRoute('registry_conformite_organisation_list');
    }

    public function deleteConfirmationAction(string $id): Response
    {
        /** @var Conformite $object */
        $object = $this->repository->findOneById($id);

        if ($object->getCollectivity()->isHasModuleConformiteOrganisation()) {
            return parent::deleteConfirmationAction($id);
        }

        return $this->redirectToRoute('registry_conformite_organisation_list');
    }

    public function listConformitesAction(Request $request): Response
    {
        $criteria     = [];
        $collectivity = $this->userProvider->getAuthenticatedUser()->getCollectivity();
        if (!$this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            $criteria['collectivity'] = $collectivity;
        }

        $category = $this->entityManager->getRepository(Category::class)->findOneBy([
            'name' => 'Conformité de la structure',
        ]);

        $form           = null;
        $evaluations    = null;
        $isAdminView    = $this->authorizationChecker->isGranted('ROLE_REFERENT');
        $lastEvaluation = $this->repository->findLastByOrganisation($collectivity);

        if (!$isAdminView && null !== $lastEvaluation) {
            $evaluations = $this->repository->findAllByActiveOrganisationWithHasModuleConformiteOrganisationAndOrderedByDate($collectivity);

            $form = $this->createForm(EvaluationPiloteType::class, $lastEvaluation);
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->flush();

                $this->addFlash('success', $this->getFlashbagMessage('success', 'pilote', $evaluations[0]));

                return $this->redirectToRoute($this->getRouteName('list'));
            }
            $form = $form->createView();
        }

        return $this->render('Registry/Conformite_organisation/list.html.twig', [
            'totalItem'   => $this->repository->count($criteria),
            'evaluations' => $evaluations,
            'category'    => $category,
            'route'       => $this->router->generate('registry_conformite_organisation_list_datatables'),
            'form'        => $form,
        ]);
    }

    public function editAction(Request $request, string $id): Response
    {
        /** @var Evaluation $evaluation */
        $evaluation = $this->repository->findOneById($id);
        if (!$evaluation) {
            throw new NotFoundHttpException("No object found with ID '{$id}'");
        }

        if ($evaluation->getCollectivity() && !$evaluation->getCollectivity()->isHasModuleConformiteOrganisation()) {
            throw new AccessDeniedHttpException('Ce module est désactivé sur la structure');
        }

        if (!$evaluation->isDraft()) {
            throw new BadRequestHttpException("Submitted evaluation can't be modified");
        }

        /** @var User $user */
        $user = $this->getUser();
        if (!$user->hasAccessTo($evaluation)) {
            return $this->redirectToRoute($this->getRouteName('list'));
        }

        $this->addMissingNewQuestionsAndProcessus($evaluation);

        $form = $this->createForm($this->getFormType(), $evaluation);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var SubmitButton $button */
            $button = $form->get('save');
            if ($button->isClicked()) {
                $evaluation->setIsDraft(false);
            }
            $this->entityManager->flush();

            $this->addFlash('success', $this->getFlashbagMessage('success', 'edit', $evaluation));

            $this->dispatcher->dispatch(new ConformiteOrganisationEvent($evaluation));

            return $this->redirectToRoute($this->getRouteName('list'));
        }

        return $this->render($this->getTemplatingBasePath('edit'), [
            'form' => $form->createView(),
        ]);
    }

    public function reportAction(Request $request, string $id)
    {
        $evaluation = $this->repository->findOneById($id);
        if (!$evaluation) {
            throw new NotFoundHttpException("No object found with ID '{$id}'");
        }
        if ($evaluation->getCollectivity() && !$evaluation->getCollectivity()->isHasModuleConformiteOrganisation()) {
            throw new AccessDeniedHttpException('Ce module est désactivé sur la structure');
        }
        $withAllActions = $request->query->getBoolean('all_actions', true);

        /** @var User $user */
        $user = $this->getUser();
        if (!$user->hasAccessTo($evaluation)) {
            return $this->redirectToRoute($this->getRouteName('list'));
        }

        return $this->wordHandler->generateRegistryConformiteOrganisationReport($evaluation, $withAllActions);
    }

    private function addMissingNewQuestionsAndProcessus(Evaluation $evaluation)
    {
        foreach ($this->processusRepository->findNewNotUsedInGivenConformite($evaluation) as $processus) {
            $conformite = new Conformite();
            $conformite->setProcessus($processus);
            $evaluation->addConformite($conformite);
        }

        foreach ($evaluation->getConformites() as $conformite) {
            foreach ($this->questionRepository->findNewNotUsedByGivenConformite($conformite) as $question) {
                $reponse = new Reponse();
                $reponse->setQuestion($question);
                $conformite->addReponse($reponse);
            }
        }
    }

    protected function getDomain(): string
    {
        return 'registry';
    }

    protected function getModel(): string
    {
        return 'conformite_organisation';
    }

    protected function getModelClass(): string
    {
        return Evaluation::class;
    }

    protected function getFormType(): string
    {
        return EvaluationType::class;
    }

    public function listDataTables(Request $request): JsonResponse
    {
        $criteria = $this->getRequestCriteria();

        $conformites = $this->getResults($request, $criteria);
        $reponse     = $this->getBaseDataTablesResponse($request, $conformites, $criteria);

        $yes = '<span class="badge bg-dark-gray">' . $this->translator->trans('global.label.yes') . '</span>';
        $no  = '<span class="badge bg-gray">' . $this->translator->trans('global.label.no') . '</span>';

        /* @var Evaluation $conformite */
        foreach ($conformites as $evaluation) {
            if (is_array($evaluation)) {
                $evaluation = $evaluation[0];
            }
            $reponse['data'][] = [
                'id'           => $evaluation->getId(),
                'created_at'   => $evaluation ? date_format($evaluation->getCreatedAt(), 'd/m/Y') : null,
                'collectivite' => $evaluation->getCollectivity()->getName(),
                'participant'  => $evaluation->getParticipants()->count(),
                'draft'        => $evaluation->isDraft() ? $yes : $no,
                'actions'      => $this->generateActionCell($evaluation),
            ];
        }

        $jsonResponse = new JsonResponse();
        $jsonResponse->setJson(\json_encode($reponse));

        return $jsonResponse;
    }

    protected function getLabelAndKeysArray(): array
    {
        if ($this->authorizationChecker->isGranted('ROLE_REFERENT')) {
            return [
                0 => 'created_at',
                1 => 'collectivite',
                2 => 'participant',
                3 => 'draft',
                4 => 'actions',
            ];
        }

        return [
            0 => 'created_at',
            1 => 'participant',
            2 => 'draft',
            3 => 'actions',
        ];
    }

    private function generateActionCell(Evaluation $conformity)
    {
        $html = '<a href="' .
            $this->router->generate('registry_conformite_organisation_report', ['id' => $conformity->getId()]) . '">
            <i aria-hidden="true" class="fa fa-print"></i> ' .
            $this->translator->trans('global.action.print')
            . '</a> ';

        if ($conformity->isDraft()) {
            $html .= '<a href="' .
                $this->router->generate('registry_conformite_organisation_edit', ['id' => $conformity->getId()]) . '">
            <i aria-hidden="true" class="fa fa-pencil"></i> ' .
                $this->translator->trans('global.action.edit')
                . '</a> ';
        }

        $html .= '<a href="' .
        $this->router->generate('registry_conformite_organisation_delete', ['id' => $conformity->getId()]) .
        '"><i aria-hidden="true" class="fa fa-trash"></i> ' .
        $this->translator->trans('global.action.delete')
        . '</a> '
        ;

        return $html;
    }

    private function getRequestCriteria()
    {
        $criteria = [];
        $user     = $this->userProvider->getAuthenticatedUser();

        if (!$this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            $criteria['collectivity'] = $user->getCollectivity();
        }

        if (\in_array(UserRoleDictionary::ROLE_REFERENT, $user->getRoles())) {
            $criteria['collectivity'] = $user->getCollectivitesReferees();
        }

        return $criteria;
    }

    protected function getBaseDataTablesResponse(Request $request, $results, array $criteria = [])
    {
        $draw = $request->request->get('draw');

        $reponse = [
            'draw'            => $draw,
            'recordsTotal'    => $this->repository->count(),
            'recordsFiltered' => $this->repository->count($criteria),
            'data'            => [],
        ];

        return $reponse;
    }

    protected function createQueryBuilder(): QueryBuilder
    {
        return $this->getManager()
            ->createQueryBuilder()
            ->select('o')
            ->from($this->getModelClass(), 'o')
        ;
    }

    public function count(array $criteria = [])
    {
        $qb = $this
            ->createQueryBuilder()
            ->select('count(o.id)')
        ;

        if (isset($criteria['collectivity']) && $criteria['collectivity'] instanceof Collection) {
            $qb->leftJoin('o.collectivity', 'collectivite');
            $this->addInClauseCollectivities($qb, $criteria['collectivity']->toArray());
            unset($criteria['collectivity']);
        }

        foreach ($criteria as $key => $value) {
            $this->addWhereClause($qb, $key, $value);
        }

        return $qb
            ->getQuery()
            ->getSingleScalarResult()
        ;
    }

    protected function getManager(): EntityManagerInterface
    {
        $manager = $this->registry->getManager();

        if (!$manager instanceof EntityManagerInterface) {
            throw new \Exception('Registry Manager must be an instance of EntityManagerInterface');
        }

        return $manager;
    }
}
