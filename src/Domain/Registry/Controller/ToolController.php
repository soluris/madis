<?php

/**
 * This file is part of the MADIS - RGPD Management application.
 *
 * @copyright Copyright (c) 2018-2019 Soluris - Solutions Numériques Territoriales Innovantes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace App\Domain\Registry\Controller;

use App\Application\Controller\CRUDController;
use App\Application\Symfony\Security\UserProvider;
use App\Application\Traits\ServersideDatatablesTrait;
use App\Domain\Documentation\Model\Category;
use App\Domain\Registry\Dictionary\MesurementStatusDictionary;
use App\Domain\Registry\Dictionary\ToolTypeDictionary;
use App\Domain\Registry\Form\Type\ToolType;
use App\Domain\Registry\Model;
use App\Domain\Registry\Repository;
use App\Domain\Reporting\Handler\WordHandler;
use App\Domain\User\Dictionary\UserRoleDictionary;
use App\Domain\User\Model\Collectivity;
use App\Domain\User\Model\User;
use App\Domain\User\Repository as UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Snappy\Pdf;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Polyfill\Intl\Icu\Exception\MethodNotImplementedException;

/**
 * @property Repository\Tool $repository
 */
class ToolController extends CRUDController
{
    use ServersideDatatablesTrait;

    /**
     * @var UserRepository\Collectivity
     */
    protected $collectivityRepository;

    /**
     * @var WordHandler
     */
    protected $wordHandler;

    /**
     * @var AuthorizationCheckerInterface
     */
    protected $authorizationChecker;

    /**
     * @var UserProvider
     */
    protected $userProvider;

    protected RequestStack $requestStack;

    /**
     * @var RouterInterface
     */
    protected $router;

    public function __construct(
        EntityManagerInterface $entityManager,
        TranslatorInterface $translator,
        Repository\Tool $repository,
        UserRepository\Collectivity $collectivityRepository,
        WordHandler $wordHandler,
        AuthorizationCheckerInterface $authorizationChecker,
        UserProvider $userProvider,
        RouterInterface $router,
        Pdf $pdf,
        RequestStack $requestStack,
    ) {
        parent::__construct($entityManager, $translator, $repository, $pdf, $userProvider, $authorizationChecker);
        $this->collectivityRepository = $collectivityRepository;
        $this->wordHandler            = $wordHandler;
        $this->authorizationChecker   = $authorizationChecker;
        $this->userProvider           = $userProvider;
        $this->router                 = $router;
        $this->requestStack           = $requestStack;
        // Deny access to single collectivity users if tool module is disabled
        // Fixes https://gitlab.adullact.net/soluris/madis/-/issues/949
        $user = $userProvider->getAuthenticatedUser();
        if ($user && !$user->hasModuleTools()) {
            throw new AccessDeniedHttpException('Ce module est désactivé sur votre structure');
        }
    }

    protected function getDomain(): string
    {
        return 'registry';
    }

    protected function getModel(): string
    {
        return 'tool';
    }

    protected function getModelClass(): string
    {
        return Model\Tool::class;
    }

    protected function getFormType(): string
    {
        return ToolType::class;
    }

    protected function getListData()
    {
        $request  = $this->requestStack->getCurrentRequest();
        $criteria = $this->getRequestCriteria($request);

        return $this->repository->findBy($criteria);
    }

    public function listAction(): Response
    {
        $request  = $this->requestStack->getCurrentRequest();
        $category = $this->entityManager->getRepository(Category::class)->findOneBy([
            'name' => 'Logiciels',
        ]);

        return $this->render($this->getTemplatingBasePath('list'), [
            'totalItem' => $this->repository->count($this->getRequestCriteria($request)),
            'category'  => $category,
            'route'     => $this->router->generate('registry_tool_list_datatables'),
        ]);
    }

    // Override default actions to disable access if tool module is disabled in the collectivity

    public function showAction(string $id): Response
    {
        /** @var Model\Tool $object */
        $object = $this->repository->findOneById($id);
        if ($object->getCollectivity() && $object->getCollectivity()->isHasModuleTools()) {
            return parent::showAction($id);
        }

        return $this->redirectToRoute('registry_tool_list');
    }

    public function editAction(Request $request, string $id): Response
    {
        /** @var Model\Tool $object */
        $object = $this->repository->findOneById($id);
        if ($object->getCollectivity() && $object->getCollectivity()->isHasModuleTools()) {
            return parent::editAction($request, $id);
        }

        return $this->redirectToRoute('registry_tool_list');
    }

    public function deleteAction(string $id): Response
    {
        /** @var Model\Tool $object */
        $object = $this->repository->findOneById($id);
        if ($object->getCollectivity() && $object->getCollectivity()->isHasModuleTools()) {
            return parent::deleteAction($id);
        }

        return $this->redirectToRoute('registry_tool_list');
    }

    public function createAction(Request $request): Response
    {
        $collectivity = $this->userProvider->getAuthenticatedUser()->getCollectivity();
        if ($collectivity && $collectivity->isHasModuleTools()) {
            return parent::createAction($request);
        }

        return $this->redirectToRoute('registry_tool_list');
    }

    public function pdfAction(string $id)
    {
        /** @var Model\Tool $object */
        $object = $this->repository->findOneById($id);
        if ($object->getCollectivity() && $object->getCollectivity()->isHasModuleTools()) {
            return parent::pdfAction($id);
        }

        return $this->redirectToRoute('registry_tool_list');
    }

    private function getRequestCriteria(Request $request)
    {
        $criteria = [];
        $user     = $this->userProvider->getAuthenticatedUser();

        if (!$this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            $criteria['collectivity'] = $user->getCollectivity();
        }

        if (\in_array(UserRoleDictionary::ROLE_REFERENT, $user->getRoles())) {
            $criteria['collectivity'] = $user->getCollectivitesReferees();
        }

        if ($request->query->getBoolean('action_plan')) {
            // Since we have to display planified & not-applied mesurement, filter
            $criteria['planificationDate'] = 'null';
            $criteria['status']            = MesurementStatusDictionary::STATUS_NOT_APPLIED;
        }

        return $criteria;
    }

    /**
     * Generate a word report of tools.
     *
     * @throws \PhpOffice\PhpWord\Exception\Exception
     */
    public function reportAction(): Response
    {
        if (!$this->userProvider->getAuthenticatedUser()->getCollectivity()->isHasModuleTools()) {
            return $this->redirectToRoute('registry_tool_list');
        }
        $objects = $this->repository->findAllByCollectivity(
            $this->userProvider->getAuthenticatedUser()->getCollectivity(),
            ['name' => 'asc']
        );

        return $this->wordHandler->generateRegistryToolReport($objects, $this->userProvider->getAuthenticatedUser()->getCollectivity());
    }

    protected function getLabelAndKeysArray(): array
    {
        if ($this->isGranted('ROLE_REFERENT')) {
            return [
                'name',
                'collectivity',
                'service',
                'type',
                'editor',
                'archival',
                'encrypted',
                'access_control',
                'update',
                'backup',
                'deletion',
                'tracking',
                'has_comment',
                'other',
                'treatments',
                'contractors',
                'proofs',
                'mesurements',
                'createdAt',
                'updatedAt',
                'actions',
            ];
        }
        if ($this->userProvider->getAuthenticatedUser()->hasServices()) {
            return [
                'name',
                'service',
                'type',
                'editor',
                'archival',
                'encrypted',
                'access_control',
                'update',
                'backup',
                'deletion',
                'tracking',
                'has_comment',
                'other',
                'treatments',
                'contractors',
                'proofs',
                'mesurements',
                'createdAt',
                'updatedAt',
                'actions',
            ];
        }

        return [
            'name',
            'type',
            'editor',
            'archival',
            'encrypted',
            'access_control',
            'update',
            'backup',
            'deletion',
            'tracking',
            'has_comment',
            'other',
            'treatments',
            'contractors',
            'proofs',
            'mesurements',
            'createdAt',
            'updatedAt',
            'actions',
        ];
    }

    public function listDataTables(Request $request): JsonResponse
    {
        $criteria = $this->getRequestCriteria($request);
        $tools    = $this->getResults($request, $criteria);
        $reponse  = $this->getBaseDataTablesResponse($request, $tools, $criteria);

        $yes = '<span class="badge bg-green">' . $this->translator->trans('global.label.yes') . '</span>';
        $no  = '<span class="badge bg-yellow">' . $this->translator->trans('global.label.no') . '</span>';

        /** @var Model\Tool $tool */
        foreach ($tools as $tool) {
            try {
                $col = $tool->getCollectivity() ? $tool->getCollectivity()->getName() : '';
            } catch (\Exception $e) {
                $col = '';
            }
            $reponse['data'][] = [
                'id'             => $tool->getId(),
                'name'           => $this->generateShowLink($tool),
                'collectivity'   => $this->authorizationChecker->isGranted('ROLE_REFERENT') ? $col : '',
                'service'        => $tool->getService() ? $tool->getService()->getName() : '',
                'type'           => ToolTypeDictionary::getTypes()[$tool->getType()],
                'editor'         => $tool->getEditor(),
                'archival'       => $tool->getArchival()->isCheck() ? $yes : $no,
                'encrypted'      => $tool->getEncrypted()->isCheck() ? $yes : $no,
                'access_control' => $tool->getAccessControl()->isCheck() ? $yes : $no,
                'update'         => $tool->getUpdate()->isCheck() ? $yes : $no,
                'backup'         => $tool->getBackup()->isCheck() ? $yes : $no,
                'deletion'       => $tool->getDeletion()->isCheck() ? $yes : $no,
                'tracking'       => $tool->getTracking()->isCheck() ? $yes : $no,
                'has_comment'    => $tool->getHasComment()->isCheck() ? $yes : $no,
                'other'          => $tool->getOther()->isCheck() ? $yes : $no,
                'treatments'     => Model\Tool::generateLinkedDataColumn($tool->getTreatments()),
                'contractors'    => Model\Tool::generateLinkedDataColumn($tool->getContractors()),
                'proofs'         => Model\Tool::generateLinkedDataColumn($tool->getProofs()),
                'mesurements'    => Model\Tool::generateLinkedDataColumn($tool->getMesurements()),
                'createdAt'      => $tool->getCreatedAt()->format('d-m-Y H:i'),
                'updatedAt'      => $tool->getUpdatedAt()->format('d-m-Y H:i'),
                'actions'        => $this->generateActionCell($tool),
                'updatedBy'      => $tool->getUpdatedBy(),
            ];
        }

        $jsonResponse = new JsonResponse();
        $jsonResponse->setJson(json_encode($reponse));

        return $jsonResponse;
    }

    private function generateShowLink(Model\Tool $tool)
    {
        return '<a href="' .
            $this->router->generate('registry_tool_show', ['id' => $tool->getId()]) .
            '">' . \htmlspecialchars($tool->getName()) . '</a>';
    }

    private function generateActionCell(Model\Tool $tool)
    {
        $user = $this->userProvider->getAuthenticatedUser();
        if (
            (
                $this->authorizationChecker->isGranted('ROLE_USER')
                && $tool->getCollectivity()->getIsServicesEnabled()
                && ($user->getServices()->isEmpty() || $tool->isInUserServices($user))
            )
            || $this->authorizationChecker->isGranted('ROLE_ADMIN')
            || !$tool->getCollectivity()->getIsServicesEnabled()
        ) {
            return '<a href="' .
                $this->router->generate('registry_tool_edit', ['id' => $tool->getId()]) . '">
            <i aria-hidden="true" class="fa fa-pencil"></i> ' .
                $this->translator->trans('global.action.edit')
                . '</a>

            <a href="' .
                $this->router->generate('registry_tool_delete', ['id' => $tool->getId()]) .
                '"><i aria-hidden="true" class="fa fa-trash"></i> ' .
                $this->translator->trans('global.action.delete')
                . '</a>';
        }
    }

    public function apiGetToolsByCollectivity(string $collectivityId): Response
    {
        if (!$this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedHttpException('You can\'t access to a collectivity mesurement data');
        }

        /** @var Collectivity|null $collectivity */
        $collectivity = $this->collectivityRepository->findOneById($collectivityId);
        if (null === $collectivity) {
            throw new NotFoundHttpException('Can\'t find collectivity for id ' . $collectivityId);
        }

        $tools = $this->repository->findAllByCollectivity(
            $collectivity,
            [
                'name' => 'ASC',
            ]
        );

        $responseData = [];

        /** @var Model\Tool $tool */
        foreach ($tools as $tool) {
            $responseData[] = [
                'value' => $tool->getId()->toString(),
                'text'  => $tool->__toString(),
            ];
        }

        return new JsonResponse($responseData);
    }

    /**
     * The deletion action
     * Delete the data.
     * OVERRIDE of the CRUDController to manage clone id.
     *
     * @throws \Exception
     */
    public function deleteConfirmationAction(string $id): Response
    {
        $object = $this->repository->findOneById($id);
        if (!$object) {
            throw new NotFoundHttpException("No object found with ID '{$id}'");
        }
        if ($object->getCollectivity() && !$object->getCollectivity()->isHasModuleTools()) {
            throw new AccessDeniedHttpException('Ce module est désactivé sur la structure');
        }
        /** @var User $user */
        $user = $this->getUser();
        if (!$user->hasAccessTo($object)) {
            return $this->redirectToRoute($this->getRouteName('list'));
        }

        if ($this->isSoftDelete()) {
            if (!\method_exists($object, 'setDeletedAt')) {
                throw new MethodNotImplementedException('setDeletedAt');
            }
            $object->setDeletedAt(new \DateTimeImmutable());
            $this->repository->update($object);
        } else {
            $this->entityManager->remove($object);
            $this->entityManager->flush();
        }

        $this->addFlash('success', $this->getFlashbagMessage('success', 'delete', $object));

        return $this->redirectToRoute($this->getRouteName('list'));
    }
}
