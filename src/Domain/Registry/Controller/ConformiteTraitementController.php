<?php

/**
 * This file is part of the MADIS - RGPD Management application.
 *
 * @copyright Copyright (c) 2018-2019 Soluris - Solutions Numériques Territoriales Innovantes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace App\Domain\Registry\Controller;

use App\Application\Controller\CRUDController;
use App\Application\Symfony\Security\UserProvider;
use App\Application\Traits\ServersideDatatablesTrait;
use App\Domain\AIPD\Converter\ModeleToAnalyseConverter;
use App\Domain\AIPD\Model\AnalyseImpact;
use App\Domain\AIPD\Repository as AipdRepository;
use App\Domain\Documentation\Model\Category;
use App\Domain\Registry\Calculator\Completion\ConformiteTraitementCompletion;
use App\Domain\Registry\Dictionary\ConformiteTraitementLevelDictionary;
use App\Domain\Registry\Form\Type\ConformiteTraitement\ConformiteTraitementType;
use App\Domain\Registry\Model;
use App\Domain\Registry\Model\ConformiteTraitement\ConformiteTraitement;
use App\Domain\Registry\Model\Treatment;
use App\Domain\Registry\Repository;
use App\Domain\Registry\Symfony\EventSubscriber\Event\ConformiteTraitementEvent;
use App\Domain\Reporting\Handler\WordHandler;
use App\Domain\User\Dictionary\UserRoleDictionary;
use App\Domain\User\Model\User;
use App\Domain\User\Repository as UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Knp\Snappy\Pdf;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @property Repository\ConformiteTraitement\ConformiteTraitement $repository
 */
class ConformiteTraitementController extends CRUDController
{
    use ServersideDatatablesTrait;

    /**
     * @var UserRepository\Collectivity
     */
    protected $collectivityRepository;

    /**
     * @var Repository\Treatment
     */
    protected $treatmentRepository;

    /**
     * @var WordHandler
     */
    protected $wordHandler;

    /**
     * @var AuthorizationCheckerInterface
     */
    protected $authorizationChecker;

    /**
     * @var UserProvider
     */
    protected $userProvider;

    /**
     * @var Repository\ConformiteTraitement\Question
     */
    protected $questionRepository;

    /**
     * @var EventDispatcherInterface
     */
    protected $dispatcher;

    private AipdRepository\ModeleAnalyse $modeleRepository;

    private RouterInterface $router;

    public function __construct(
        EntityManagerInterface $entityManager,
        TranslatorInterface $translator,
        Repository\ConformiteTraitement\ConformiteTraitement $repository,
        UserRepository\Collectivity $collectivityRepository,
        WordHandler $wordHandler,
        AuthorizationCheckerInterface $authorizationChecker,
        UserProvider $userProvider,
        Repository\Treatment $treatmentRepository,
        Repository\ConformiteTraitement\Question $questionRepository,
        EventDispatcherInterface $dispatcher,
        Pdf $pdf,
        AipdRepository\ModeleAnalyse $modeleRepository,
        RouterInterface $router,
    ) {
        parent::__construct($entityManager, $translator, $repository, $pdf, $userProvider, $authorizationChecker);
        $this->collectivityRepository = $collectivityRepository;
        $this->wordHandler            = $wordHandler;
        $this->authorizationChecker   = $authorizationChecker;
        $this->userProvider           = $userProvider;
        $this->treatmentRepository    = $treatmentRepository;
        $this->questionRepository     = $questionRepository;
        $this->dispatcher             = $dispatcher;
        $this->modeleRepository       = $modeleRepository;
        $this->router                 = $router;
        $this->repository             = $repository;

        // Deny access to single collectivity users if conformite traitement module is disabled
        // Fixes https://gitlab.adullact.net/soluris/madis/-/issues/949
        $user = $userProvider->getAuthenticatedUser();
        if ($user && !$user->hasModuleConformiteTraitement()) {
            throw new AccessDeniedHttpException('Ce module est désactivé sur votre structure');
        }
    }

    protected function getDomain(): string
    {
        return 'registry';
    }

    protected function getModel(): string
    {
        return 'conformite_traitement';
    }

    protected function getModelClass(): string
    {
        return ConformiteTraitement::class;
    }

    public function reportAction()
    {
        $collectivity = $this->userProvider->getAuthenticatedUser()->getCollectivity();

        if (!$collectivity->isHasModuleConformiteTraitement()) {
            return $this->redirectToRoute('registry_conformite_organisation_list');
        }
        $objects = $this->treatmentRepository->findAllByCollectivity(
            $collectivity
        );

        return $this->wordHandler->generateRegistryConformiteTraitementReport($objects);
    }

    protected function getFormType(): string
    {
        return ConformiteTraitementType::class;
    }

    protected function getListData()
    {
        $collectivity = null;
        $user         = $this->userProvider->getAuthenticatedUser();

        if (!$this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            $collectivity = $user->getCollectivity();
        }

        if (\in_array(UserRoleDictionary::ROLE_REFERENT, $user->getRoles())) {
            $collectivity = \iterable_to_array($user->getCollectivitesReferees());
        }

        return $this->treatmentRepository->findAllActiveByCollectivityWithHasModuleConformiteTraitement($collectivity);
    }

    public function listAction(): Response
    {
        $user          = $this->userProvider->getAuthenticatedUser();
        $services_user = $user->getServices();
        $criteria      = [];

        $category = $this->entityManager->getRepository(Category::class)->findOneBy([
            'name' => 'Conformité des traitements',
        ]);

        return $this->render('Registry/Conformite_traitement/list.html.twig', [
            'objects'       => $this->getListData(),
            'totalItem'     => $this->repository->count($criteria),
            'category'      => $category,
            'services_user' => $services_user,
            'route'         => $this->router->generate('registry_conformite_traitement_list_datatables'),
        ]);
    }

    /**
     * {@inheritdoc}
     * Override method in order to hydrate questions.
     */
    public function createAction(Request $request): Response
    {
        /**
         * @var ConformiteTraitement
         */
        $object = new ConformiteTraitement();

        /** @var User $user */
        $user = $this->getUser();

        /** @var Treatment $traitement */
        $traitement = $this->treatmentRepository->findOneById($request->get('idTraitement'));
        if ($traitement->getCollectivity() && !$traitement->getCollectivity()->isHasModuleConformiteTraitement()) {
            throw new AccessDeniedHttpException('La structure de ce traitement a le module conformité des traitements désactivé');
        }
        $object->setTraitement($traitement);

        if (!$user->hasAccessTo($traitement)) {
            return $this->redirectToRoute($this->getRouteName('list'));
        }

        $service       = $object->getTraitement()->getService();
        $user          = $this->userProvider->getAuthenticatedUser();
        $services_user = $user->getServices();

        if (!($this->authorizationChecker->isGranted('ROLE_USER') && ($services_user->isEmpty() || $services_user->contains($service)))) {
            return $this->redirectToRoute('registry_treatment_list');
        }

        // Before create form, hydrate answers array with potential question responses
        foreach ($this->questionRepository->findAll(['position' => 'ASC']) as $question) {
            $reponse = new Model\ConformiteTraitement\Reponse();
            $reponse->setQuestion($question);
            $object->addReponse($reponse);
        }

        $form = $this->createForm($this->getFormType(), $object);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($object);
            $em->flush();

            $this->addFlash('success', $this->getFlashbagMessage('success', 'create', $object));

            return $this->redirectToRoute($this->getRouteName('list'));
        }

        $serviceEnabled = $object->getTraitement()->getCollectivity()->getIsServicesEnabled();

        return $this->render($this->getTemplatingBasePath('create'), [
            'form'           => $form->createView(),
            'serviceEnabled' => $serviceEnabled,
        ]);
    }

    /**
     * {@inheritdoc}
     * Override method in order to hydrate new questions.
     *
     * @param string $id The ID of the data to edit
     */
    public function editAction(Request $request, string $id): Response
    {
        /** @var ConformiteTraitement $object */
        $object = $this->repository->findOneById($id);
        if (!$object) {
            throw new NotFoundHttpException("No object found with ID '{$id}'");
        }

        if (!$object->getTraitement() || !$object->getTraitement()->getCollectivity() || !$object->getTraitement()->getCollectivity()->isHasModuleConformiteTraitement()) {
            throw new NotFoundHttpException("No object found with ID '{$id}'");
        }

        /** @var User $user */
        $user = $this->getUser();
        if (!$user->hasAccessTo($object->getTraitement())) {
            return $this->redirectToRoute($this->getRouteName('list'));
        }

        $service       = $object->getTraitement()->getService();
        $user          = $this->userProvider->getAuthenticatedUser();
        $services_user = $user->getServices();

        if (!($this->authorizationChecker->isGranted('ROLE_USER') && ($services_user->isEmpty() || $services_user->contains($service)))) {
            return $this->redirectToRoute('registry_treatment_list');
        }

        // Before create form, hydrate new answers array with potential question responses
        foreach ($this->questionRepository->findNewQuestionsNotUseInGivenConformite($object) as $question) {
            $reponse = new Model\ConformiteTraitement\Reponse();
            $reponse->setQuestion($question);
            $object->addReponse($reponse);
        }

        $form = $this->createForm($this->getFormType(), $object, ['validation_groups' => ['default', $this->getModel(), 'edit']]);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->formPrePersistData($object);
            $this->entityManager->persist($object);
            $this->entityManager->flush();

            $this->dispatcher->dispatch(new ConformiteTraitementEvent($object));

            $this->addFlash('success', $this->getFlashbagMessage('success', 'edit', $object));

            return $this->redirectToRoute($this->getRouteName('list'));
        }

        $serviceEnabled = $object->getTraitement()->getCollectivity()->getIsServicesEnabled();

        return $this->render($this->getTemplatingBasePath('edit'), [
            'form'           => $form->createView(),
            'serviceEnabled' => $serviceEnabled,
        ]);
    }

    public function startAipdAction(Request $request, string $id)
    {
        /** @var ConformiteTraitement $conformiteTraitement */
        $conformiteTraitement = $this->repository->findOneById($id);
        if (!$conformiteTraitement) {
            throw new NotFoundHttpException("No object found with ID '{$id}'");
        }

        /** @var User $user */
        $user       = $this->getUser();
        $traitement = $conformiteTraitement->getTraitement();
        if ($traitement->getCollectivity() && !$traitement->getCollectivity()->isHasModuleConformiteTraitement()) {
            throw new AccessDeniedHttpException('La structure de ce traitement a le module conformité des traitements désactivé');
        }
        if (!$user->hasAccessTo($traitement)) {
            return $this->redirectToRoute($this->getRouteName('list'));
        }

        if ($request->isMethod('GET')) {
            return $this->render($this->getTemplatingBasePath('start'), [
                'totalItem'            => $this->modeleRepository->count(),
                'route'                => $this->router->generate('aipd_analyse_impact_modele_datatables', ['collectivity' => $conformiteTraitement->getTraitement()->getCollectivity()->getId()->toString()]),
                'conformiteTraitement' => $conformiteTraitement,
            ]);
        }

        if (!$request->request->has('modele_choice')) {
            throw new BadRequestHttpException('Parameter modele_choice must be present');
        }

        if (null === $modele = $this->modeleRepository->findOneById($request->request->get('modele_choice'))) {
            throw new NotFoundHttpException('No modele with Id ' . $request->request->get('modele_choice') . ' exists.');
        }

        $analyseImpact = ModeleToAnalyseConverter::createFromModeleAnalyse($modele);
        $analyseImpact->setCreatedAt(new \DateTimeImmutable());
        $analyseImpact->setConformiteTraitement($conformiteTraitement);

        $this->setAnalyseReponsesQuestionConformite($analyseImpact, $conformiteTraitement);

        foreach ($analyseImpact->getScenarioMenaces() as $scenarioMenace) {
            if (null !== $scenarioMenace->getMesuresProtections()) {
                foreach ($scenarioMenace->getMesuresProtections() as $mesureProtection) {
                    $this->entityManager->persist($mesureProtection);
                }
            }
        }

        $this->entityManager->persist($analyseImpact);
        $this->entityManager->flush();

        return $this->redirectToRoute('aipd_analyse_impact_create', [
            'id' => $analyseImpact->getId(),
        ]);
    }

    private function setAnalyseReponsesQuestionConformite(AnalyseImpact &$analyseImpact, ConformiteTraitement $conformiteTraitement)
    {
        foreach ($conformiteTraitement->getReponses() as $reponse) {
            $q = $reponse->getQuestion()->getQuestion();
            $q = $analyseImpact->getQuestionConformitesOfName($q);
            $q->setAnalyseImpact($analyseImpact);
            $q->setReponseConformite($reponse);
            $reponse->addAnalyseQuestionConformite($q);
            $this->entityManager->persist($reponse);
        }
    }

    public function listDataTables(Request $request): JsonResponse
    {
        $criteria = $this->getRequestCriteria();

        $treatments = $this->getResults($request, $criteria);
        $reponse    = $this->getBaseDataTablesResponse($request, $treatments, $criteria);

        /** @var Treatment $treatment */
        foreach ($treatments as $treatment) {
            if (is_array($treatment) && count($treatment) > 0) {
                $treatment = $treatment[0];
            }
            $actions = $this->generateActionCellContent($treatment);

            $reponse['data'][] = [
                'icon'                  => $this->getIcon($treatment),
                'nom'                   => $this->getTreatmentLink($treatment),
                'collectivite'          => $this->isGranted('ROLE_REFERENT') ? $treatment->getCollectivity()->getName() : '',
                'service'               => $this->userProvider->getAuthenticatedUser()->hasServices() && $treatment->getService() ? $treatment->getService()->getName() : '',
                'gestionnaire'          => $treatment->getManager(),
                'conformite_traitement' => $this->getTreatmentConformity($treatment),
                'conformite_question'   => $this->getQuestionConformity($treatment),
                'eval_createdAt'        => $treatment->getConformiteTraitement() ? date_format($treatment->getConformiteTraitement()->getCreatedAt(), 'd/m/Y') : null,
                'avis_aipd'             => $this->getAvisAipd($treatment),
                'aipd_createdAt'        => $treatment->getConformiteTraitement() && $treatment->getConformiteTraitement()->getLastAnalyseImpact() ? date_format($treatment->getConformiteTraitement()->getLastAnalyseImpact()->getCreatedAt(), 'd/m/Y') : null,
                'actions'               => $actions,
            ];
        }

        $jsonResponse = new JsonResponse();
        $jsonResponse->setJson(\json_encode($reponse));

        return $jsonResponse;
    }

    protected function getPlanifiedMesurements(ConformiteTraitement $conformiteTraitement): array
    {
        $planifiedMesurementsToBeNotified = [];
        foreach ($conformiteTraitement->getReponses() as $reponse) {
            $mesurements = \iterable_to_array($reponse->getActionProtectionsPlanifiedNotSeens());
            foreach ($mesurements as $mesurement) {
                if (!\in_array($mesurement, $planifiedMesurementsToBeNotified)) {
                    \array_push($planifiedMesurementsToBeNotified, $mesurement);
                }
            }
        }

        return $planifiedMesurementsToBeNotified;
    }

    protected function getIcon(Treatment $treatment)
    {
        // return '<span style="background:red">blabla</span>';
        $conf                            = $treatment->getConformiteTraitement();
        $planifiedMesurementToBeNotified = [];
        if ($conf) {
            $planifiedMesurementToBeNotified = $this->getPlanifiedMesurements($conf);
        }
        $result = '';
        if (count($planifiedMesurementToBeNotified)) {
            $m = '';

            foreach ($planifiedMesurementToBeNotified as $mes) {
                $m .= '<li>' . $mes->getName() . '</li>';
            }

            $result = '<div class="mesurement-hide">
                <i aria-hidden="true" class="primary-i fas fa-exclamation-circle"></i>
                <div class="primary">
                    <span>' . $this->translator->trans('registry.conformite_traitement.label.tooltip.mesurements_done') . '</span>
                    <ul>' . $m . '</ul>
                </div>
            </div>';
        }
        if ($conf && $conf->getNeedsAipd()) {
            $aipd = $conf->getLastAnalyseImpact();
            if ($aipd) {
                $result .= '<div>
                    <span style="display:none">
                  ' . $this->translator->trans('registry.conformite_traitement.label.tooltip.aipd_in_progress') . '  
</span>
                    <i class="fa fa-exclamation-triangle" aria-label="' . $this->translator->trans('registry.conformite_traitement.label.tooltip.aipd_in_progress') . '" title="' . $this->translator->trans('registry.conformite_traitement.label.tooltip.aipd_in_progress') . '" style="color:#f39c12;"></i>
                </div>';
            } else {
                $result .= '<div>
<span style="display:none">
                  ' . $this->translator->trans('registry.conformite_traitement.label.tooltip.aipd_to_do') . '  
</span>
                    <i class="fa fa-exclamation-triangle" aria-label="' . $this->translator->trans('registry.conformite_traitement.label.tooltip.aipd_to_do') . '" title="' . $this->translator->trans('registry.conformite_traitement.label.tooltip.aipd_to_do') . '" style="color:#dd4b39;"></i>
                </div>';
            }
        }

        return $result;
    }

    protected function getLabelAndKeysArray(): array
    {
        if ($this->authorizationChecker->isGranted('ROLE_REFERENT')) {
            return [
                'icon',
                'name',
                'collectivite',
                'service',
                'gestionnaire',
                'conformite_traitement',
                'conformite_questions',
                'eval_createdAt',
                'avis_aipd',
                'aipd_createdAt',
                'actions',
            ];
        } elseif ($this->userProvider->getAuthenticatedUser()->hasServices()) {
            return [
                'icon',
                'name',
                'service',
                'gestionnaire',
                'conformite_traitement',
                'conformite_questions',
                'eval_createdAt',
                'avis_aipd',
                'aipd_createdAt',
                'actions',
            ];
        }

        return [
            'icon',
            'name',
            'gestionnaire',
            'conformite_traitement',
            'conformite_questions',
            'eval_createdAt',
            'avis_aipd',
            'aipd_createdAt',
            'actions',
        ];
    }

    private function getRequestCriteria()
    {
        $criteria = [];
        $user     = $this->userProvider->getAuthenticatedUser();

        if (!$this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            $criteria['collectivity'] = $user->getCollectivity();
        }

        if (\in_array(UserRoleDictionary::ROLE_REFERENT, $user->getRoles())) {
            $criteria['collectivity'] = $user->getCollectivitesReferees();
        }

        // Add active treatment criteria
        // Fixes https://gitlab.adullact.net/soluris/madis/-/issues/966
        $criteria['active'] = true;

        // Only for collectivites that have conformité traitement module active
        // Fix for https://gitlab.adullact.net/soluris/madis/-/issues/950#note_147289
        $criteria['hasModuleConformiteTraitement'] = true;

        return $criteria;
    }

    private function getTreatmentLink(Treatment $treatment): string
    {
        $path = $this->router->generate('registry_treatment_show', ['id' => $treatment->getId()]);

        return '<a href="' . $path . '">' . $treatment->getName() . '</a> ';
    }

    private function getAvisAipd(Treatment $treatment)
    {
        if (!$treatment->getConformiteTraitement()) {
            return '<span class="label label-default" style="min-width: 100%; display: inline-block;">Non réalisée</span>';
        }
        $conf = $treatment->getConformiteTraitement();

        if (null === $conf->getLastAnalyseImpact()) {
            return '<span class="label label-default" style="min-width: 100%; display: inline-block;">Non réalisée</span>';
        }
        $analyse_impact = $conf->getLastAnalyseImpact();
        $statut         = $analyse_impact->getStatut();

        switch ($statut) {
            case 'defavorable':
                $label = 'Défavorable';
                $class = 'label-danger';
                break;
            case 'favorable_reserve':
                $label = 'Favorable avec réserve(s)';
                $class = 'label-warning';
                break;
            case 'favorable':
                $label = 'Favorable';
                $class = 'label-success';
                break;
            case 'en_cours':
                $label = 'En cours';
                $class = 'label-default';
                break;
            default:
                $label = 'Non réalisée';
                $class = 'label-default';
        }

        return '<span class="label ' . $class . '" style="min-width: 100%; display: inline-block;">' . $label . '</span>';
    }

    private function getQuestionConformity(Treatment $treatment): string
    {
        $res = '';
        $ct  = $treatment->getConformiteTraitement();
        if ($ct) {
            $nbTotal         = $ct->getNbConformes() + $ct->getNbNonConformesMineures() + $ct->getNbNonConformesMajeures();
            $widthNbConforme = $nbTotal > 0 ? round(($ct->getNbConformes() * 100) / $nbTotal) : 0;
            $widthNbMineure  = $nbTotal > 0 ? round(($ct->getNbNonConformesMineures() * 100) / $nbTotal) : 0;
            $widthNbMajeure  = 100 - ($widthNbConforme + $widthNbMineure);
            $res .= '<div class="stacked-bar-graph">';
            if ($widthNbConforme) {
                $res .= '<span style="width:' . $widthNbConforme . '%" class="bar-conforme tooltipchart"><span class="tooltipcharttext">Conforme : ' . $ct->getNbConformes() . '</span></span>';
            }
            if ($widthNbMineure) {
                $res .= '<span style="width:' . $widthNbMineure . '%" class="bar-non-conforme-mineure tooltipchart"><span class="tooltipcharttext">Non-conforme mineure : ' . $ct->getNbNonConformesMineures() . '</span></span>';
            }
            if ($widthNbMajeure) {
                $res .= '<span style="width:' . $widthNbMajeure . '%" class="bar-non-conforme-majeure tooltipchart"><span class="tooltipcharttext">Non-conforme majeure : ' . $ct->getNbNonConformesMajeures() . '</span></span>';
            }

            $res .= '</div>';
        }

        return $res;
    }

    private function getTreatmentConformity(Treatment $treatment)
    {
        if (!$treatment->getConformiteTraitement()) {
            return '<span class="label label-default" style="min-width: 100%; display: inline-block;">Non évalué</span>';
        }
        $conf  = $treatment->getConformiteTraitement();
        $level = ConformiteTraitementCompletion::getConformiteTraitementLevel($conf);

        $weight = ConformiteTraitementLevelDictionary::getConformitesWeight()[$level];

        switch ($weight) {
            case 1:
                $label = 'Conforme';
                $class = 'label-success';
                break;
            case 2:
                $label = 'Non-conforme mineure';
                $class = 'label-warning';
                break;
            default:
                $label = 'Non-conforme majeure';
                $class = 'label-danger';
        }

        return '<span class="label ' . $class . '" style="min-width: 100%; display: inline-block;">' . $label . '</span>';
    }

    private function generateActionCellContent(Treatment $treatment)
    {
        $id                  = $treatment->getId();
        $conformityTreatment = $treatment->getConformiteTraitement();
        $aipd                = $conformityTreatment?->getLastAnalyseImpact();
        $blocHtml            = '';
        $user                = $this->userProvider->getAuthenticatedUser();

        $interact = true;
        if ($treatment->getCollectivity()->getIsServicesEnabled() && $user->hasServices() && !$treatment->isInUserServices($user)) {
            $interact = false;
        }
        if ($this->authorizationChecker->isGranted('ROLE_USER') && $interact) {
            if (!$conformityTreatment) {
                $path  = $this->router->generate('registry_conformite_traitement_create', ['idTraitement' => $id]);
                $title = $this->translator->trans('registry.conformite_traitement.action.show_conformite_traitement');
                $class = 'fa-clipboard-check';
                $blocHtml .= $this->returnHtmlAction($path, $title, $class);
            } else {
                $path  = $this->router->generate('registry_conformite_traitement_edit', ['id' => $conformityTreatment->getId()]);
                $title = $this->translator->trans('registry.conformite_traitement.action.show_conformite_traitement');
                $class = 'fa-clipboard-check';
                $blocHtml .= $this->returnHtmlAction($path, $title, $class);
                if ($aipd) {
                    if ($aipd->isValidated()) {
                        $path  = $this->router->generate('aipd_analyse_impact_print', ['id' => $aipd->getId()]);
                        $title = $this->translator->trans('aipd.analyse_impact.action.print');
                        $class = 'fa-print';
                        $blocHtml .= $this->returnHtmlAction($path, $title, $class);
                    } else {
                        $path  = $this->router->generate('aipd_analyse_impact_edit', ['id' => $aipd->getId()]);
                        $title = $this->translator->trans('aipd.analyse_impact.action.edit');
                        $class = 'fa-pencil';
                        $blocHtml .= $this->returnHtmlAction($path, $title, $class);
                    }
                } else {
                    $path  = $this->router->generate('registry_conformite_traitement_start_aipd', ['id' => $conformityTreatment->getId()]);
                    $title = $this->translator->trans('aipd.analyse_impact.action.create');
                    $class = 'fa-magnifying-glass-chart';
                    $blocHtml .= $this->returnHtmlAction($path, $title, $class);
                }
            }
        } elseif ($aipd && $aipd->isValidated()) {
            $path  = $this->router->generate('aipd_analyse_impact_print', ['id' => $aipd->getId()]);
            $title = $this->translator->trans('aipd.analyse_impact.action.print');
            $class = 'fa-print';
            $blocHtml .= $this->returnHtmlAction($path, $title, $class);
        }

        return $blocHtml;
    }

    private function returnHtmlAction($path, $title, $class)
    {
        return '<a href="' . $path . '">
                        <i aria-hidden="true" class="fa ' . $class . '"></i>
                        ' . $title . '
                        </a> ';
    }

    private function isTreatmentInUserServices(Treatment $treatment): bool
    {
        $user = $this->userProvider->getAuthenticatedUser();
        if ($this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            return true;
        }

        return $treatment->isInUserServices($user);
    }

    protected function getResults(Request $request, array $criteria = []): ?Paginator
    {
        $first      = $request->request->get('start');
        $maxResults = $request->request->get('length');
        $orders     = $request->request->get('order');
        $columns    = $request->request->get('columns');

        $orderColumn = $this->getCorrespondingLabelFromkey($orders[0]['column']);
        $orderDir    = $orders[0]['dir'];

        $searches = [];
        foreach ($columns as $column) {
            if ('' !== $column['search']['value']) {
                $searches[$column['data']] = $column['search']['value'];
            }
        }

        return $this->treatmentRepository->findPaginated($first, $maxResults, $orderColumn, $orderDir, $searches, $criteria);
    }

    protected function getBaseDataTablesResponse(Request $request, $results, array $criteria = [])
    {
        $draw = $request->request->get('draw');

        $reponse = [
            'draw'            => $draw,
            'recordsTotal'    => $this->treatmentRepository->count($criteria),
            'recordsFiltered' => count($results),
            'data'            => [],
        ];

        return $reponse;
    }
}
