<?php

/**
 * This file is part of the MADIS - RGPD Management application.
 *
 * @copyright Copyright (c) 2018-2019 Soluris - Solutions Numériques Territoriales Innovantes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace App\Domain\Registry\Model;

use App\Application\Interfaces\CollectivityRelated;
use App\Application\Traits\Model\CollectivityTrait;
use App\Application\Traits\Model\CreatorTrait;
use App\Application\Traits\Model\HistoryTrait;
use App\Application\Traits\Model\ServiceTrait;
use App\Domain\Registry\Model\ConformiteTraitement\Reponse;
use App\Domain\Reporting\Model\LoggableSubject;
use App\Domain\User\Model\Service;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * Action de protection / Plan d'action.
 */
class Mesurement implements LoggableSubject, CollectivityRelated
{
    use CollectivityTrait;
    use CreatorTrait;
    use HistoryTrait;
    use ServiceTrait;

    /**
     * @var UuidInterface
     */
    private $id;

    /**
     * FR: Nom.
     *
     * @var string|null
     */
    private $name;

    /**
     * FR: Type.
     *
     * @var string|null
     */
    private $type;

    /**
     * FR: Description.
     *
     * @var string|null
     */
    private $description;

    /**
     * FR: Cout.
     *
     * @var string|null
     */
    private $cost;

    /**
     * FR: Charge.
     *
     * @var string|null
     */
    private $charge;

    /**
     * FR: Statut.
     *
     * @var string|null
     */
    private $status;

    /**
     * FR: Modifié par.
     *
     * @var string|null
     */
    private $updatedBy;

    /**
     * FR: Date de planification.
     *
     * @var \DateTime|null
     */
    private $planificationDate;

    /**
     * @var string|null
     */
    private $comment;

    /**
     * @var Mesurement|null
     */
    private $clonedFrom;

    /**
     * @var string|null
     */
    private $priority;

    /**
     * @var string|null
     */
    private $manager;

    /**
     * @var iterable
     */
    private $conformiteOrganisation;

    /**
     * @var iterable
     */
    private $conformiteTraitementReponses;

    private Collection $treatments;
    private Collection $contractors;
    private Collection $requests;
    private Collection $violations;
    private Collection $proofs;
    private Collection $tools;
    /**
     * @var Service|null
     */
    private $service;
    private Collection $mesurements;

    private ?iterable $answerSurveys;

    /**
     * Mesurement constructor.
     *
     * @throws \Exception
     */
    public function __construct()
    {
        $this->id                           = Uuid::uuid4();
        $this->proofs                       = new ArrayCollection();
        $this->treatments                   = new ArrayCollection();
        $this->requests                     = new ArrayCollection();
        $this->violations                   = new ArrayCollection();
        $this->contractors                  = new ArrayCollection();
        $this->tools                        = new ArrayCollection();
        $this->mesurements                  = new ArrayCollection();
        $this->conformiteTraitementReponses = [];
        $this->conformiteOrganisation       = [];
    }

    public function __toString(): string
    {
        if (\is_null($this->getName())) {
            return '';
        }

        if (\mb_strlen($this->getName()) > 150) {
            return \mb_substr($this->getName(), 0, 150) . '...';
        }

        return $this->getName();
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getUpdatedBy(): ?string
    {
        return $this->updatedBy;
    }

    public function setUpdatedBy(?string $updatedBy): void
    {
        $this->updatedBy = $updatedBy;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): void
    {
        $this->type = $type;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    public function getCost(): ?string
    {
        return $this->cost;
    }

    public function setCost(?string $cost): void
    {
        $this->cost = $cost;
    }

    public function getCharge(): ?string
    {
        return $this->charge;
    }

    public function setCharge(?string $charge): void
    {
        $this->charge = $charge;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): void
    {
        $this->status = $status;
    }

    public function getPlanificationDate(): ?\DateTime
    {
        return $this->planificationDate;
    }

    public function setPlanificationDate(?\DateTime $planificationDate): void
    {
        $this->planificationDate = $planificationDate;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): void
    {
        $this->comment = $comment;
    }

    public function getClonedFrom(): ?Mesurement
    {
        return $this->clonedFrom;
    }

    public function setClonedFrom(?Mesurement $clonedFrom): void
    {
        $this->clonedFrom = $clonedFrom;
    }

    public function getPriority(): ?string
    {
        return $this->priority;
    }

    public function setPriority(?string $priority): void
    {
        $this->priority = $priority;
    }

    public function getManager(): ?string
    {
        return $this->manager;
    }

    public function setManager(?string $manager): void
    {
        $this->manager = $manager;
    }

    public function getConformiteOrganisation()
    {
        return $this->conformiteOrganisation;
    }

    /**
     * @return Reponse[]
     */
    public function getConformiteTraitementReponses()
    {
        return $this->conformiteTraitementReponses;
    }

    public function getTreatments(): Collection
    {
        return $this->treatments;
    }

    public function setTreatments(Collection $treatments)
    {
        $this->treatments = $treatments;
    }

    public function addTreatment(Treatment $treatment): void
    {
        $this->treatments[] = $treatment;
    }

    public function removeTreatment(Treatment $treatment): void
    {
        if ($this->treatments && $this->treatments->count() && $this->treatments->contains($treatment)) {
            $this->treatments->removeElement($treatment);
        }
    }

    public function getContractors(): Collection
    {
        return $this->contractors;
    }

    public function setContractors(Collection $contractors): void
    {
        $this->contractors = $contractors;
    }

    public function addContractor(Contractor $contractor): void
    {
        $this->contractors[] = $contractor;
    }

    public function removeContractor(Contractor $contractor): void
    {
        if ($this->contractors && $this->contractors->count() && $this->contractors->contains($contractor)) {
            $this->contractors->removeElement($contractor);
        }
    }

    public function getRequests(): Collection
    {
        return $this->requests;
    }

    public function setRequests(Collection $requests): void
    {
        $this->requests = $requests;
    }

    public function addRequest(Request $request): void
    {
        $this->requests[] = $request;
    }

    public function removeRequest(Request $request): void
    {
        if ($this->requests && $this->requests->count() && $this->requests->contains($request)) {
            $this->requests->removeElement($request);
        }
    }

    public function getViolations(): Collection
    {
        return $this->violations;
    }

    public function setViolations(Collection $violations): void
    {
        $this->violations = $violations;
    }

    public function addViolation(Violation $violation): void
    {
        $this->violations->add($violation);
    }

    public function removeViolation(Violation $violation): void
    {
        if ($this->violations && $this->violations->count() && $this->violations->contains($violation)) {
            $this->violations->removeElement($violation);
        }
    }

    public function getTools(): Collection
    {
        return $this->tools;
    }

    public function setTools(Collection $tools): void
    {
        $this->tools = $tools;
    }

    public function addTool(Tool $tool): void
    {
        $this->tools->add($tool);
    }

    public function removeTool(Tool $tool): void
    {
        if ($this->tools && $this->tools->count() && $this->tools->contains($tool)) {
            $this->tools->removeElement($tool);
        }
    }

    public function getProofs(): Collection
    {
        return $this->proofs;
    }

    public function setProofs(Collection $proofs): void
    {
        $this->proofs = $proofs;
    }

    public function addProof(Proof $proof): void
    {
        $this->proofs->add($proof);
        $proof->addMesurement($this);
    }

    public function removeProof(Proof $proof): void
    {
        if ($this->proofs->count() && $this->proofs->contains($proof)) {
            $this->proofs->removeElement($proof);
        }
        $proof->removeMesurement($this);
    }

    public function getAnswerSurveys(): ?iterable
    {
        return $this->answerSurveys;
    }

    public function setAnswerSurveys(?iterable $answerSurveys): void
    {
        $this->answerSurveys = $answerSurveys;
    }

    public function getMesurements(): ArrayCollection|Collection
    {
        return $this->mesurements;
    }

    public function setMesurements(ArrayCollection|Collection $mesurements): void
    {
        $this->mesurements = $mesurements;
        foreach ($mesurements as $mesurement) {
            $mesurement->addMesurement($this);
        }
    }

    public function addMesurement(Mesurement $mesurement): void
    {
        $this->mesurements->add($mesurement);
    }
}
