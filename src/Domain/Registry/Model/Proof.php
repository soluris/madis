<?php

/**
 * This file is part of the MADIS - RGPD Management application.
 *
 * @copyright Copyright (c) 2018-2019 Soluris - Solutions Numériques Territoriales Innovantes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace App\Domain\Registry\Model;

use App\Application\Interfaces\CollectivityRelated;
use App\Application\Traits\Model\CollectivityTrait;
use App\Application\Traits\Model\CreatorTrait;
use App\Application\Traits\Model\HistoryTrait;
use App\Application\Traits\Model\ServiceTrait;
use App\Application\Traits\Model\SoftDeletableTrait;
use App\Domain\Reporting\Model\LoggableSubject;
use App\Domain\User\Model\Service;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Proof implements LoggableSubject, CollectivityRelated
{
    use CollectivityTrait;
    use CreatorTrait;
    use HistoryTrait;
    use SoftDeletableTrait;
    use ServiceTrait;

    /**
     * @var UuidInterface
     */
    private $id;

    /**
     * @var string|null
     */
    private $name;

    /**
     * @var string|null
     */
    private $type;

    /**
     * @var string|null
     */
    private $document;

    /**
     * @var UploadedFile|null
     */
    private $documentFile;

    /**
     * @var string|null
     */
    private $comment;

    private Collection $treatments;
    private Collection $contractors;
    private Collection $mesurements;
    private Collection $requests;
    private Collection $violations;
    private Collection $tools;
    /**
     * @var Service|null
     */
    private $service;
    private Collection $proofs;

    /**
     * Proof constructor.
     *
     * @throws \Exception
     */
    public function __construct()
    {
        $this->id          = Uuid::uuid4();
        $this->treatments  = new ArrayCollection();
        $this->contractors = new ArrayCollection();
        $this->mesurements = new ArrayCollection();
        $this->requests    = new ArrayCollection();
        $this->violations  = new ArrayCollection();
        $this->proofs      = new ArrayCollection();
        $this->tools       = new ArrayCollection();
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function __toString(): string
    {
        if (\is_null($this->getName())) {
            return '';
        }

        if (\mb_strlen($this->getName()) > 150) {
            return \mb_substr($this->getName(), 0, 150) . '...';
        }

        return $this->getName();
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): void
    {
        $this->type = $type;
    }

    public function getDocument(): ?string
    {
        return $this->document;
    }

    public function setDocument(?string $document): void
    {
        $this->document = $document;
    }

    public function getDocumentFile(): ?UploadedFile
    {
        return $this->documentFile;
    }

    public function setDocumentFile(?UploadedFile $documentFile): void
    {
        $this->documentFile = $documentFile;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): void
    {
        $this->comment = $comment;
    }

    public function getTreatments(): Collection
    {
        return $this->treatments;
    }

    public function addTreatment(Treatment $treatment): void
    {
        $this->treatments->add($treatment);
    }

    public function removeTreatment(Treatment $treatment): void
    {
        if ($this->treatments && $this->treatments->count() && $this->treatments->contains($treatment)) {
            $this->treatments->removeElement($treatment);
        }
    }

    public function getContractors(): Collection
    {
        return $this->contractors;
    }

    public function addContractor(Contractor $contractor): void
    {
        $this->contractors->add($contractor);
    }

    public function removeContractor(Contractor $contractor): void
    {
        if ($this->contractors && $this->contractors->count() && $this->contractors->contains($contractor)) {
            $this->contractors->removeElement($contractor);
        }
    }

    public function getMesurements(): Collection
    {
        return $this->mesurements;
    }

    public function addMesurement(Mesurement $mesurement): void
    {
        $this->mesurements->add($mesurement);
    }

    public function removeMesurement(Mesurement $mesurement): void
    {
        if ($this->mesurements && $this->mesurements->count() && $this->mesurements->contains($mesurement)) {
            $this->mesurements->removeElement($mesurement);
        }
    }

    public function getRequests(): Collection
    {
        return $this->requests;
    }

    public function addRequest(Request $request): void
    {
        $this->requests->add($request);
    }

    public function removeRequest(Request $request): void
    {
        if ($this->requests && $this->requests->count() && $this->requests->contains($request)) {
            $this->requests->removeElement($request);
        }
    }

    public function getViolations(): Collection
    {
        return $this->violations;
    }

    public function addViolation(Violation $violation): void
    {
        $this->violations->add($violation);
    }

    public function removeViolation(Violation $violation): void
    {
        if ($this->violations && $this->violations->count() && $this->violations->contains($violation)) {
            $this->violations->removeElement($violation);
        }
    }

    public function getTools(): ?Collection
    {
        return $this->tools;
    }

    public function setTools(Collection $tools): void
    {
        $this->tools = $tools;
    }

    public function addTool(Tool $tool): void
    {
        $this->tools->add($tool);
    }

    public function removeTool(Tool $tool): void
    {
        if ($this->tools && $this->tools->count() && $this->tools->contains($tool)) {
            $this->tools->removeElement($tool);
        }
    }

    public function getProofs(): ArrayCollection|Collection
    {
        return $this->proofs;
    }

    public function setProofs(ArrayCollection|Collection $proofs): void
    {
        $this->proofs = $proofs;
        foreach ($proofs as $proof) {
            $proof->addProof($this);
        }
    }

    public function addProof(Proof $proof): void
    {
        $this->proofs->add($proof);
    }
}
