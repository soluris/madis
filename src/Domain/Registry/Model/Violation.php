<?php

/**
 * This file is part of the MADIS - RGPD Management application.
 *
 * @copyright Copyright (c) 2018-2019 Soluris - Solutions Numériques Territoriales Innovantes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace App\Domain\Registry\Model;

use App\Application\Interfaces\CollectivityRelated;
use App\Application\Traits\Model\CollectivityTrait;
use App\Application\Traits\Model\CreatorTrait;
use App\Application\Traits\Model\HistoryTrait;
use App\Application\Traits\Model\ServiceTrait;
use App\Application\Traits\Model\SoftDeletableTrait;
use App\Domain\Reporting\Model\LoggableSubject;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class Violation implements LoggableSubject, CollectivityRelated
{
    use CollectivityTrait;
    use CreatorTrait;
    use HistoryTrait;
    use SoftDeletableTrait;
    use ServiceTrait;

    /**
     * @var UuidInterface
     */
    private $id;

    /**
     * @var \DateTime|null
     */
    private $date;

    /**
     * @var bool
     */
    private $inProgress;

    /**
     * @var iterable
     */
    private $violationNatures;

    /**
     * @var iterable
     */
    private $origins;

    /**
     * @var string|null
     */
    private $cause;

    /**
     * @var string|null
     */
    private $updatedBy;

    /**
     * @var iterable
     */
    private $concernedDataNature;

    /**
     * @var iterable
     */
    private $concernedPeopleCategories;

    /**
     * @var int|null
     */
    private $nbAffectedRows;

    /**
     * @var int|null
     */
    private $nbAffectedPersons;

    /**
     * @var iterable
     */
    private $potentialImpactsNature;

    /**
     * @var string|null
     */
    private $gravity;

    /**
     * @var string|null
     */
    private $communication;

    /**
     * @var string|null
     */
    private $communicationPrecision;

    /**
     * @var string|null
     */
    private $appliedMeasuresAfterViolation;

    /**
     * @var string|null
     */
    private $notification;

    /**
     * @var string|null
     */
    private $notificationDetails;

    /**
     * @var string|null
     */
    private $comment;

    private Collection $proofs;
    private Collection $mesurements;
    private Collection $treatments;
    private Collection $contractors;
    private Collection $requests;
    private Collection $tools;
    private Collection $violations;

    /**
     * Violation constructor.
     *
     * @throws \Exception
     */
    public function __construct()
    {
        $this->id                        = Uuid::uuid4();
        $this->date                      = new \DateTime();
        $this->inProgress                = false;
        $this->violationNatures          = [];
        $this->origins                   = [];
        $this->concernedDataNature       = [];
        $this->concernedPeopleCategories = [];
        $this->potentialImpactsNature    = [];
        $this->proofs                    = new ArrayCollection();
        $this->mesurements               = new ArrayCollection();
        $this->treatments                = new ArrayCollection();
        $this->contractors               = new ArrayCollection();
        $this->requests                  = new ArrayCollection();
        $this->tools                     = new ArrayCollection();
        $this->violations                = new ArrayCollection();
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function __toString(): string
    {
        if (\is_null($this->getDate())) {
            return '';
        }

        return "Violation du {$this->getDate()->format('d/m/Y')}";
    }

    public function getUpdatedBy(): ?string
    {
        return $this->updatedBy;
    }

    public function setUpdatedBy(?string $updatedBy): void
    {
        $this->updatedBy = $updatedBy;
    }

    public function getDate(): ?\DateTime
    {
        return $this->date;
    }

    public function setDate(?\DateTime $date): void
    {
        $this->date = $date;
    }

    public function isInProgress(): bool
    {
        return $this->inProgress;
    }

    public function setInProgress(bool $inProgress): void
    {
        $this->inProgress = $inProgress;
    }

    public function getViolationNatures(): iterable
    {
        return $this->violationNatures;
    }

    public function setViolationNatures(iterable $violationNatures): void
    {
        $this->violationNatures = $violationNatures;
    }

    public function getOrigins(): iterable
    {
        return $this->origins;
    }

    public function setOrigins(iterable $origins): void
    {
        $this->origins = $origins;
    }

    public function getCause(): ?string
    {
        return $this->cause;
    }

    public function setCause(?string $cause): void
    {
        $this->cause = $cause;
    }

    public function getConcernedDataNature(): iterable
    {
        return $this->concernedDataNature;
    }

    public function setConcernedDataNature(iterable $concernedDataNature): void
    {
        $this->concernedDataNature = $concernedDataNature;
    }

    public function getConcernedPeopleCategories(): iterable
    {
        return $this->concernedPeopleCategories;
    }

    public function setConcernedPeopleCategories(iterable $concernedPeopleCategories): void
    {
        $this->concernedPeopleCategories = $concernedPeopleCategories;
    }

    public function getNbAffectedRows(): ?int
    {
        return $this->nbAffectedRows;
    }

    public function setNbAffectedRows(?int $nbAffectedRows): void
    {
        $this->nbAffectedRows = $nbAffectedRows;
    }

    public function getNbAffectedPersons(): ?int
    {
        return $this->nbAffectedPersons;
    }

    public function setNbAffectedPersons(?int $nbAffectedPersons): void
    {
        $this->nbAffectedPersons = $nbAffectedPersons;
    }

    public function getPotentialImpactsNature(): iterable
    {
        return $this->potentialImpactsNature;
    }

    public function setPotentialImpactsNature(iterable $potentialImpactsNature): void
    {
        $this->potentialImpactsNature = $potentialImpactsNature;
    }

    public function getGravity(): ?string
    {
        return $this->gravity;
    }

    public function setGravity(?string $gravity): void
    {
        $this->gravity = $gravity;
    }

    public function getCommunication(): ?string
    {
        return $this->communication;
    }

    public function setCommunication(?string $communication): void
    {
        $this->communication = $communication;
    }

    public function getCommunicationPrecision(): ?string
    {
        return $this->communicationPrecision;
    }

    public function setCommunicationPrecision(?string $communicationPrecision): void
    {
        $this->communicationPrecision = $communicationPrecision;
    }

    public function getAppliedMeasuresAfterViolation(): ?string
    {
        return $this->appliedMeasuresAfterViolation;
    }

    public function setAppliedMeasuresAfterViolation(?string $appliedMeasuresAfterViolation): void
    {
        $this->appliedMeasuresAfterViolation = $appliedMeasuresAfterViolation;
    }

    public function getNotification(): ?string
    {
        return $this->notification;
    }

    public function setNotification(?string $notification): void
    {
        $this->notification = $notification;
    }

    public function getNotificationDetails(): ?string
    {
        return $this->notificationDetails;
    }

    public function setNotificationDetails(?string $notificationDetails): void
    {
        $this->notificationDetails = $notificationDetails;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): void
    {
        $this->comment = $comment;
    }

    public function getProofs(): Collection
    {
        return $this->proofs;
    }

    public function setProofs($proofs)
    {
        $this->proofs = $proofs;
    }

    public function addProof(Proof $proof): void
    {
        $this->proofs->add($proof);
        $proof->addViolation($this);
    }

    public function removeProof(Proof $proof): void
    {
        if ($this->proofs && $this->proofs->count() && $this->proofs->contains($proof)) {
            $this->proofs->removeElement($proof);
            $proof->removeViolation($this);
        }
    }

    public function getMesurements(): Collection
    {
        return $this->mesurements;
    }

    public function setMesurements(Collection $mesurements): void
    {
        $this->mesurements = $mesurements;
    }

    public function addMesurement(Mesurement $mesurement): void
    {
        $this->mesurements->add($mesurement);
        $mesurement->addViolation($this);
    }

    public function removeMesurement(Mesurement $mesurement): void
    {
        if ($this->mesurements && $this->mesurements->count() && $this->mesurements->contains($mesurement)) {
            $this->mesurements->removeElement($mesurement);
            $mesurement->removeViolation($this);
        }
    }

    public function addRequest(Request $request): void
    {
        $this->requests[] = $request;
    }

    public function removeRequest(Request $request): void
    {
        if ($this->requests && $this->requests->count() && $this->requests->contains($request)) {
            $this->requests->removeElement($request);
        }
    }

    public function addTreatment(Treatment $treatment): void
    {
        $this->treatments[] = $treatment;
    }

    public function removeTreatment(Treatment $treatment): void
    {
        if ($this->treatments && $this->treatments->count() && $this->treatments->contains($treatment)) {
            $this->treatments->removeElement($treatment);
        }
    }

    public function getTreatments(): Collection
    {
        return $this->treatments;
    }

    public function addContractor(Contractor $contractor): void
    {
        $this->contractors[] = $contractor;
    }

    public function removeContractor(Contractor $contractor): void
    {
        if ($this->contractors && $this->contractors->count() && $this->contractors->contains($contractor)) {
            $this->contractors->removeElement($contractor);
        }
    }

    public function getContractors(): Collection
    {
        return $this->contractors;
    }

    public function getRequests(): ArrayCollection|Collection
    {
        return $this->requests;
    }

    public function setRequests(ArrayCollection|Collection $requests): void
    {
        $this->requests = $requests;
    }

    public function getTools(): ArrayCollection|Collection
    {
        return $this->tools;
    }

    public function setTools(ArrayCollection|Collection $tools): void
    {
        $this->tools = $tools;
    }

    public function addTool(Tool $tool): void
    {
        $this->tools[] = $tool;
    }

    public function removeTool(Tool $tool): void
    {
        if ($this->tools && $this->tools->count() && $this->tools->contains($tool)) {
            $this->tools->removeElement($tool);
        }
    }

    public function getViolations(): ArrayCollection|Collection
    {
        return $this->violations;
    }

    public function setViolations(ArrayCollection|Collection $violations): void
    {
        $this->violations = $violations;
        foreach ($violations as $violation) {
            $violation->addViolation($this);
        }
    }

    public function addViolation(Violation $violation): void
    {
        $this->violations->add($violation);
    }
}
