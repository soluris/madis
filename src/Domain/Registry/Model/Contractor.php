<?php

/**
 * This file is part of the MADIS - RGPD Management application.
 *
 * @copyright Copyright (c) 2018-2019 Soluris - Solutions Numériques Territoriales Innovantes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace App\Domain\Registry\Model;

use App\Application\Interfaces\CollectivityRelated;
use App\Application\Traits\Model\CollectivityTrait;
use App\Application\Traits\Model\CreatorTrait;
use App\Application\Traits\Model\HistoryTrait;
use App\Application\Traits\Model\ServiceTrait;
use App\Domain\Registry\Model\Embeddable\Address;
use App\Domain\Reporting\Model\LoggableSubject;
use App\Domain\User\Model\Embeddable\Contact;
use App\Domain\User\Model\Service;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class Contractor implements LoggableSubject, CollectivityRelated
{
    use CollectivityTrait;
    use CreatorTrait;
    use HistoryTrait;
    use ServiceTrait;

    /**
     * @var UuidInterface
     */
    private $id;

    /**
     * @var string|null
     */
    private $name;

    /**
     * @var string|null
     */
    private $referent;

    /**
     * Clauses contractuelles vérifiées.
     *
     * @var bool
     */
    private $contractualClausesVerified;

    /**
     * @var string|null
     */
    private $otherInformations;

    /**
     * @var string|null
     */
    private $updatedBy;

    /**
     * @var Address|null
     */
    private $address;

    /**
     * @var Contractor|null
     */
    private $clonedFrom;

    /**
     * Adopte les éléments de sécurité.
     *
     * @var bool
     */
    private $adoptedSecurityFeatures;

    /**
     * Registre des traitements.
     *
     * @var bool
     */
    private $maintainsTreatmentRegister;

    /**
     * Données dans l'UE.
     *
     * @var bool
     */
    private $sendingDataOutsideEu;

    /**
     * @var Contact|null
     */
    private $dpo;

    /**
     * @var Contact|null
     */
    private $legalManager;

    /**
     * @var bool
     */
    private $hasDpo;

    /**
     * @var Service|null
     */
    private $service;

    private Collection $mesurements;
    private Collection $requests;
    private Collection $violations;
    private Collection $tools;
    private Collection $treatments;
    private Collection $proofs;
    private Collection $contractors;

    /**
     * Contractor constructor.
     *
     * @throws \Exception
     */
    public function __construct()
    {
        $this->id                         = Uuid::uuid4();
        $this->contractualClausesVerified = false;
        $this->adoptedSecurityFeatures    = false;
        $this->maintainsTreatmentRegister = false;
        $this->sendingDataOutsideEu       = false;
        $this->hasDpo                     = false;
        $this->treatments                 = new ArrayCollection();
        $this->proofs                     = new ArrayCollection();
        $this->tools                      = new ArrayCollection();
        $this->mesurements                = new ArrayCollection();
        $this->requests                   = new ArrayCollection();
        $this->violations                 = new ArrayCollection();
        $this->contractors                = new ArrayCollection();
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function __toString(): string
    {
        if (\is_null($this->getName())) {
            return '';
        }

        if (\mb_strlen($this->getName()) > 150) {
            return \mb_substr($this->getName(), 0, 150) . '...';
        }

        return $this->getName();
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    public function getUpdatedBy(): ?string
    {
        return $this->updatedBy;
    }

    public function setUpdatedBy(?string $updatedBy): void
    {
        $this->updatedBy = $updatedBy;
    }

    public function getReferent(): ?string
    {
        return $this->referent;
    }

    public function setReferent(?string $referent): void
    {
        $this->referent = $referent;
    }

    public function isContractualClausesVerified(): bool
    {
        return $this->contractualClausesVerified;
    }

    public function setContractualClausesVerified(bool $contractualClausesVerified): void
    {
        $this->contractualClausesVerified = $contractualClausesVerified;
    }

    public function getOtherInformations(): ?string
    {
        return $this->otherInformations;
    }

    public function setOtherInformations(?string $otherInformations): void
    {
        $this->otherInformations = $otherInformations;
    }

    public function getAddress(): ?Address
    {
        return $this->address;
    }

    public function setAddress(Address $address): void
    {
        $this->address = $address;
    }

    public function getClonedFrom(): ?Contractor
    {
        return $this->clonedFrom;
    }

    public function setClonedFrom(?Contractor $clonedFrom): void
    {
        $this->clonedFrom = $clonedFrom;
    }

    public function isAdoptedSecurityFeatures(): bool
    {
        return $this->adoptedSecurityFeatures;
    }

    public function setAdoptedSecurityFeatures(bool $adoptedSecurityFeatures): void
    {
        $this->adoptedSecurityFeatures = $adoptedSecurityFeatures;
    }

    public function isMaintainsTreatmentRegister(): bool
    {
        return $this->maintainsTreatmentRegister;
    }

    public function setMaintainsTreatmentRegister(bool $maintainsTreatmentRegister): void
    {
        $this->maintainsTreatmentRegister = $maintainsTreatmentRegister;
    }

    public function isSendingDataOutsideEu(): bool
    {
        return $this->sendingDataOutsideEu;
    }

    public function setSendingDataOutsideEu(bool $sendingDataOutsideEu): void
    {
        $this->sendingDataOutsideEu = $sendingDataOutsideEu;
    }

    public function getDpo(): ?Contact
    {
        return $this->dpo;
    }

    public function setDpo(?Contact $dpo): void
    {
        $this->dpo = $dpo;
    }

    public function getLegalManager(): ?Contact
    {
        return $this->legalManager;
    }

    public function setLegalManager(?Contact $legalManager): void
    {
        $this->legalManager = $legalManager;
    }

    public function isHasDpo(): bool
    {
        return $this->hasDpo;
    }

    public function setHasDpo(bool $hasDpo): void
    {
        $this->hasDpo = $hasDpo;
    }

    public function getTreatments(): Collection
    {
        return $this->treatments;
    }

    public function setTreatments(Collection $treatments)
    {
        $this->treatments = $treatments;
    }

    public function addTreatment(Treatment $treatment): void
    {
        $treatment->addContractor($this);
        $this->treatments->add($treatment);
    }

    public function removeTreatment(Treatment $treatment): void
    {
        $treatment->removeContractor($this);
        if ($this->treatments && $this->treatments->count() && $this->treatments->contains($treatment)) {
            $this->treatments->removeElement($treatment);
        }
    }

    public function getMesurements(): Collection
    {
        return $this->mesurements;
    }

    public function setMesurements(Collection $mesurements): void
    {
        $this->mesurements = $mesurements;
    }

    public function addMesurement(Mesurement $mesurement): void
    {
        $mesurement->addContractor($this);
        $this->mesurements->add($mesurement);
    }

    public function removeMesurement(Mesurement $mesurement): void
    {
        $mesurement->removeContractor($this);
        if ($this->mesurements && $this->mesurements->count() && $this->mesurements->contains($mesurement)) {
            $this->mesurements->removeElement($mesurement);
        }
    }

    public function getTools(): Collection
    {
        return $this->tools;
    }

    public function setTools(Collection $tools): void
    {
        $this->tools = $tools;
    }

    public function addTool(Tool $tool): void
    {
        $tool->addContractor($this);
        $this->tools->add($tool);
    }

    public function removeTool(Tool $tool): void
    {
        $tool->removeContractor($this);
        if ($this->tools && $this->tools->count() && $this->tools->contains($tool)) {
            $this->tools->removeElement($tool);
        }
    }

    public function getRequests(): Collection
    {
        return $this->requests;
    }

    public function setRequests(Collection $requests): void
    {
        $this->requests = $requests;
    }

    public function addRequest(Request $request): void
    {
        $request->addContractor($this);
        $this->requests->add($request);
    }

    public function removeRequest(Request $request): void
    {
        $request->removeContractor($this);
        if ($this->requests && $this->requests->count() && $this->requests->contains($request)) {
            $this->requests->removeElement($request);
        }
    }

    public function getViolations(): Collection
    {
        return $this->violations;
    }

    public function setViolations(Collection $violations): void
    {
        $this->violations = $violations;
    }

    public function addViolation(Violation $violation): void
    {
        $violation->addContractor($this);
        $this->violations->add($violation);
    }

    public function removeViolation(Violation $violation): void
    {
        $violation->removeContractor($this);
        if ($this->violations && $this->violations->count() && $this->violations->contains($violation)) {
            $this->violations->removeElement($violation);
        }
    }

    public function getProofs(): Collection
    {
        return $this->proofs;
    }

    public function setProofs(Collection $proofs)
    {
        $this->proofs = $proofs;
    }

    public function addProof(Proof $proof): void
    {
        $proof->addContractor($this);
        $this->proofs->add($proof);
    }

    public function removeProof(Proof $proof): void
    {
        $proof->removeContractor($this);
        if ($this->proofs && $this->proofs->count() && $this->proofs->contains($proof)) {
            $this->proofs->removeElement($proof);
        }
    }

    public function getContractors(): ArrayCollection|Collection
    {
        return $this->contractors;
    }

    public function setContractors(ArrayCollection|Collection $contractors): void
    {
        $this->contractors = $contractors;
        foreach ($contractors as $contractor) {
            $contractor->addContractor($this);
        }
    }

    public function addContractor(Contractor $contractor): void
    {
        $this->contractors->add($contractor);
    }
}
