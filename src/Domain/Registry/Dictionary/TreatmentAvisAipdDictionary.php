<?php

/**
 * This file is part of the MADIS - RGPD Management application.
 *
 * @copyright Copyright (c) 2018-2019 Soluris - Solutions Numériques Territoriales Innovantes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace App\Domain\Registry\Dictionary;

use App\Application\Dictionary\SimpleDictionary;

class TreatmentAvisAipdDictionary extends SimpleDictionary
{
    public const DEFAVORABLE       = 'defavorable';
    public const FAVORABLE_RESERVE = 'favorable_reserve';
    public const FAVORABLE         = 'favorable';
    public const EN_COURS          = 'en_cours';
    public const NON_REALISEE      = 'non_realisee';

    public function __construct()
    {
        parent::__construct('registry_treatment_avis_aipd', self::getAvisAipd());
    }

    /**
     * Get an array of Basis.
     *
     * @return array
     */
    public static function getAvisAipd()
    {
        return [
            self::DEFAVORABLE       => 'Défavorable',
            self::FAVORABLE_RESERVE => 'Favorable avec réserve(s)',
            self::FAVORABLE         => 'Favorable',
            self::EN_COURS          => 'En cours',
            self::NON_REALISEE      => 'Non réalisée',
        ];
    }

    /**
     * Get keys of the Basis array.
     *
     * @return array
     */
    public static function getAvisAipdsKeys()
    {
        return \array_keys(self::getAvisAipd());
    }
}
