<?php

/**
 * This file is part of the MADIS - RGPD Management application.
 *
 * @copyright Copyright (c) 2018-2019 Soluris - Solutions Numériques Territoriales Innovantes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace App\Domain\Registry\Dictionary;

use App\Application\Dictionary\SimpleDictionary;

class TreatmentSpecificDictionary extends SimpleDictionary
{
    public const LARGE_SCALE_COLLECTION               = 'large_scale_collection';
    public const DATA_CROSSING                        = 'data_crossing';
    public const AUTOMATED_DECISION_WITH_LEGAL_EFFECT = 'automated_decisions_with_legal_effect';
    public const EVALUATION_OR_RATING                 = 'evaluation_or_rating';
    public const AUTOMATIC_EXCLUSION_SERVICE          = 'automatic_exclusion_service';
    public const VULNERABLE_PEOPLE                    = 'vulnerable_people';
    public const SYSTEMATIC_MONITORING                = 'systematic_monitoring';
    public const INNOVATE_USE                         = 'innovative_use';

    public function __construct()
    {
        parent::__construct('registry_treatment_specific', self::getSpecificTreatment());
    }

    /**
     * Get an array of Basis.
     *
     * @return array
     */
    public static function getSpecificTreatment()
    {
        return [
            self::LARGE_SCALE_COLLECTION               => 'Collecte à large échelle',
            self::DATA_CROSSING                        => 'Croisement d\'ensemble de données',
            self::AUTOMATED_DECISION_WITH_LEGAL_EFFECT => 'Décision automatique avec effet juridique',
            self::EVALUATION_OR_RATING                 => 'Évaluation ou notation',
            self::AUTOMATIC_EXCLUSION_SERVICE          => 'Exclusion du bénéfice d\'un droit, d’un service ou contrat',
            self::VULNERABLE_PEOPLE                    => 'Personnes vulnérables',
            self::SYSTEMATIC_MONITORING                => 'Surveillance systématique',
            self::INNOVATE_USE                         => 'Usage innovant',
        ];
    }

    /**
     * Get keys of the Basis array.
     *
     * @return array
     */
    public static function getgetSpecificTreatmentsKeys()
    {
        return \array_keys(self::getSpecificTreatment());
    }
}
