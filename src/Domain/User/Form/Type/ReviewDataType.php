<?php

/**
 * This file is part of the MADIS - RGPD Management application.
 *
 * @copyright Copyright (c) 2018-2019 Soluris - Solutions Numériques Territoriales Innovantes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace App\Domain\User\Form\Type;

use App\Domain\User\Model\Collectivity;
use App\Domain\User\Model\ReviewData;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Validator\Constraints\File;

class ReviewDataType extends AbstractType
{
    /**
     * @var AuthorizationCheckerInterface
     */
    private $authorizationChecker;

    private string $maxSize;

    /**
     * CollectivityType constructor.
     */
    public function __construct(AuthorizationCheckerInterface $authorizationChecker, string $maxSize)
    {
        $this->maxSize              = $maxSize;
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * Build type form.
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $logo = null;
        if (isset($options['data'])) {
            $logo = $options['data']->getLogo();
        }
        if ($logo) {
            $builder->add('deleteLogo', HiddenType::class, [
                'mapped'   => false,
                'required' => false,
                'attr'     => [
                    'value' => '0',
                ],
            ]);
        }
        $builder
            ->add('documentName', TextType::class, [
                'label'       => 'user.organization.label.document_name',
                'required'    => true,
                'purify_html' => true,
            ])
            ->add('logo', FileType::class, [
                'label'       => 'user.organization.label.logo',
                'mapped'      => false,
                'required'    => false,
                'constraints' => [
                    // Élément suivant commenté, car il génère un message d'erreur en plus de l'autre message
                    // new Image(['groups' => ['default']]),
                    new File([
                        'maxSize'   => $this->maxSize,
                        'groups'    => ['default'],
                        'mimeTypes' => [
                            'image/png', // .png
                            'image/jpeg', // .jpg, .jpeg
                        ],
                        'mimeTypesMessage' => 'document_validator.document_file.thumbnail',
                    ]),
                ],
                'attr' => [
                    'accept' => 'image/*',
                ],
            ])
            ->add('showCollectivityLogoFooter', CheckboxType::class, [
                'label'    => 'user.organization.label.show_collectivity_logo',
                'required' => false,
            ])
            ->add('showDPDLogoFooter', CheckboxType::class, [
                'label'    => 'user.organization.label.show_dpd_logo',
                'required' => false,
            ])
        ;
        if ($this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            /** @var Collectivity|null $collectivity */
            $collectivity = null;
            //            $sections = [
            //                ReviewData::CONTINUOUS_AMELIORATION,
            //                ReviewData::CONTRACTOR_REGISTRY,
            //                ReviewData::PROOF_LIST,
            //                ReviewData::PROTECT_ACTIONS,
            //                ReviewData::REQUEST_REGISTRY,
            //                ReviewData::SUIVI,
            //                ReviewData::TREATMENT_CONFORMITY,
            //                ReviewData::AIPD,
            //                ReviewData::COLLECTIVITY_CONFORMITY,
            //                ReviewData::CONFORMITY_EVALUATION,
            //                ReviewData::TREATMENT_REGISTRY,
            //                ReviewData::USER_LIST,
            //                ReviewData::VIOLATION_REGISTRY,
            //                ReviewData::TOOL_REGISTRY,
            //            ];
            if (isset($options['data'])) {
                $collectivity = $options['data']->getCollectivity();
                //                $sections = $options['data']->getSections();
            }
            if (!$collectivity && isset($options['empty_data'])) {
                $collectivity = $options['empty_data']->getCollectivity();
            }

            // Always show every choice: https://gitlab.adullact.net/soluris/madis/-/issues/932

            $choices = self::getSections();

            $builder->add('sections', ChoiceType::class, [
                'label'    => 'user.organization.label.sections',
                'required' => false,
                //                'data' => $sections,
                'choices'  => $choices,
                'multiple' => true,
                'expanded' => false,
                'attr'     => [
                    'class'            => 'selectpicker',
                    'data-live-search' => 'true',
                    'title'            => 'global.placeholder.multiple_select',
                    'aria-label'       => 'Sections du bilan',
                ],
            ]);
        }
    }

    /**
     * Provide type options.
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class'        => ReviewData::class,
                'validation_groups' => [
                    'default',
                    'user',
                ],
            ]);
    }

    public static function getSections(): array
    {
        return [
            'user.organization.label.' . ReviewData::TREATMENT_REGISTRY  => ReviewData::TREATMENT_REGISTRY,
            'user.organization.label.' . ReviewData::CONTRACTOR_REGISTRY => ReviewData::CONTRACTOR_REGISTRY,

            'user.organization.label.' . ReviewData::TOOL_REGISTRY => ReviewData::TOOL_REGISTRY,

            'user.organization.label.' . ReviewData::REQUEST_REGISTRY   => ReviewData::REQUEST_REGISTRY,
            'user.organization.label.' . ReviewData::VIOLATION_REGISTRY => ReviewData::VIOLATION_REGISTRY,

            'user.organization.label.' . ReviewData::CONFORMITY_EVALUATION => ReviewData::CONFORMITY_EVALUATION,

            'user.organization.label.' . ReviewData::TREATMENT_CONFORMITY => ReviewData::TREATMENT_CONFORMITY,

            'user.organization.label.' . ReviewData::AIPD                    => ReviewData::AIPD,
            'user.organization.label.' . ReviewData::COLLECTIVITY_CONFORMITY => ReviewData::COLLECTIVITY_CONFORMITY,

            'user.organization.label.' . ReviewData::PROTECT_ACTIONS         => ReviewData::PROTECT_ACTIONS,
            'user.organization.label.' . ReviewData::CONTINUOUS_AMELIORATION => ReviewData::CONTINUOUS_AMELIORATION,

            'user.organization.label.' . ReviewData::PROOF_LIST => ReviewData::PROOF_LIST,

            // 'user.organization.label.' . ReviewData::SUIVI                   => ReviewData::SUIVI,

            'user.organization.label.' . ReviewData::USER_LIST => ReviewData::USER_LIST,
        ];
    }
}
