<?php

/**
 * This file is part of the MADIS - RGPD Management application.
 *
 * @copyright Copyright (c) 2018-2019 Soluris - Solutions Numériques Territoriales Innovantes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace App\Domain\User\Model;

use App\Application\Traits\Model\CollectivityTrait;
use App\Application\Traits\Model\HistoryTrait;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class ReviewData
{
    use CollectivityTrait;
    use HistoryTrait;

    public const TREATMENT_REGISTRY      = 'treatment_registry';
    public const CONTRACTOR_REGISTRY     = 'contractor_registry';
    public const TOOL_REGISTRY           = 'tool_registry';
    public const REQUEST_REGISTRY        = 'request_registry';
    public const VIOLATION_REGISTRY      = 'violation_registry';
    public const CONFORMITY_EVALUATION   = 'conformity_evaluation';
    public const TREATMENT_CONFORMITY    = 'treatment_conformity';
    public const AIPD                    = 'aipd';
    public const COLLECTIVITY_CONFORMITY = 'collectivity_conformity';
    public const PROTECT_ACTIONS         = 'protect_actions';
    public const CONTINUOUS_AMELIORATION = 'continuous_amelioration';
    public const PROOF_LIST              = 'proof_list';
    public const USER_LIST               = 'user_list';
    public const SUIVI                   = 'suivi';

    /**
     * @var UuidInterface
     */
    private $id;

    /**
     * @var string|null
     */
    private $documentName;

    /**
     * @var string|null
     */
    private $logo;

    private array $sections = [
        ReviewData::CONTINUOUS_AMELIORATION,
        ReviewData::CONTRACTOR_REGISTRY,
        ReviewData::PROOF_LIST,
        ReviewData::PROTECT_ACTIONS,
        ReviewData::REQUEST_REGISTRY,
        ReviewData::SUIVI,
        ReviewData::TREATMENT_CONFORMITY,
        ReviewData::AIPD,
        ReviewData::COLLECTIVITY_CONFORMITY,
        ReviewData::CONFORMITY_EVALUATION,
        ReviewData::TREATMENT_REGISTRY,
        ReviewData::USER_LIST,
        ReviewData::VIOLATION_REGISTRY,
        ReviewData::TOOL_REGISTRY,
    ];

    /**
     * @var bool
     */
    private $showCollectivityLogoFooter;

    /**
     * @var bool
     */
    private $showDPDLogoFooter;

    /**
     * @var Collectivity
     */
    private $collectivity;

    public function __construct()
    {
        $this->id = Uuid::uuid4();
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getDocumentName(): ?string
    {
        return $this->documentName;
    }

    public function setDocumentName(?string $documentName): void
    {
        $this->documentName = $documentName;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(?string $logo): void
    {
        $this->logo = $logo;
    }

    public function getSections(): array
    {
        return $this->sections;
    }

    public function setSections(array $sections): void
    {
        $this->sections = $sections;
    }

    public function isShowCollectivityLogoFooter(): bool
    {
        return $this->showCollectivityLogoFooter;
    }

    public function setShowCollectivityLogoFooter(bool $showCollectivityLogoFooter): void
    {
        $this->showCollectivityLogoFooter = $showCollectivityLogoFooter;
    }

    public function isShowDPDLogoFooter(): bool
    {
        return $this->showDPDLogoFooter;
    }

    public function setShowDPDLogoFooter(bool $showDPDLogoFooter): void
    {
        $this->showDPDLogoFooter = $showDPDLogoFooter;
    }
}
