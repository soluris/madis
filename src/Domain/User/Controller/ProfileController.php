<?php

/**
 * This file is part of the MADIS - RGPD Management application.
 *
 * @copyright Copyright (c) 2018-2019 Soluris - Solutions Numériques Territoriales Innovantes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace App\Domain\User\Controller;

use App\Application\Controller\ControllerHelper;
use App\Application\Symfony\Security\UserProvider;
use App\Domain\User\Form\Type\CollectivityType;
use App\Domain\User\Form\Type\ReviewDataType;
use App\Domain\User\Form\Type\UserType;
use App\Domain\User\Model\ReviewData;
use App\Domain\User\Model\User;
use App\Domain\User\Repository;
use Doctrine\ORM\EntityManagerInterface;
use Gaufrette\FilesystemInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\String\Slugger\SluggerInterface;

class ProfileController extends AbstractController
{
    // use ControllerTrait;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ControllerHelper
     */
    private $helper;

    /**
     * @var UserProvider
     */
    private $userProvider;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var Repository\Collectivity
     */
    private $collectivityRepository;

    /**
     * @var Repository\User
     */
    private $userRepository;

    /**
     * @var Security
     */
    private $security;
    private ?string $sso_type;

    /**
     * @var FilesystemInterface
     */
    protected $logoFilesystem;

    protected SluggerInterface $slugger;

    public function __construct(
        EntityManagerInterface $entityManager,
        ControllerHelper $helper,
        RequestStack $requestStack,
        UserProvider $userProvider,
        Repository\Collectivity $collectivityRepository,
        Repository\User $userRepository,
        Security $security,
        ?string $sso_type,
        FilesystemInterface $logoFilesystem,
        SluggerInterface $slugger,
    ) {
        $this->entityManager          = $entityManager;
        $this->helper                 = $helper;
        $this->requestStack           = $requestStack;
        $this->userProvider           = $userProvider;
        $this->collectivityRepository = $collectivityRepository;
        $this->userRepository         = $userRepository;
        $this->sso_type               = $sso_type;
        $this->security               = $security;
        $this->logoFilesystem         = $logoFilesystem;
        $this->slugger                = $slugger;
    }

    /**
     * Show user collectivity information.
     *
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function collectivityShowAction(): Response
    {
        $object = $this->userProvider->getAuthenticatedUser()->getCollectivity();

        return $this->helper->render('User/Profile/collectivity_show.html.twig', [
            'object'   => $object,
            'sections' => ReviewDataType::getSections(),
        ]);
    }

    public function formPrePersistData($object, $form = null): void
    {
        /** @var Form $reviewDataForm */
        $reviewDataForm = $form->get('reviewData');
        if ($reviewDataForm) {
            /** @var UploadedFile $logoFile */
            $logoFile = $reviewDataForm->get('logo')->getData();
            if ($logoFile) {
                $originalFilename = pathinfo($logoFile->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename = $this->slugger->slug($originalFilename);
                $newFilename  = $safeFilename . '-' . uniqid() . '.' . $logoFile->guessExtension();
                $this->logoFilesystem->write($newFilename, \fopen($logoFile->getRealPath(), 'r'));

                // updates the 'brochureFilename' property to store the PDF file name
                // instead of its contents
                /** @var ReviewData $reviewData */
                $reviewData = $object->getReviewData();
                $reviewData->setLogo('/uploads/collectivity/logos/' . $newFilename);
                $object->setReviewData($reviewData);
            }

            if ($reviewDataForm->has('deleteLogo')) {
                $deleteLogo = $reviewDataForm->get('deleteLogo')->getData();
                if ($deleteLogo) {
                    /** @var ReviewData $reviewData */
                    $reviewData = $object->getReviewData();
                    $reviewData->setLogo(null);
                    $object->setReviewData($reviewData);
                }
            }
        }
    }

    /**
     * Generate collectivity edit form for user.
     *
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function collectivityEditAction(): Response
    {
        $request = $this->requestStack->getMasterRequest();
        $object  = $this->userProvider->getAuthenticatedUser()->getCollectivity();

        if (!$this->security->isGranted('ROLE_USER')) {
            throw new AccessDeniedHttpException();
        }
        $form = $this->helper->createForm(
            CollectivityType::class,
            $object,
            [
                'validation_groups' => [
                    'default',
                    'collectivity_user',
                    'edit',
                ],
            ]
        );

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->formPrePersistData($object, $form);
            $this->entityManager->persist($object);
            $this->collectivityRepository->update($object);

            $this->helper->addFlash('success', $this->helper->trans('user.organization.flashbag.success.my_organization_edit'));

            return $this->helper->redirectToRoute('user_profile_collectivity_show', ['id' => $object->getId()]);
        }

        return $this->helper->render('User/Profile/collectivity_edit.html.twig', [
            'form'   => $form->createView(),
            'object' => $object,
        ]);
    }

    /**
     * Generate user edit form.
     *
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function userEditAction(): Response
    {
        $request = $this->requestStack->getMasterRequest();
        $object  = $this->userProvider->getAuthenticatedUser();

        $services = false;

        if ($object) {
            $services = $object->getServices();
        }

        $form = $this->helper->createForm(
            UserType::class,
            $object,
            [
                'validation_groups' => [
                    'default',
                    'collectivity_user',
                    'edit',
                ],
            ]
        );

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->userRepository->update($object);

            $this->helper->addFlash('success', $this->helper->trans('user.user.flashbag.success.my_profil_edit'));

            return $this->helper->redirectToRoute('user_profile_user_edit');
        }

        return $this->helper->render('User/Profile/user_edit.html.twig', [
            'form'           => $form->createView(),
            'roles'          => $object->getRoles(),
            'services'       => $services,
            'sso_type'       => $this->sso_type,
            'sso_associated' => null !== $object->getSsoKey(),
        ]);
    }

    public function userSsoUnlinkAction(): Response
    {
        $object = $this->userProvider->getAuthenticatedUser();
        $object->setSsoKey(null);
        $this->entityManager->persist($object);
        $this->entityManager->flush();
        $this->helper->addFlash('success',
            $this->helper->trans('user.user.flashbag.success.sso_unlink')
        );

        return $this->helper->redirectToRoute('user_profile_user_edit');
    }
}
