<?php

/**
 * This file is part of the MADIS - RGPD Management application.
 *
 * @copyright Copyright (c) 2018-2019 Soluris - Solutions Numériques Territoriales Innovantes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace App\Domain\Admin\Cloner;

use App\Domain\Registry\Model as RegistryModel;
use App\Domain\User\Model as UserModel;

class ToolCloner extends AbstractCloner
{
    /**
     * @param RegistryModel\Tool $referent
     *
     * @throws \Exception
     */
    protected function cloneReferentForCollectivity($referent, UserModel\Collectivity $collectivity): RegistryModel\Tool
    {
        $tool = new RegistryModel\Tool();

        $tool->setName($referent->getName());
        $tool->setType($referent->getType());
        $tool->setEditor($referent->getEditor());
        $tool->setArchival($referent->getArchival());
        $tool->setEncrypted($referent->getEncrypted());
        $tool->setAccessControl($referent->getAccessControl());
        $tool->setUpdate($referent->getUpdate());
        $tool->setBackup($referent->getBackup());
        $tool->setDeletion($referent->getDeletion());
        $tool->setHasComment($referent->getHasComment());
        $tool->setOther($referent->getOther());
        $tool->setTracking($referent->getTracking());
        $tool->setManager($referent->getManager());
        $tool->setDescription($referent->getDescription());
        $tool->setProdDate($referent->getProdDate());
        $tool->setCountryType($referent->getCountryType());
        $tool->setCountryName($referent->getCountryName());
        $tool->setCountryGuarantees($referent->getCountryGuarantees());
        $tool->setOtherInfo($referent->getOtherInfo());
        $tool->setOtherInfo($referent->getOtherInfo());

        $tool->setCollectivity($collectivity);
        $tool->setClonedFrom($referent);

        return $tool;
    }
}
