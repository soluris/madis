<?php

namespace App\Infrastructure\ORM\Registry\Repository\ConformiteOrganisation;

use App\Application\Doctrine\Repository\CRUDRepository;
use App\Application\Traits\RepositoryUtils;
use App\Domain\Registry\Model;
use App\Domain\Registry\Repository;
use App\Domain\User\Model\Collectivity;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;

class Evaluation extends CRUDRepository implements Repository\ConformiteOrganisation\Evaluation
{
    use RepositoryUtils;

    protected function getModelClass(): string
    {
        return Model\ConformiteOrganisation\Evaluation::class;
    }

    public function findAllByActiveOrganisationWithHasModuleConformiteOrganisationAndOrderedByDate($organisation = null)
    {
        $qBuilder = $this
            ->createQueryBuilder()
            ->leftJoin('o.collectivity', 'c')
        ;
        if (null !== $organisation) {
            if (\is_array($organisation)) {
                $qBuilder
                    ->andWhere(
                        $qBuilder->expr()->in('o.collectivity', ':collectivities')
                    )
                    ->setParameter('collectivities', $organisation)
                ;
            } else {
                $qBuilder
                    ->andWhere('o.collectivity = :organisation_id')
                    ->setParameter('organisation_id', $organisation->getId());
            }
        } else {
            $qBuilder
                ->addSelect('c');
        }

        $qBuilder
            ->addSelect('conformites')
            ->leftJoin('o.conformites', 'conformites')
            ->andWhere($qBuilder->expr()->eq('c.hasModuleConformiteOrganisation', ':true'))
            ->setParameter('true', true)
            ->addOrderBy('o.date', 'DESC')
            ->addOrderBy('o.createdAt', 'DESC')
        ;

        return $qBuilder
            ->getQuery()
            ->getResult()
        ;
    }

    public function findLastByOrganisation(Collectivity $organisation): ?Model\ConformiteOrganisation\Evaluation
    {
        $results = $this->createQueryBuilder()
            ->addSelect('conformites, processus, reponses, questions, actionProtections')
            ->andWhere('o.collectivity = :organisation')
            ->setParameter('organisation', $organisation)
            ->andWhere('o.isDraft = 0')
            ->leftJoin('o.conformites', 'conformites')
            ->leftJoin('conformites.processus', 'processus')
            ->leftJoin('conformites.reponses', 'reponses')
            ->leftJoin('conformites.actionProtections', 'actionProtections')
            ->leftJoin('processus.questions', 'questions')
            ->addOrderBy('o.date', 'DESC')
            ->addOrderBy('o.createdAt', 'DESC')
            ->getQuery()
            ->getResult()
        ;

        return isset($results[0]) ? $results[0] : null;
    }

    public function count(array $criteria = [])
    {
        $qb = $this->createQueryBuilder();
        $qb->leftJoin('o.collectivity', 'collectivite')
            ->andWhere('collectivite.hasModuleConformiteOrganisation=1')
        ;

        if (isset($criteria['collectivity']) && $criteria['collectivity'] instanceof Collection) {
            $this->addInClauseCollectivities($qb, $criteria['collectivity']->toArray());
            unset($criteria['collectivity']);
        }
        foreach ($criteria as $key => $value) {
            $this->addWhereClause($qb, $key, $value);
        }
        $qb->select('COUNT(o.id)');

        foreach ($criteria as $key => $value) {
            $this->addWhereClause($qb, $key, $value);
        }

        return $qb->getQuery()->getSingleScalarResult();
    }

    public function findPaginated($firstResult, $maxResults, $orderColumn, $orderDir, $searches, $criteria = [])
    {
        $qb = $this->createQueryBuilder()
            ->leftJoin('o.collectivity', 'collectivite')
            ->leftJoin('o.participants', 'participants')
            ->andWhere('collectivite.hasModuleConformiteOrganisation=1')
        ;

        if (isset($criteria['collectivity']) && $criteria['collectivity'] instanceof Collection) {
            $this->addInClauseCollectivities($qb, $criteria['collectivity']->toArray());
            unset($criteria['collectivity']);
        }
        foreach ($criteria as $key => $value) {
            $this->addWhereClause($qb, $key, $value);
        }
        // todo filtres et tri sur les participants

        $this->addTableSearches($qb, $searches);
        $this->addTableOrder($qb, $orderColumn, $orderDir);

        $query = $qb->getQuery();
        $query->setFirstResult($firstResult);
        $query->setMaxResults($maxResults);

        return new Paginator($query);
    }

    private function addTableOrder(QueryBuilder $queryBuilder, $orderColumn, $orderDir)
    {
        switch ($orderColumn) {
            case 'created_at':
                $queryBuilder->addOrderBy('o.createdAt', $orderDir);
                break;
            case 'collectivite':
                $queryBuilder->addOrderBy('collectivite.name', $orderDir);
                break;
            case 'participant':
                $queryBuilder->addSelect('count(participants.id) as participantsCount');
                $queryBuilder->addOrderBy('participantsCount', $orderDir);
                $queryBuilder->groupBy('o.id');
                break;
            case 'draft':
                $queryBuilder->addOrderBy('o.isDraft', $orderDir);
                break;
        }
    }

    private function addTableSearches(QueryBuilder $queryBuilder, $searches)
    {
        foreach ($searches as $columnName => $search) {
            switch ($columnName) {
                case 'created_at':
                    if (is_string($search)) {
                        $queryBuilder->andWhere('o.createdAt BETWEEN :create_start_date AND :create_finish_date')
                            ->setParameter('create_start_date', date_create_from_format('d/m/y', substr($search, 0, 8))->format('Y-m-d 00:00:00'))
                            ->setParameter('create_finish_date', date_create_from_format('d/m/y', substr($search, 11, 8))->format('Y-m-d 23:59:59'));
                    }
                    break;
                case 'collectivite':
                    $queryBuilder->andWhere('collectivite.name LIKE :collectivity')
                        ->setParameter('collectivity', '%' . $search . '%');
                    break;
                case 'participant':
                    $queryBuilder->addSelect('count(participants.id) as participantsCount2');
                    $queryBuilder->andHaving('participantsCount2 = :partcnt')
                        ->setParameter('partcnt', $search);
                    $queryBuilder->groupBy('o.id');
                    break;
                case 'draft':
                    $queryBuilder->andWhere('o.isDraft = :draft')
                        ->setParameter('draft', $search);
                    break;
            }
        }
    }
}
