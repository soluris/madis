<?php

/**
 * This file is part of the MADIS - RGPD Management application.
 *
 * @copyright Copyright (c) 2018-2019 Soluris - Solutions Numériques Territoriales Innovantes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace App\Application\Traits\Model;

use App\Domain\User\Model\Service;
use App\Domain\User\Model\User;

trait ServiceTrait
{
    /**
     * @var Service|null
     */
    private $service;

    public function getService(): ?Service
    {
        return $this->service;
    }

    public function setService(?Service $service = null): void
    {
        $this->service = $service;
    }

    public function isInUserServices(User $user): bool
    {
        if (false == $user->getCollectivity()->getIsServicesEnabled()) {
            return true;
        }
        if (!$this->getService()) {
            return true;
        }
        if (0 === $user->getServices()->count()) {
            return true;
        }
        foreach ($user->getServices() as $service) {
            if ($this->getService() && $service->getId() == $this->getService()->getId()) {
                return true;
            }
        }

        return false;
    }
}
