<?php

namespace App\Application\Form\Type;

use App\Application\Interfaces\CollectivityRelated;
use App\Domain\Registry\Dictionary\MesurementStatusDictionary;
use App\Domain\Registry\Dictionary\ProofTypeDictionary;
use App\Domain\Registry\Dictionary\ViolationNatureDictionary;
use App\Domain\Registry\Model\Mesurement;
use App\Domain\Registry\Model\Proof;
use App\Domain\Registry\Model\Request;
use App\Domain\Registry\Model\Treatment;
use App\Domain\Registry\Model\Violation;
use App\Domain\User\Model\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Security\Core\Security;

class LinkableType extends AbstractType
{
    private Security $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    protected function getLinkedFormField(string $label, string $class, $object, string $ariaLabel = '', $orderField = null): array
    {
        return [
            'label'         => $label,
            'class'         => $class,
            'required'      => false,
            'multiple'      => true,
            'expanded'      => false,
            'by_reference'  => false,
            'query_builder' => function (EntityRepository $er) use ($object, $orderField, $class) {
                if ($object instanceof CollectivityRelated && !\is_null($object->getCollectivity())) {
                    $collectivity = $object->getCollectivity();
                } else {
                    /** @var User $authenticatedUser */
                    $authenticatedUser = $this->security->getUser();
                    $collectivity      = $authenticatedUser->getCollectivity();
                }
                $qb = $er->createQueryBuilder('c');
                if (isset($orderField) && is_array($orderField)) {
                    foreach ($orderField as $field => $dir) {
                        $qb->addOrderBy('c.' . $field, $dir);
                    }
                } elseif ($orderField) {
                    $qb->addOrderBy('c.' . $orderField, 'asc');
                }
                if ($object instanceof $class) {
                    // self referencing, remove current object from query
                    $qb->andWhere('c != :self')
                        ->setParameter('self', $object)
                    ;
                }
                if ($collectivity) {
                    return $qb
                        ->andWhere('c.collectivity = :collectivity')
                        ->setParameter('collectivity', $collectivity)
                    ;
                }

                return $qb;
            },
            'attr' => [
                'class'            => 'selectpicker',
                'data-live-search' => 'true',
                'title'            => 'global.placeholder.multiple_select',
                'aria-label'       => $ariaLabel,
            ],
            'choice_label' => function ($object) {
                if ($object instanceof Request) {
                    return $this->formatRequestLabel($object);
                }
                if ($object instanceof Violation) {
                    return $this->formatViolationLabel($object);
                }
                if ($object instanceof Proof) {
                    return $this->formatProofLabel($object);
                }
                if ($object instanceof Treatment) {
                    return $this->formatInactiveObjectLabel($object);
                }
                if ($object instanceof Mesurement) {
                    return $this->formatStatusObjectLabel($object);
                }

                return $object->__toString();
            },
        ];
    }

    protected function formatRequestLabel(Request $object)
    {
        if (!\method_exists($object, '__toString')) {
            throw new \RuntimeException('The object ' . \get_class($object) . ' must implement __toString() method');
        }

        if (\method_exists($object, 'getDeletedAt') && null !== $object->getDeletedAt()) {
            return '(Archivé) ' . $object->getDate()->format('d/m/Y') . ' - ' . $object->__toString();
        }

        return $object->getDate()->format('d/m/Y') . ' - ' . $object->__toString();
    }

    /**
     * Prefix every inactive object with "Inactif".
     */
    protected function formatInactiveObjectLabel($object): string
    {
        if (!\method_exists($object, '__toString')) {
            throw new \RuntimeException('The object ' . \get_class($object) . ' must implement __toString() method');
        }

        if (\method_exists($object, 'isActive') && !$object->isActive()) {
            return '(Inactif) ' . $object->__toString();
        }

        return $object->__toString();
    }

    /**
     * Add status to object name.
     */
    protected function formatStatusObjectLabel($object): string
    {
        if (!\method_exists($object, '__toString')) {
            throw new \RuntimeException('The object ' . \get_class($object) . ' must implement __toString() method');
        }

        if (\method_exists($object, 'getStatus')) {
            return $object->__toString() . ' (' . MesurementStatusDictionary::getStatus()[$object->getStatus()] . ')';
        }

        return $object->__toString();
    }

    /**
     * Prefix every proof.
     */
    protected function formatProofLabel(Proof $object): string
    {
        if (!\method_exists($object, '__toString')) {
            throw new \RuntimeException('The object ' . \get_class($object) . ' must implement __toString() method');
        }

        if (\method_exists($object, 'getDeletedAt') && null !== $object->getDeletedAt()) {
            return '(Archivé) ' . $object->__toString() . ' (' . ProofTypeDictionary::getTypes()[$object->getType()] . ')';
        }

        return $object->__toString() . ' (' . ProofTypeDictionary::getTypes()[$object->getType()] . ')';
    }

    /**
     * Prefix violations.
     */
    protected function formatViolationLabel(Violation $object): string
    {
        if (!\method_exists($object, '__toString')) {
            throw new \RuntimeException('The object ' . \get_class($object) . ' must implement __toString() method');
        }

        $natures = [];

        if ($object->getViolationNatures()) {
            $raw = $object->getViolationNatures();
            if (is_string($raw)) {
                $raw = explode(',', $raw);
            }
            $natures = array_map(function ($n) {return ViolationNatureDictionary::getNatures()[trim($n)]; }, (array) $raw);
        }

        if (\method_exists($object, 'getDeletedAt') && null !== $object->getDeletedAt()) {
            return '(Archivé) ' . $object->getDate()->format('d/m/Y') . ' - ' . join(', ', $natures);
        }

        return $object->getDate()->format('d/m/Y') . ' - ' . join(', ', $natures);
    }
}
